# **GeekShots 📷**
![Authors](https://img.shields.io/badge/authors-2-green)
![Version](https://img.shields.io/badge/version-1.5.0-blue)
![Likes](https://img.shields.io/badge/likes-102k-blue)
<br><br>
`GeekShots` is a single-page-application for online photo contests. Professional and amateur photographers can participate in challenging contests by uploading their best photographies that correspond with the subject of the contest. Admins manage all of the contests and vote for the entries. Users participate and could be picked for a jury if a certain ranking is met.<br><br>
🔰 Users ranking:<br>
`Organizer`<br>
Organizer (administrator) account type can create contests, send invitations to other users, vote for entries and draw winners.<br>
`Junkie`<br>
Junkie ranking is assigned to each user upon registration. Junkies can participate in contests.<br>
`Enthusiast`<br>
Enthusiast ranking is unlocked when a `Junkie` collects 51 points. Enthusiasts can participate in contests.<br>
`Master`<br>
Master ranking is unlocked when an `Enthusiast` collects 151 points. Masters can participate in contests. Masters can be invited as jury).<br>
`Wise and Benevolent Photo Dictator`<br>
Wise and Benevolent Photo Dictator ranking is unlocked when a `Master` collects 1001 points. Wise and Benevolent Photo Dictators can participate in contests. Wise and Benevolent Photo Dictators can be invited as jury).<br><br>
Dummy organizer profiles are preseeded on npm populate request first of them with the following details:
<br>🆔 username: edo@telerik.com
<br>🔐 password: Admin<br><br>
Users can collect points for the following actions:
<br>📷 joining open photo contest: 1 point
<br>✉️ being invited for photo contest: 3 points
<br>🏆 1st place in photo contest: 50 points (40 if shared 1st)
<br>🏆 2nd place in photo contest: 35 points (25 if shared 2nd)
<br>🏆 3rd place in photo contest: 20 points (10 if shared 3rd)
<br><br>
If you have any suggestions or issues 🐛, please feel free to share them with us. 💬💬
## **Prerequisites**
Before you begin, ensure you have met the following requirements:
<br>🏁 You have installed the latest version of VS Code
<br>💻 You have a `<Windows/Linux/Mac>` machine.
<br>📖 You have read the complete README file.<br><br>
## **Using the GeekShots API**
To install the `GeekShots API`, follow these steps:<br>
👀<br>
1.&ensp;First install all dependency files:
```
npm install
```
2.&ensp;Find the database file `contest.sql` or `contest.mdb` from `src/data/db-utils/` folder and open it in MySql Workbench (or similar).
3.&ensp;Create a `.env` file as the template below
```
PORT = 3001
DB_USER = root     // username of your database
DB_PASSWORD = root  // password of your database
DB_DATABASE = contest 
```
4.&ensp;Populate the database (including dummy admin user) with the following command:
```
npm run populate
```
5.&ensp;If necessary you can empty the database with the below command and then populate it again:
```
npm run empty
```
6.&ensp;Start the server in developers mode `<nodemon>`:
```
npm run start:dev
```
7.&ensp;Read carefully the input requirements of each endpoint:<br>
<br>`POST` ➡️ http://localhost:3001/api/session **Input**: username & password **Response**: JWT token for authentication
<br>`DELETE` ➡️ http://localhost:3001/api/session **Input**: N/A **Response**: confirmation message
<br>`GET` ➡️ http://localhost:3001/api/contests **Input**: N/A **Response**: array containing all contests
<br>`GET` ➡️ http://localhost:3001/api/contests/:id **Input**: N/A **Response**: object containing contests details
<br>`GET` ➡️ http://localhost:3001/api/contests/categories **Input**: N/A **Response**: array containing all categories
<br>`POST` ➡️ http://localhost:3001/api/contests/record **Input**: title, days, hours, category, type, draw **Response**: object containing contest details
<br>`POST` ➡️ http://localhost:3001/api/contests/:id/record **Input**: cover (form-data) **Response**: object containing contest cover details
<br>`POST` ➡️ http://localhost:3001/api/contests/:id/winners **Input**: N/A **Response**: array containing contest winners 
<br>`GET` ➡️ http://localhost:3001/api/contests/:id/winners **Input**: N/A **Response**: array containing contest winners (incl. photos)
<br>`POST` ➡️ http://localhost:3001/api/contests/:id/jury **Input**: array contatining jury ids **Response**: array containing contest jury 
<br>`POST` ➡️ http://localhost:3001/api/contests/:id/invitations **Input**: array contatining invited users ids **Response**: array containing invited users 
<br>`GET` ➡️ http://localhost:3001/api/contests/:id/photos **Input**: N/A **Response**: array containing contest photos 
<br>`GET` ➡️ http://localhost:3001/api/contests/:id/photos/:photoId **Input**: N/A **Response**: object containing contest photo details
<br>`POST` ➡️ http://localhost:3001/api/contests/:id/photos **Input**: photo (form-data) **Response**: object containing photo details 
<br>`POST` ➡️ http://localhost:3001/api/contests/:id/photos/:photoId **Input**: title, story **Response**: object containing contest photo details 
<br>`POST` ➡️ http://localhost:3001/api/contests/:id/photos/:photoId/votest **Input**: score, comment **Response**: object containing vote details
<br>`GET` ➡️ http://localhost:3001/api/users **Input**: N/A **Response**: array containing all users
<br>`POST` ➡️ http://localhost:3001/api/users/:id **Input**: username, first name, last name, password **Response**: object containing user details
<br>`GET` ➡️ http://localhost:3001/api/users/:id **Input**: N/A **Response**: object containing user details
<br><br>
## **Contact** 🔍

You can reach us at:
<br><kristina.zlateva.a24@learn.telerikacademy.com> 
<br><borisborisov.a24@learn.telerikacademy.com>.

## **License** 🛡️

This project uses the following license:   

![license](https://img.shields.io/badge/license-telerik2020-yellow)

![protection](https://img.shields.io/badge/bug_protected-green)