import dotenv from 'dotenv';
import bcrypt from 'bcrypt';
import path from 'path';

const config = dotenv.config().parsed;

export const DB_CONFIG = {
    host: 'localhost',
    port: '3306',
    user: config.DB_USER,
    password: config.DB_PASSWORD,
    database: config.DB_DATABASE,
    multipleStatements: true,
  };

export const PORT = config.PORT;

export const PRIVATE_KEY = 'cheta';

// 3600 secs = 1hr
export const TOKEN_LIFETIME = 60 * 60; 

export const DEFAULT_USER_ROLE = 'Junkie';
export const ADMIN_USER_ROLE = 'Organizer';
export const ENTHUSIAST_USER_ROLE = 'Enthusiast';
export const MASTER_USER_ROLE = 'Master';
export const DICTATOR_USER_ROLE = 'Wise and Benevolent Photo Dictator';

export const COMMENT_USER_POINTS = 5;
export const REVIEW_USER_POINTS = 5;
export const RATING_USER_POINTS = 5;
export const RETURN_BOOK_USER_POINTS = 10;

export const ADMIN_PASSWORD_HASH = bcrypt.hashSync('Admin', 10);
export const KRISI_PASSWORD_HASH = bcrypt.hashSync('Krisi', 10);
export const BORIS_PASSWORD_HASH = bcrypt.hashSync('Boris', 10);
export const ROSEN_PASSWORD_HASH = bcrypt.hashSync('Roskata', 10);
export const NADYA_PASSWORD_HASH = bcrypt.hashSync('Nadeto', 10);
export const STOYAN_PASSWORD_HASH = bcrypt.hashSync('Stoyan', 10);
export const MIRCHEV_PASSWORD_HASH = bcrypt.hashSync('Mirchev', 10);
export const MILANOV_PASSWORD_HASH = bcrypt.hashSync('Milanov', 10);
export const PETROV_PASSWORD_HASH = bcrypt.hashSync('Petrov', 10);
export const CHRISTOV_PASSWORD_HASH = bcrypt.hashSync('Christov', 10);
export const MARKOVA_PASSWORD_HASH = bcrypt.hashSync('Markova', 10);
export const HRISIMOVA_PASSWORD_HASH = bcrypt.hashSync('HRISIMOVA', 10);
export const MINEV_PASSWORD_HASH = bcrypt.hashSync('Minev', 10);
export const ANCHEV_PASSWORD_HASH = bcrypt.hashSync('Anchev', 10);

export const checkFileType = (file, cb) => {
  const fileTypes = /jpeg|jpg|png/;
  const extName = fileTypes.test(path.extname(file.originalname).toLowerCase());
  const mimeType = fileTypes.test(file.mimetype);

  if (mimeType && extName) {
      return cb(null, true);
  } else {
      cb({ message: 'Jpeg, jpg or png images only!' });
  }
};
