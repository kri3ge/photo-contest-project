import express from 'express';
import serviceErrors from '../services/service-errors.js';
import { createValidator, createUserSchema } from '../validations/index.js';
import { authMiddleware } from '../auth/middlewares.js';

const createUsersController = (usersService, usersData) => {

  const usersController = express.Router();

  usersController
    .get('/',
      authMiddleware,
      async (req, res) => {
        try {
          const { search } = req.query;
          const { users, error } = await usersService.getAllUsers(usersData)(search);

          if (error === serviceErrors.RECORD_NOT_FOUND) {
            res.status(404).send({ message: 'No users registered!' });
          } else {
          res.status(200).send(users);
          }
        } catch (err) {
          res.status(500).send({ err: err.message });
        }
      })
    .get('/:id',
      authMiddleware,
      async (req, res) => {
        try {
          const { id } = req.params;

          const { error, user } = await usersService.getUserById(usersData)(
            +id,
          );

          if (error === serviceErrors.RECORD_NOT_FOUND) {
            res.status(401).send({ message: 'User not found!' });
          } else {
            res.status(200).send(user);
          }
        } catch (err) {
          res.status(500).send({ err: err.message });
        }
      })
    // create user
    .post('/', createValidator(createUserSchema), async (req, res) => {
      try {
        const createData = req.body;

        const { error, user } = await usersService.createUser(usersData)(
          createData,
        );
        if (error === serviceErrors.DUPLICATE_RECORD) {
          res
            .status(409)
            .send({ message: 'This username has already been registered' });
        } else {
          res.status(201).send(user);
        }
      } catch (err) {
        res.status(500).send({ err: err.message });
      }
    });

  return usersController;
};

export default createUsersController;
