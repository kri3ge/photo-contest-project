import express from 'express';
import multer from 'multer';
import sharp from 'sharp';
import path from 'path';
import { contestIdMiddleware } from '../auth/middlewares.js';
import { checkFileType } from '../config.js';
import { createValidator, photoDetailsSchema, voteSchema } from '../validations/index.js';
import serviceErrors from '../services/service-errors.js';

const storage = multer.diskStorage({
    destination: './public/orig-photos/',
    filename: function (req, file, cb) {
        cb(null, 'orig-' + file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    },
});

const upload = multer({
    storage: storage,
    limits: { fileSize: 1024 * 1024 * 15 },
    fileFilter: (req, file, cb) => {
        checkFileType(file, cb);
    },
}).single('contestImage');

const createPhotosController = (photosService, photosData, usersData) => {

    const photosController = express.Router();

    photosController
        .get('/:id/photos',
            contestIdMiddleware(),
            async (req, res) => {
                try {
                    const { id } = req.params;

                    const { error, contestPhotos } = await photosService.getContestPhotos(photosData)(id);

                    if (error === serviceErrors.RECORD_NOT_FOUND) {
                        return res.status(401).send({
                            message: `Sorry, there are no photos uploaded for contest with id ${id}`,
                        });
                    } else {
                        return res.status(200).send(contestPhotos);
                    }

                } catch (err) {
                    res.status(500).send({ err: err.message });
                }
            })
        .get('/:id/photos/:photoId',
            contestIdMiddleware(),
            async (req, res) => {
                try {
                    const { id, photoId } = req.params;

                    const { error, contestPhoto } = await photosService.getContestPhotoById(photosData)(id, photoId);

                    if (error === serviceErrors.RECORD_NOT_FOUND) {
                        return res.status(401).send({
                            message: `Sorry, there is no photo with id ${photoId} for contest with id ${id}`,
                        });
                    } else {
                        return res.status(200).send(contestPhoto);
                    }

                } catch (err) {
                    res.status(500).send({ err: err.message });
                }
            })
        .post('/:id/photos',
            contestIdMiddleware(),
            async (req, res) => {
                const filePromise = new Promise((resolve, reject) => {
                    upload(req, res, err => {
                        if (err) {
                            reject({
                                code: 401,
                                message: err.message,
                            });
                        }
                        if (!req.file) {
                            reject({
                                code: 402,
                                message: 'No image selected!',
                            });
                        }
                        resolve(req.file);
                    });
                });

                try {
                    const file = await filePromise;
                    try {
                        const contestId = req.params.id;
                        const userId = req.user.id;
                        const { filename } = file;

                        const resFilename = path.join(
                            'res-' + req.file.fieldname + '-' + Date.now() + path.extname(req.file.originalname),
                        );
                        await sharp(req.file.path).resize({ width: 1200, height: 630 }).toFile(
                            path.join('./', 'public', 'res-photos', resFilename));

                        const { error, photo } = await photosService.createPhoto(photosData)(
                            contestId,
                            userId,
                            filename,
                            resFilename,
                        );

                        if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
                            return res.status(403).send({
                                message: `Sorry, you have already uploaded a photo for contest with id ${contestId}!`,
                            });
                        } else {
                            return res.status(201).send(photo);
                        }

                    } catch (err) {
                        res.status(500).send({ err: err.message });
                    }

                } catch (err) {
                    res.status(err.code).send({ message: err.message });
                }
            })
        .post('/:id/photos/:photoId',
            contestIdMiddleware(),
            createValidator(photoDetailsSchema),
            async (req, res) => {
                try {
                    const { id, photoId } = req.params;
                    const userId = req.user.id;

                    const { error, photoDetails } = await photosService.createPhotoDetails(photosData, usersData)(
                        id,
                        photoId,
                        req.body,
                        userId,
                    );

                    if (error === serviceErrors.RECORD_NOT_FOUND) {
                        return res.status(403).send({
                            message: `Sorry, there is no photo with id ${photoId} for contest with id ${id}!`,
                        });
                    } else if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
                        return res.status(404).send({
                            message: `Photo details have already been assigned to the photo with id ${photoId}!`,
                        });
                    } else {
                        return res.status(201).send(photoDetails);
                    }

                } catch (err) {
                    res.status(err.code).send({ message: err.message });
                }
            })
        .post('/:id/photos/:photoId/votes',
            contestIdMiddleware(),
            createValidator(voteSchema),
            async (req, res) => {
                try {
                    const contestId = req.params.id;
                    const photoId = req.params.photoId;
                    const userId = req.user.id;
                    const { role } = req.user;

                    const { error, photoVote } = await photosService.createPhotoVote(photosData)(
                        userId,
                        contestId,
                        photoId,
                        req.body,
                        role,
                    );

                    if (error === serviceErrors.RECORD_NOT_FOUND) {
                        return res.status(401).send({
                            message: `Sorry, there is no photo with id ${photoId} for contest with id ${contestId}!`,
                        });
                    } else if (error === serviceErrors.DUPLICATE_RECORD) {
                        return res.status(402).send({
                            message: `You have alrearedy voted for the photo with id ${photoId}!`,
                        });
                    } else if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
                        return res.status(403).send({
                            message: 'Only organizers and photo masters selected as a jury are allowed to vote!',
                        });
                    } else {
                        return res.status(201).send(photoVote);
                    }

                } catch (err) {
                    res.status(err.code).send({ message: err.message });
                }
            });

    return photosController;
};

export default createPhotosController;