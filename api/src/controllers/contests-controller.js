import express from 'express';
import multer from 'multer';
import sharp from 'sharp';
import path from 'path';
import moment from 'moment';
import serviceErrors from '../services/service-errors.js';
import { contestIdMiddleware, roleMiddleware } from '../auth/middlewares.js';
import { createValidator, createContestSchema, juryInvitationsSchema } from '../validations/index.js';
import { ADMIN_USER_ROLE, checkFileType } from '../config.js';

const storage = multer.diskStorage({
  destination: './public/covers/',
  filename: (req, file, cb) => {
    cb(null, 'orig-' + file.fieldname + '-' + Date.now() + path.extname(file.originalname));
  },
});

const upload = multer({
  storage: storage,
  limits: { fileSize: 1024 * 1024 * 15 },
  fileFilter: (req, file, cb) => {
    checkFileType(file, cb);
  },
}).single('coverImg');

const createContestsController = (contestsService, contestsData, usersData) => {

  const contestsController = express.Router();

  contestsController
    .get('/', async (req, res) => {
      try {
        const page = parseInt(req.query.page);
        const limit = parseInt(req.query.limit);

        const { error, allContests } = await contestsService.getAllContests(contestsData)();

        if (error === serviceErrors.RECORD_NOT_FOUND) {
          return res.status(404).send({ message: 'Sorry, no contests available!' });
        }

        const startIndex = (page - 1) * limit;
        const endIndex = page * limit;
        const contests = {};

        if (endIndex < allContests.length) {
          contests.next = {
            page: page + 1,
            limit: limit,
          };
        }
        if (startIndex > 0) {
          contests.previous = {
            page: page - 1,
            limit: limit,
          };
        }

        contests.contestsList = allContests.slice(startIndex, endIndex);

        return res.status(200).send(contests);

      } catch (err) {
        res.status(500).send({ err: err.message });
      }
    })
    .get('/categories',
      async (req, res) => {
        try {
          const { error, categories } = await contestsService.getContestCategories(contestsData)();

          if (error === serviceErrors.RECORD_NOT_FOUND) {
            return res.status(404).send({
              message: 'No categories yet!',
            });
          }
          res.status(200).send(categories);

        } catch (err) {
          res.status(500).send({ err: err.message });
        }
      })
    .post(
      '/record',
      roleMiddleware(ADMIN_USER_ROLE),
      createValidator(createContestSchema),
      async (req, res) => {
        try {
          const {
            title,
            phaseOneLimitDays,
            phaseTwoLimitHours,
            contestCategory,
            contestType,
            is_auto,
          } = req.body;
          const startDate = moment().format('YYYY-MM-DD');

          const { error, contest } = await contestsService.createContest(
            contestsData,
          )(
            title,
            phaseOneLimitDays,
            phaseTwoLimitHours,
            contestCategory,
            contestType,
            startDate,
            is_auto,
          );
          if (error === serviceErrors.DUPLICATE_RECORD) {
            return res.status(404).send({
              message: `Sorry, contest ${title} aleready exists!`,
            });
          } else {
            return res.status(201).send(contest);
          }

        } catch (err) {
          res.status(500).send({ err: err.message });
        }
      },
    )
    .post(
      '/:id/record/',
      roleMiddleware(ADMIN_USER_ROLE),
      contestIdMiddleware(),
      async (req, res) => {
        const filePromise = new Promise((resolve, reject) => {
          upload(req, res, err => {
            if (err) {
              reject({
                code: 401,
                message: err.message,
              });
            }
            if (!req.file) {
              reject({
                code: 402,
                message: 'No image selected!',
              });
            }
            resolve(req.file);
          });
        });
        try {
          const file = await filePromise;
          try {
            const { filename } = file;
            const { id } = req.params;

            const coverRes = path.join(
              'res-' + req.file.fieldname + '-' + + Date.now() + path.extname(req.file.originalname),
            );
            await sharp(req.file.path).resize({ width: 1200, height: 400 }).toFile(
              path.join('./', 'public', 'res-covers', coverRes));

            const { error, contestCover } = await contestsService.createContestCover(
              contestsData,
            )(
              id,
              filename,
              coverRes,
            );

            if (error === serviceErrors.DUPLICATE_RECORD) {
              return res.status(404).send({
                message: `Sorry, cover has already been added to contest with id ${id}!`,
              });
            } else {
              return res.status(201).send(contestCover);
            }

          } catch (err) {
            res.status(500).send({ err: err.message });
          }

        } catch (err) {
          res.status(err.code).send({ message: err.message });
        }
      },
    )
    .get('/:id',
      contestIdMiddleware(),
      async (req, res) => {
        try {
          const { id } = req.params;

          const { contest } = await contestsService.getContestById(
            contestsData,
          )(+id);
          res.status(200).send(contest);

        } catch (err) {
          res.status(500).send({ err: err.message });
        }
      })
    .post('/:id/winners',
      roleMiddleware(ADMIN_USER_ROLE),
      contestIdMiddleware(),
      async (req, res) => {
        try {
          const { id } = req.params;

          const { error, winners } = await contestsService.drawAndSetContestWinners(contestsData, usersData)(id);

          if (error === serviceErrors.DUPLICATE_RECORD) {
            return res.status(403).send({
              message: `Sorry, winners for contest with id ${id} have already been drawn and set!`,
            });
          } else if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
            return res.status(404).send({
              message: `Sorry, photo scores for contest with id ${id} must be populated in orded to draw winners!`,
            });
          }
          res.status(201).send(winners);

        } catch (err) {
          res.status(500).send({ err: err.message });
        }
      })
    .get('/:id/winners',
      contestIdMiddleware(),
      async (req, res) => {
        try {
          const { id } = req.params;

          const { error, winners } = await contestsService.getContestWinners(contestsData)(id);

          if (error === serviceErrors.RECORD_NOT_FOUND) {
            return res.status(404).send({
              message: `Sorry, winners for contest with id ${id} have not been drawn yet!`,
            });
          }
          res.status(201).send(winners);

        } catch (err) {
          res.status(500).send({ err: err.message });
        }
      })
    .post(
      '/:id/jury',
      roleMiddleware(ADMIN_USER_ROLE),
      createValidator(juryInvitationsSchema),
      contestIdMiddleware(),
      async (req, res) => {
        try {
          const { id } = req.params;
          const { usersIds } = req.body;

          const { error, jury } = await contestsService.setContestJury(contestsData)(
            usersIds,
            id,
          );

          if (error === serviceErrors.DUPLICATE_RECORD) {
            return res.status(404).send({
              message: `Jury for contest with id ${id} has already been set!`,
            });
          }
          return res.status(201).send(jury);

        } catch (err) {
          return res.status(500).send({ err: err.message });
        }
      },
    )
    .post(
      '/:id/invitations',
      roleMiddleware(ADMIN_USER_ROLE),
      contestIdMiddleware(),
      createValidator(juryInvitationsSchema),
      async (req, res) => {
        try {
          const { id } = req.params;
          const { usersIds } = req.body;

          const { error, invitations } = await contestsService.setContestInvitations(contestsData)(
            usersIds,
            id,
          );

          if (error === serviceErrors.DUPLICATE_RECORD) {
            return res.status(404).send({
              message: `Invitations for contest with id ${id} have already been set!`,
            });
          }
          return res.status(201).send(invitations);

        } catch (err) {
          return res.status(500).send({ err: err.message });
        }
      },
    );

  return contestsController;
};

export default createContestsController;
