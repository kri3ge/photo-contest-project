import express from 'express';
import createToken from './../auth/create-token.js';
import serviceErrors from '../services/service-errors.js';
import { createValidator, signInSchema } from '../validations/index.js';
import { authMiddleware } from '../auth/middlewares.js';

const createAuthController = (usersService, usersData) => {

  const authController = express.Router();

  authController
    .post('/session', createValidator(signInSchema), async (req, res) => {
      try {
        const { username, password } = req.body;
        const { error, user } = await usersService.signInUser(usersData)(
          username,
          password,
        );

        if (error === serviceErrors.INVALID_USERNAME) {
          res.status(403).send({
            message: 'Invalid username',
          });
        } else if (error === serviceErrors.INVALID_PASSWORD) {
          res.status(403).send({
            message: 'Invalid password',
          });
        } else {
          const payload = {
            sub: user.id,
            username: user.username,
            firstName: user.first_name,
            lastName: user.last_name,
            memberSince: user.member_since,
            role: user.role,
            picture: user.picture,
            points: user.user_points,  
          };
          const token = createToken(payload);

          res.status(200).send({
            token: token,
            username: user.username,
            role: user.role,
            id: user.id,
          });
        }
      } catch (err) {
        res.status(500).send({ err: err.message });
      }
    })
    .delete('/session',
      authMiddleware,
      async (req, res) => {
        try {
          const headerToken = req.headers.authorization.split(' ')[1];
          const { id } = req.user;
          const { message } = await usersService.signOutUser(usersData)(
            id,
            headerToken,
          );

          res.status(200).send({
            message,
          });
        } catch (err) {
          res.status(500).send({ err: err.message });
        }
      });

  return authController;
};

export default createAuthController;
