
export const createContestSchema = {
  title: (value) => {
    if (!value) {
      return 'Title should be indicated in the body!';
    }
    if (
      typeof value !== 'string' ||
      value.trim().length < 3 ||
      value.trim().length > 45
    ) {
      return 'Contest title should be a string in range [3..45] symbols!';
    }

    return null;
  },
  phaseOneLimitDays: (value) => {
    if (!value) {
      return 'phaseOneLimitDays should be indicated in the body!';
    }
    if (value < 1 || value > 31) {
      return 'Phase 1 limit should be between 1 and 31 days!';
    }

    return null;
  },
  phaseTwoLimitHours: (value) => {
    if (!value) {
      return 'phaseTwoLimitDays should be indicated in the body!';
    }
    if (value < 1 || value > 24) {
      return 'Phase 2 limit should be between 1 and 24 hours!';
    }

    return null;
  },
  contestCategory: (value) => {
    if (!value) {
      return 'contestCategory should be indicated in the body!';
    }
    if (
      typeof value !== 'string' ||
      value.trim().length < 3 ||
      value.trim().length > 60
    ) {
      return 'Contest category should be a string in range [3..60] symbols!';
    }

    return null;
  },
  contestType: (value) => {
    if (!value) {
      return 'contestType should be indicated in the body!';
    }
    if (value !== 'Open' && value !== 'Invitational') {
      return 'Contest type should be Open or Invitational!';
    }

    return null;
  },
  is_auto: (value) => {
    if (value.length === 0 || typeof value === 'undefined') {
      return 'Draw type(is_auto) should be indicated in the body!';
    }
    if (isNaN(+value) || value < 0 || value > 1) {
      return 'Draw type(is_auto) should be an integer in range [0..1]! Use 1 for is_auto and 0 for is_not_auto.';
    }

    return null;
  },
};
