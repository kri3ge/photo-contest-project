export const voteSchema = {
    score: (value) => {
        if (value === null || typeof value === 'undefined') {
            return 'Score should be indicated in the body!';
        }
        if (
            typeof value !== 'number' &&
            (value < 0 || value > 10)
        ) {
            return 'Score should be a number in range [1..10]!';
        }

        return null;
    },
    comment: (value) => {
        if (!value) {
            return 'Comment should be indicated in the body!';
        }
        if (
            typeof value !== 'string' ||
            value.trim().length < 3
        ) {
            return 'Comment should be a string longer than 3 symbols!';
        }

        return null;
    },

};