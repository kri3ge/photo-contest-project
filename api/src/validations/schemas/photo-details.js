export const photoDetailsSchema = {
    title: (value) => {
        if (!value) {
            return 'Title should be indicated in the body!';
        }
        if (
            typeof value !== 'string' ||
            value.trim().length < 3 ||
            value.trim().length > 45
        ) {
            return 'Photo title should be a string in range [3..45] symbols!';
        }

        return null;
    },
    story: (value) => {
        if (!value) {
            return 'Story should be indicated in the body!';
        }
        if (
            typeof value !== 'string' ||
            value.trim().length < 3
        ) {
            return 'Photo story should be a string longer than 3 symbols!';
        }

        return null;
    },
};
