export const juryInvitationsSchema = {
    usersIds: (value) => {
        if (!value) {
            return 'UserIds should be indicated in the body!';
        }
        if (!Array.isArray(value)) {
            return 'UsersIds should be an array containing user ids!';
        }

        return null;
    },
};