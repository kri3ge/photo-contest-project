export const signInSchema = {
  username: (value) => {
    const regexUsername = /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/;

    if (!value) {
      return { message: 'username is required!' };
    }

    if (
      typeof value !== 'string' ||
      value.trim().length < 5 ||
      value.trim().length > 40 ||
      !value.match(regexUsername)
    ) {
      return {
        message:
          'username should be string in range [5..40] symbols in format pesho.peshov@example.com',
      };
    }

    return null;
  },
  username: (value) => {
    const regexUsername = /^[a-zA-Z\d\-_.&!@#%$]+$/;

    if (value && !value.match(regexUsername)) {
      return {
        message:
          'Username should be alphanumerical string in range [3..25] symbols! Can include special characters',
      };
    }
    if (value && value.trim() < 3) {
      return {
        message:
          'Username should be alphanumerical string in range [3..25] symbols! Can include special characters',
      };
    }

    return null;
  },

  password: (value) => {
    const regexPass = /^[a-zA-Z\d\-_.,&!@#%$]+$/;

    if (!value) {
      return { message: 'Password is required!' };
    }

    if (
      typeof value !== 'string' ||
      value.trim().length < 5 ||
      value.trim().length > 25 ||
      !value.match(regexPass)
    ) {
      return {
        message:
          'Password should be alphanumerical string in range [3..25] symbols! Can include special characters',
      };
    }

    return null;
  },
};
