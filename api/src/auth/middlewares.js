import passport from 'passport';
import contestsData from '../data/contests-data.js';
import usersData from '../data/users-data.js';

const authMiddleware = passport.authenticate('jwt', { session: false });

const logoutMiddleware = () => {
  return async (req, res, next) => {
    const headerToken = req.headers.authorization.split(' ')[1];
    const userToken = await usersData.getToken(headerToken);

    if (userToken[0]) {
      return res.status(401).send({
        message: 'User has logged out.',
      });
    } else {
      next();
    }
  };
};

const roleMiddleware = (roleName) => {
  return async (req, res, next) => {
    const user = await usersData.getWithAccount(req.user.username);

    if (req.user && user.role === roleName) {
      next();
    } else {
      return res.status(401).send({
        message: 'Resource is forbidden.',
      });
    }
  };
};

const contestIdMiddleware = () => {
  return async (req, res, next) => {
    const { id } = req.params;
    const contest = await contestsData.getContestByIdOnly(id);

    if (contest.title) {
      next();
    } else {
      return res.status(401).send({
        message: `Sorry, there is no contest with id ${id}.`,
      });
    }
  };
};


export {
  authMiddleware,
  logoutMiddleware,
  roleMiddleware,
  contestIdMiddleware,
};
