import serviceErrors from './service-errors.js';
import bcrypt from 'bcrypt';

const signInUser = usersData => async (username, password) => {
    const user = await usersData.getWithAccount(username);
    
    if (!user) {
      return {
        error: serviceErrors.INVALID_USERNAME,
        user: null,
      };
    } else if (!(await bcrypt.compare(password, user.password))) {
      return {
        error: serviceErrors.INVALID_PASSWORD,
        user: null,
      };
    }

    const userPoints = await usersData.getUserPoints(user.id);

    if (userPoints['SUM(points)'] > 50 && userPoints['SUM(points)'] < 151 && user.role === 'Junkie') {
      await usersData.userRoleUpgrade(user.id, user.role);
    } else if (userPoints['SUM(points)'] > 150 && userPoints['SUM(points)'] < 1000 && user.role === 'Enthusiast') {
      await usersData.userRoleUpgrade(user.id, user.role);
    } else if (userPoints['SUM(points)'] > 1000 && user.role === 'Master') {
      await usersData.userRoleUpgrade(user.id, user.role);
    }

    const userUpd = await usersData.getBy('id', user.id);

    return {
      error: null,
      user: userUpd,
    };
  };


const getAllUsers = usersData => async (filter) => {
  const users = filter
      ? await usersData.getBy('username', filter)
      : await usersData.getAll();

      if (!users) {
        return {
          error: serviceErrors.RECORD_NOT_FOUND,
          users: null,
        };
      }

      users.map( user => {
        if (user.user_points == null) {
          user.user_points = 0;
        } });
        return { 
          error: null,
          users: users,
        };
  };


const getUserById = usersData => async (id) => {
    const user = await usersData.getBy('id', id);
    if (!user) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        user: null,
      };
    }
    return { error: null, user: user };
  };

const createUser = usersData => async (userCreate) => {
    const { username, firstName, lastName, password, picture } = userCreate;

    const existingUser = await usersData.getBy('username', username);

    if (existingUser.username) {
      return {
        error: serviceErrors.DUPLICATE_RECORD,
        user: null,
      };
    }
    
    const memberSince = new Date();
    const passwordHash = await bcrypt.hash(password, 10);
    const user = await usersData.createUser(
      username,
      firstName, 
      lastName,
      passwordHash,
      memberSince,
      picture,
    );

    return { error: null, user: user };
  };

export default {
  signInUser,
  getAllUsers,
  getUserById,
  createUser,
};
