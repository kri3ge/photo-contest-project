import serviceErrors from './service-errors.js';
import moment from 'moment';

const getAllContests = contestsData => async () => {
  const allContests = await contestsData.getAllContests();
  if (allContests.length === 0) {
    return { error: serviceErrors.RECORD_NOT_FOUND, allContests: null };
  }
  allContests.map((contest) => {
    contest.start_date = moment(contest.start_date).format('DD/MM/YYYY');
  });

  return { error: null, allContests: allContests };
};

const getContestById = contestsData => async (id) => {
  const contest = await contestsData.getContestById(id);
  const invitations = await contestsData.getContestInvitations(id);
  const jury = await contestsData.getContestJury(id);
  const winners = await contestsData.getContestWinners(id);

  contest.invitations = invitations;
  contest.jury = jury;
  contest.winners = winners;
  contest.start_date = moment(contest.start_date).format('DD/MM/YYYY');

  return { contest: contest };
};

const setContestJury = contestsData => async (usersIds, contestId) => {
  const isJury = await contestsData.getContestJury(contestId);

  if (isJury[0]) {
    return {
      error: serviceErrors.DUPLICATE_RECORD,
      jury: null,
    };
  } else {
    await Promise.all(usersIds.map(user => contestsData.setContestJury(contestId, user)));
  }

  return {
    error: null,
    jury: {
      message: `Jury for contest with id ${contestId} has been set!`,
      jury: await contestsData.getContestJury(contestId),
    },
  };
};

const getContestWinners = contestsData => async(contestId) => {
  const winners = await contestsData.getContestWinners(contestId);
  const contestPhotosAvgScores = await contestsData.getPhotosByContestId(contestId);
  winners.map(winner => {
  const filteredPhotos = contestPhotosAvgScores.filter(user => user.user_id === winner.users_id)[0];
  winner.photo = filteredPhotos.photo;
  winner.photo_res = filteredPhotos.photo_res;
  winner.photo_id = filteredPhotos.id;
  });

  if (!winners || winners.length === 0) {
    return {
      error: serviceErrors.RECORD_NOT_FOUND,
      winners: null,
    };
  }
  return {
    error: null,
    winners: winners,
  };
};

const drawAndSetContestWinners = (contestsData, usersData) => async (contestId) => {
  const isWinners = await contestsData.getContestWinners(contestId);

  if (isWinners[0]) {
    return {
      error: serviceErrors.DUPLICATE_RECORD,
      winners: null,
    };
  }

  const contestPhotosAvgScores = await contestsData.getPhotosByContestId(contestId);
  contestPhotosAvgScores.sort((a, b) => (a.score > b.score) ? -1 : 1);

  const groupBy = (data, keySelector, valueSelector = (el => el)) => {
    const map = new Map();

    for (const obj of data) {
      const key = keySelector(obj);
      const value = valueSelector(obj);

      if (!map.has(key)) {
        map.set(key, []);
      }

      map.get(key).push(value);
    }

    return map;
  };

  const groupedByScore = groupBy(contestPhotosAvgScores, el => el.score);
  const arrayFromMapValues = Array.from(groupedByScore.values()).slice(0, 3);

  const [first, second, third] = arrayFromMapValues;

  if (!first || !second || !third) {
    return {
      error: serviceErrors.OPERATION_NOT_PERMITTED,
      winners: null,
    };
  }

  const firstPlaces = first?.map(el => ({ ...el, place: 'First', points: first.length > 1 ? 40 : 50 }));
  const secondPlaces = second?.map(el => ({ ...el, place: 'Second', points: second.length > 1 ? 25 : 35 }));
  const thirdPlaces = third?.map(el => ({ ...el, place: 'Third', points: third.length > 1 ? 10 : 20 }));

  if (first.length === 1 && first[0].score / second[0].score >= 2) {
    firstPlaces[0].points = 75;
  }

  const usersPlaces = [...firstPlaces, ...secondPlaces, ...thirdPlaces];

  await Promise.all(usersPlaces.map(user => contestsData.setContestWinners(user.place, contestId, user.user_id)));
  await Promise.all(usersPlaces.map(user => usersData.insertUserPoints(user.points, user.user_id, contestId)));

  return {
    error: null,
    winners: {
      message: `Winners for contest with id ${contestId} has been set!`,
      winners: usersPlaces,
    },
  };
};

const createContest = contestsData => async (
  title,
  phaseOneLimitDays,
  phaseTwoLimitHours,
  contestCategory,
  contestType,
  startDate,
  is_auto,
) => {
  const nameMatch = await contestsData.getContestByName(title);
  await contestsData.setCategoryId(contestCategory);

  if (nameMatch) {
    return {
      error: serviceErrors.DUPLICATE_RECORD,
      contest: null,
    };
  }

  const createdContest = await contestsData.createContest(
    title,
    phaseOneLimitDays,
    phaseTwoLimitHours,
    contestCategory,
    contestType,
    startDate,
    is_auto,
  );

  createdContest.start_date = moment(createdContest.start_date).format('DD/MM/YYYY');

  return {
    error: null,
    contest: {
      message: `Contest with title ${title} was successfully created!`,
      contest: createdContest,
    },
  };
};

const getContestCategories = contestsData => async () => {
  const categories = await contestsData.getContestCategories();
  if (categories.length === 0) {
    return {
      error: serviceErrors.RECORD_NOT_FOUND,
      categories: null,
    };
  }

  return { error: null, categories: categories };
};

const createContestCover = contestsData => async (contestId, cover, coverRes) => {
  const isCover = await contestsData.getContestCover(contestId);

  if (isCover) {
    return {
      error: serviceErrors.DUPLICATE_RECORD,
      contestCover: null,
    };
  } else {
    return {
      error: null,
      contestCover: await contestsData.createContestCover(contestId, cover, coverRes),
    };
  }
};

const setContestInvitations = contestsData => async (usersIds, contestId) => {
  const isInvitations = await contestsData.getContestInvitations(contestId);

  if (isInvitations[0]) {
    return {
      error: serviceErrors.DUPLICATE_RECORD,
      invitations: null,
    };
  } else {
    await Promise.all(usersIds.map(user => contestsData.setContestInvitations(contestId, user)));
  }

  return {
    error: null,
    invitations: {
      message: `Invitations for contest with id ${contestId} has been set!`,
      invitations: await contestsData.getContestInvitations(contestId),
    },
  };
};

export default {
  getAllContests,
  getContestById,
  setContestJury,
  getContestWinners,
  drawAndSetContestWinners,
  createContest,
  getContestCategories,
  createContestCover,
  setContestInvitations,
};
