export default {
    /** Such a record does not exist (when it is expected to exist) */
    RECORD_NOT_FOUND: 1,
    /** The requirements do not allow more than one of that resource */
    DUPLICATE_RECORD: 2, 
    /** The requirements do not allow such an operation */
    OPERATION_NOT_PERMITTED: 3,
    /** username mismatch */
    INVALID_USERNAME: 4,
    /** password mismatch */
    INVALID_PASSWORD: 5,
    /** resource unavailable */
    RESOURCE_UNAVAILABLE : 6,
    /** book ID mismatch */
    INVALID_BOOK: 7,
};