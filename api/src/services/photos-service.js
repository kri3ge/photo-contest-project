import serviceErrors from './service-errors.js';
import moment from 'moment';

const getContestPhotos = photosData => async (id) => {
    const isPhotos = await photosData.getContestPhotos(id);

    if (!isPhotos[0]) {
        return {
            error: serviceErrors.RECORD_NOT_FOUND,
            contestPhotos: null,
        };
    } else {
        return {
            error: null,
            contestPhotos: isPhotos,
        };
    }
};

const getContestPhotoById = photosData => async (contestId, photoId) => {
    const isPhoto = await photosData.getContestPhotoById(contestId, photoId);

    if (!isPhoto[0]) {
        return {
            error: serviceErrors.RECORD_NOT_FOUND,
            contestPhoto: null,
        };
    }

    isPhoto[0].scores = await photosData.getContestPhotoScore(photoId);
    isPhoto[0].upload_date = moment(isPhoto[0].uploadDate).format('DD/MM/YYYY');

    return {
        error: null,
        contestPhoto: isPhoto,
    };
};

const createPhoto = photosData => async (contestId, userId, filename, resFilename) => {
    const isUserPhoto = await photosData.getUserPhotoByContest(contestId, userId);

    if (isUserPhoto) {
        return {
            error: serviceErrors.OPERATION_NOT_PERMITTED,
            photo: null,
        };
    } else {
        return {
            error: null,
            photo: await photosData.createContestPhoto(contestId, userId, filename, resFilename),
        };
    }
};

const createPhotoDetails = (photosData, usersData) => async (contestId, id, body, userId) => {
    const isPhoto = await photosData.getPhotoById(id, contestId);
    const isDetails = await photosData.getPhotoDetailsById(id);

    if (!isPhoto) {
        return {
            error: serviceErrors.RECORD_NOT_FOUND,
            photo_details: null,
        };
    } else if (isDetails) {
        return {
            error: serviceErrors.OPERATION_NOT_PERMITTED,
            photo_details: null,
        };
    } else {
        const isInvited = await usersData.getInvitedStatus(userId, contestId);
        
        isInvited[0]  
        ? await usersData.insertUserPoints(3, userId, contestId) 
        : await usersData.insertUserPoints(1, userId, contestId);
        
        return {
            error: null,
            photoDetails: await photosData.addDetails(id, body),
        };
    }
};

const createPhotoVote = photosData => async (userId, contestId, photoId, body, role) => {
    const isPhoto = await photosData.getPhotoById(photoId, contestId);
    const isUser = await photosData.getUserVote(userId, photoId);
    const isJury = await photosData.getJuryStatus(userId, contestId);

    if (!isPhoto) {
        return {
            error: serviceErrors.RECORD_NOT_FOUND,
            photo_details: null,
        };
    } else if (isUser) {
        return {
            error: serviceErrors.DUPLICATE_RECORD,
            photo_details: null,
        };
    } else if (!isJury && role !== 'Organizer') {
        return {
            error: serviceErrors.OPERATION_NOT_PERMITTED,
            photo_details: null,
        };
    } else {
        return {
            error: null,
            photoVote: await photosData.juryVoteForPhoto(photoId, body, userId),
        };
    }
};

export default {
    getContestPhotos,
    getContestPhotoById,
    createPhoto,
    createPhotoDetails,
    createPhotoVote,
};
