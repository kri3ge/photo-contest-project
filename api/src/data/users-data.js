import { DEFAULT_USER_ROLE } from '../config.js';
import pool from './pool.js';

const getAll = async () => {
  const sql = `
  SELECT u.id, u.username, u.first_name, u.last_name, u.password, r.role, u.picture, u.member_since, SUM(up.points) user_points
  FROM users u
  JOIN roles r ON u.roles_id = r.id
  LEFT JOIN user_points up ON u.id = up.users_id
  GROUP BY u.id
  `;

  return await pool.query(sql);
};

const getWithAccount = async (username) => {
  const sql = `
  SELECT u.id, u.username, u.first_name, u.last_name, u.password, r.role, picture, member_since
  FROM users u
  JOIN roles r ON u.roles_id = r.id
  WHERE u.username = '${username}';
  `;

  const result = await pool.query(sql, [username]);

  return result[0];
};

const getBy = async (column, value) => {
  // the following 2 operations only work with userId
  const points = await getUserPoints(value);
  const photos = await getUserPhotos(value);
  const sql = `
  SELECT u.id, u.username, u.first_name, u.last_name, u.password, r.role, picture, member_since
  FROM users u
  JOIN roles r ON u.roles_id = r.id
  WHERE u.${column} = '${value}';
  `;

  const result = await pool.query(sql, [value]);
  return { ...result[0], user_points: points['SUM(points)'], photos: photos };
};

const createUser = async (
  username,
  firstName,
  lastName,
  password,
  memberSince,
  picture = 'https://drive.google.com/uc?export=download&id=1npe8ZWpFYHKj1TAyVFZPP-xZSoNEHINB',
  role = DEFAULT_USER_ROLE,
) => {
  const sql = `
  INSERT INTO users
  VALUES (null, ?, ?, ?, ?, (SELECT r.id FROM roles r WHERE r.role = ?), ?, ?);
    `;

  const result = await pool.query(sql, [
    username,
    firstName,
    lastName,
    password,
    role,
    picture,
    memberSince,
  ]);

  return {
    id: result.insertId,
    username: username,
  };
};

const getUserPoints = async (id) => {
  const sql = `
  SELECT SUM(points)
  FROM user_points
  WHERE users_id = ?;
  `;

  const result = await pool.query(sql, [id]);

  return result[0];
};

const getUserPhotos = async (id) => {
  const sql = `
  SELECT p.id, pd.title, pd.story, p.photo, p.photo_res, p.upload_date, p.contests_id, p.users_id, avg(ps.score) score
  FROM photos p
  JOIN photo_details pd ON p.id = pd.photos_id
  LEFT JOIN photo_score ps ON p.id = ps.photos_id
  WHERE p.users_id = ?
  GROUP BY p.id;
  `;

  const result = await pool.query(sql, id);

  return result;
};

const getInvitedStatus = async (userId, contestId) => {
  const sql = `
  SELECT *
  FROM invitations
  WHERE users_id = ? AND contests_id = ?;
  `;

  const result = await pool.query(sql, [userId, contestId]);

  return result;
};

const userRoleUpgrade = async (id, role) => {
  const sql = `
  UPDATE users
  SET roles_id = (SELECT r.id FROM roles r WHERE r.role = ?) + 1
  WHERE id = ?;
  `;

  return await pool.query(sql, [role, id]);
};

const insertUserPoints = async (points, userId, contestId) => {
  const sql = `
  INSERT INTO user_points
  VALUES (null, ?, ?, ?);
  `;

  await pool.query(sql, [points, userId, contestId]);
};

export default {
  getAll,
  getBy,
  createUser,
  getWithAccount,
  getUserPoints,
  getInvitedStatus,
  userRoleUpgrade,
  insertUserPoints,
};
