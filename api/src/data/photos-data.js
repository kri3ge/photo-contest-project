import pool from './pool.js';
import moment from 'moment';

const getContestPhotos = async (id) => {
  const sql = `
  SELECT p.id, pd.title, pd.story, p.photo, p.photo_res, p.upload_date, p.contests_id, p.users_id, avg(ps.score) score
  FROM photos p
  JOIN photo_details pd ON p.id = pd.photos_id
  LEFT JOIN photo_score ps ON p.id = ps.photos_id
  WHERE contests_id = ?
  GROUP BY p.id;
  `;

  const result = await pool.query(sql, id);  
  result.map(photo => photo.upload_date = moment(photo.uploadDate).format('DD/MM/YYYY'));

  return result;
};

const getContestPhotoById = async (contestId, photoId) => {
  const sql = `
  SELECT p.id, pd.title, pd.story, p.photo, p.photo_res, p.upload_date, p.contests_id, p.users_id
  FROM photos p
  JOIN photo_details pd ON p.id = pd.photos_id
  WHERE p.id = ? AND p.contests_id = ?
  `;

  const result = await pool.query(sql, [photoId, contestId]);

  return result;
};

const getContestPhotoScore = async (id) => {
  const sql = `
  SELECT score, comment, users_id
  FROM photo_score
  WHERE photos_id = ?
  `;

  const result = await pool.query(sql, id);

  return result;
};

const createContestPhoto = async (contestId, userId, filename, resFilename) => {
  const sql = `
    INSERT INTO photos
    VALUES
    (null, ?, ?, ?, ?, ?)
    `;

  const result = await pool.query(sql, [
    filename,
    resFilename,
    moment().format('YYYY-MM-DD'),
    contestId,
    userId,
  ]);

  return {
    id: result.insertId,
    photo: filename,
  };
};

const addDetails = async (id, body) => {
  const sql = `
    INSERT INTO photo_details
    VALUES
    (null, ?, ?, ?)
    `;

  await pool.query(sql, [body.title, body.story, id]);

  return {
    photoId: id,
    title: body.title,
    story: body.story,
  };
};

const getPhotoDetailsById = async (id) => {
  const sql = `
  SELECT * 
  FROM photo_details
  WHERE photos_id = ?
  `;

  const result = await pool.query(sql, id);

  return result[0];
};

const getPhotoById = async (id, contestId) => {
  const sql = `
  SELECT * 
  FROM photos
  WHERE id = ? AND contests_id = ?
  `;

  const result = await pool.query(sql, [id, contestId]);

  return result[0];
};

const getUserPhotoByContest = async (contestId, userId) => {
  const sql = `
  SELECT * 
  FROM photos
  WHERE contests_id = ? AND users_id = ?
  `;

  const result = await pool.query(sql, [contestId, userId]);

  return result[0];
};

const juryVoteForPhoto = async (photoId, body, userId) => {
  const sql = `
  INSERT INTO photo_score
  VALUES
  (null, ?, ?, ?, ?)
  `;

  const result = await pool.query(sql, [body.score, body.comment, photoId, userId]);

  return {
    voteId: result.insertId,
    photoId: photoId,
    score: body.score,
    comment: body.comment,
  };
};

const getUserVote = async (userId, photoId) => {
  const sql = `
  SELECT * 
  FROM photo_score
  WHERE users_id = ? AND photos_id =?
  `;

  const result = await pool.query(sql, [userId, photoId]);

  return result[0];
};

const getJuryStatus = async (userId, contestId) => {
  const sql = `
  SELECT * 
  FROM jury
  WHERE users_id = ? AND contests_id = ?
  `;

  const result = await pool.query(sql, [userId, contestId]);

  return result[0];
};

export default {
  getContestPhotos,
  getContestPhotoById,
  getContestPhotoScore,
  createContestPhoto,
  addDetails,
  getPhotoDetailsById,
  getPhotoById,
  getUserPhotoByContest,
  juryVoteForPhoto,
  getUserVote,
  getJuryStatus,
};