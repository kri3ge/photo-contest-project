import contestData from './contest-data.js';

(async () => {
    await contestData.deleteAllData();

    console.log('Done deleting!');

    process.exit(0);
})();
