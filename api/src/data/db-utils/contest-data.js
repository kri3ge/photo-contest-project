import pool from '../pool.js';
import {
  ADMIN_PASSWORD_HASH,
  BORIS_PASSWORD_HASH,
  KRISI_PASSWORD_HASH,
  NADYA_PASSWORD_HASH,
  ROSEN_PASSWORD_HASH,
  STOYAN_PASSWORD_HASH,
  MIRCHEV_PASSWORD_HASH,
  ANCHEV_PASSWORD_HASH,
  MILANOV_PASSWORD_HASH,
  PETROV_PASSWORD_HASH, 
  CHRISTOV_PASSWORD_HASH, 
  MARKOVA_PASSWORD_HASH, 
  HRISIMOVA_PASSWORD_HASH, 
  MINEV_PASSWORD_HASH,
} from '../../config.js';

const uploadContestData = async () => {
  const sql = `
    ALTER table users auto_increment = 1;
    ALTER table roles auto_increment = 1;
    ALTER table user_points auto_increment = 1;
    ALTER table contests auto_increment = 1;
    ALTER table contest_categories auto_increment = 1;
    ALTER table contest_types auto_increment = 1;
    ALTER table contest_winners auto_increment = 1;
    ALTER table contest_winner_places auto_increment = 1;
    ALTER table jury auto_increment = 1;
    ALTER table invitations auto_increment = 1;
    ALTER table covers auto_increment = 1;
    ALTER table photos auto_increment = 1;
    ALTER table photo_score auto_increment = 1;
    ALTER table photo_details auto_increment = 1;

    INSERT INTO roles
    VALUES
    (null, 'Junkie'),
    (null, 'Enthusiast'),
    (null, 'Master'), 
    (null, 'Wise and Benevolent Photo Dictator'),
    (null, 'Organizer');

    INSERT INTO contest_types
    VALUES
    (null, 'Open'),
    (null, 'Invitational');

    INSERT INTO contest_categories
    VALUES
    (null, 'Animals'),
    (null, 'Nature'),
    (null, 'Sport'),
    (null, 'Travel'),
    (null, 'Machines'),
    (null, 'People');

    INSERT INTO contest_winner_places
    VALUES
    (null, 'First'),
    (null, 'Second'),
    (null, 'Third');
        
    INSERT INTO users
    VALUES
    (null, 'edo@telerik.com', 'Edo', 'Evlogiev', '${ADMIN_PASSWORD_HASH}', 5, 'https://drive.google.com/uc?export=download&id=1npe8ZWpFYHKj1TAyVFZPP-xZSoNEHINB', '2020-11-04'),
    (null, 'teo@telerik.com', 'Teo', 'Naidenov', '${KRISI_PASSWORD_HASH}', 2, 'https://drive.google.com/uc?export=download&id=1npe8ZWpFYHKj1TAyVFZPP-xZSoNEHINB', '2020-04-11'),
    (null, 'boris@telerik.com', 'Boris', 'Borisov', '${BORIS_PASSWORD_HASH}', 1, 'https://drive.google.com/uc?export=download&id=1npe8ZWpFYHKj1TAyVFZPP-xZSoNEHINB', '2020-04-11'),
    (null, 'rosen@telerik.com', 'Rosen', 'Urkov', '${ROSEN_PASSWORD_HASH}', 5, 'https://drive.google.com/uc?export=download&id=1npe8ZWpFYHKj1TAyVFZPP-xZSoNEHINB', '2020-05-10'),
    (null, 'nadya@telerik.com', 'Nadya', 'Atanasova', '${NADYA_PASSWORD_HASH}', 5, 'https://drive.google.com/uc?export=download&id=1npe8ZWpFYHKj1TAyVFZPP-xZSoNEHINB', '2020-05-10'),
    (null, 'stoyan@telerik.com', 'Stoyan', 'Peshev', '${STOYAN_PASSWORD_HASH}', 5, 'https://drive.google.com/uc?export=download&id=1npe8ZWpFYHKj1TAyVFZPP-xZSoNEHINB', '2020-10-10'),
    (null, 'mirchev@telerik.com', 'Niki', 'Mirchev', '${MIRCHEV_PASSWORD_HASH}', 3, 'https://drive.google.com/uc?export=download&id=1npe8ZWpFYHKj1TAyVFZPP-xZSoNEHINB', '2020-09-15'),
    (null, 'milanov@telerik.com', 'Niki', 'Milanov', '${MILANOV_PASSWORD_HASH}', 2, 'https://drive.google.com/uc?export=download&id=1npe8ZWpFYHKj1TAyVFZPP-xZSoNEHINB', '2020-09-15'),
    (null, 'petrov@telerik.com', 'Georgi', 'Petrov', '${PETROV_PASSWORD_HASH}', 2, 'https://drive.google.com/uc?export=download&id=1npe8ZWpFYHKj1TAyVFZPP-xZSoNEHINB', '2020-09-15'),
    (null, 'christov@telerik.com', 'Vasil', 'Christov', '${CHRISTOV_PASSWORD_HASH}', 3, 'https://drive.google.com/uc?export=download&id=1npe8ZWpFYHKj1TAyVFZPP-xZSoNEHINB', '2020-09-15'),
    (null, 'markova@telerik.com', 'Maya', 'Markova', '${MARKOVA_PASSWORD_HASH}', 1, 'https://drive.google.com/uc?export=download&id=1npe8ZWpFYHKj1TAyVFZPP-xZSoNEHINB', '2020-09-15'),
    (null, 'hrisimova@telerik.com', 'Nadya', 'Hrisimova', '${HRISIMOVA_PASSWORD_HASH}', 1, 'https://drive.google.com/uc?export=download&id=1npe8ZWpFYHKj1TAyVFZPP-xZSoNEHINB', '2020-09-15'),
    (null, 'minev@telerik.com', 'Niki', 'Minev', '${MINEV_PASSWORD_HASH}', 1, 'https://drive.google.com/uc?export=download&id=1npe8ZWpFYHKj1TAyVFZPP-xZSoNEHINB', '2020-09-15'),
    (null, 'anchev@telerik.com', 'Niki', 'Anchev', '${ANCHEV_PASSWORD_HASH}', 3, 'https://drive.google.com/uc?export=download&id=1npe8ZWpFYHKj1TAyVFZPP-xZSoNEHINB', '2020-09-15');
    
    INSERT INTO contests
    VALUES
    /* id, title, phaseOneLimitDays, PhaseTwoLimitHours, categoryId, typeId, startDate, isAuto */
    (null, 'Get wild or stay home', '20', '24', '1', '1', '2020-11-13', false),
    (null, 'The look of silence', '20', '10', '2', '2', '2020-10-20', false),
    (null, 'Just do it','20', '20', '3', '1', '2020-11-12', false),
    (null, 'Your best friend', '20', '24', '1', '1', '2020-11-10', true),
    (null, 'Style me', '30', '20', '1', '1', '2020-11-03', false),
    (null, 'Checkmate', '25', '20', '3', '1', '2020-09-16', false),
    (null, 'Wintering', '30', '20', '3', '1', '2020-10-28', true),
    (null, 'Around the world', '20', '20', '4', '1', '2020-11-25', true),
    (null, 'Traditions', '15', '15', '4', '2', '2020-11-26', false),
    (null, 'Sea Stories', '7', '15', '2', '1', '2020-11-28', true),
    (null, 'On top of the world', '11', '24', '2', '1', '2020-11-20', true),
    (null, 'Highway to paradise', '30', '24', '4', '2', '2020-12-20', false);

    INSERT INTO covers
    VALUES
    (null, 'orig-coverImg-1.jpg', 'res-coverImg-1.jpg', '1'),
    (null, 'orig-coverImg-2.jpg', 'res-coverImg-2.jpg', '2'),
    (null, 'orig-coverImg-3.jpg', 'res-coverImg-3.jpg', '3'),
    (null, 'orig-coverImg-4.jpg', 'res-coverImg-4.jpg', '4'),
    (null, 'orig-coverImg-5.jpg', 'res-coverImg-5.jpg', '5'),
    (null, 'orig-coverImg-6.jpg', 'res-coverImg-6.jpg', '6'),
    (null, 'orig-coverImg-7.jpg', 'res-coverImg-7.jpg', '7'),
    (null, 'orig-coverImg-8.jpg', 'res-coverImg-8.jpg', '8'),
    (null, 'orig-coverImg-9.jpg', 'res-coverImg-9.jpg', '9'),
    (null, 'orig-coverImg-10.jpg', 'res-coverImg-10.jpg', '10'),
    (null, 'orig-coverImg-11.jpg', 'res-coverImg-11.jpg', '11'),
    (null, 'orig-coverImg-12.jpg', 'res-coverImg-12.jpg', '12');

    INSERT INTO user_points
    VALUES
    /* id, points, userId, contestId */
    (null, 77, 2, 2),
    (null, 33, 3, 2),
    (null, 168, 7, 2),
    (null, 61, 8, 2),
    (null, 58, 9, 2),
    (null, 182, 10, 2),
    (null, 7, 11, 2),
    (null, 21, 12, 2),
    (null, 8, 13, 2),
    (null, 220, 14, 2);

    INSERT INTO jury
    VALUES
    /* id, users_id, contests_id */
    (null, '1', '1'),
    (null, '1', '2'),
    (null, '1', '3'),
    (null, '1', '4'),
    (null, '1', '5'),
    (null, '1', '6'),
    (null, '1', '7'),
    (null, '1', '8'),
    (null, '1', '9'),
    (null, '1', '10'),
    (null, '1', '11'),
    (null, '4', '1'),
    (null, '4', '2'),
    (null, '4', '3'),
    (null, '4', '4'),
    (null, '4', '5'),
    (null, '4', '6'),
    (null, '4', '7'),
    (null, '4', '8'),
    (null, '4', '9'),
    (null, '4', '10'),
    (null, '4', '11'),
    (null, '5', '1'),
    (null, '5', '2'),
    (null, '5', '3'),
    (null, '5', '4'),
    (null, '5', '5'),
    (null, '5', '6'),
    (null, '5', '7'),
    (null, '5', '8'),
    (null, '5', '9'),
    (null, '5', '10'),
    (null, '5', '11'),
    (null, '6', '1'),
    (null, '6', '2'),
    (null, '6', '3'),
    (null, '6', '4'),
    (null, '6', '5'),
    (null, '6', '6'),
    (null, '6', '7'),
    (null, '6', '8'),
    (null, '6', '9'),
    (null, '6', '10'),
    (null, '6', '11');
    
    INSERT INTO photos
    VALUES
    /* id, photo, photo_res, uploadDate, contestsId, usersId */
    (null, 'orig-contestImage-1605461247532.jpg', 'res-contestImage-1605461247549.jpg', '2020-11-01', '2', '2'),
    (null, 'orig-contestImage-1605467503189.jpg', 'res-contestImage-1605467503212.jpg', '2020-11-02', '2', '3'),
    (null, 'orig-contestImage-1605467658721.jpg', 'res-contestImage-1605467658743.jpg', '2020-11-04', '2', '7'),
    (null, 'orig-contestImage-1605467370709.jpg', 'res-contestImage-1605467370730.jpg', '2020-11-01', '2', '8'),
    (null, 'orig-contestImage-1606055585362.jpg', 'res-contestImage-1606055585385.jpg', '2020-11-10', '4', '2'),
    (null, 'orig-contestImage-1606122536270.jpg', 'res-contestImage-1606122538557.jpg', '2020-11-22', '1', '8'),
    (null, 'orig-contestImage-1606124281058.jpg', 'res-contestImage-1606124281148.jpg', '2020-11-28', '1', '14'),
    (null, 'orig-contestImage-1606124232056.jpg', 'res-contestImage-1606124232109.jpg', '2020-11-28', '1', '13'),
    (null, 'orig-contestImage-1606124182480.jpg', 'res-contestImage-1606124182633.jpg', '2020-11-25', '1', '12'),
    (null, 'orig-contestImage-1606124112522.jpg', 'res-contestImage-1606124112553.jpg', '2020-11-22', '1', '11'),
    (null, 'orig-contestImage-1606124045173.jpg', 'res-contestImage-1606124045233.jpg', '2020-11-16', '1', '10'),
    (null, 'orig-contestImage-1606123976408.jpg', 'res-contestImage-1606123976516.jpg', '2020-11-18', '1', '9'),
    (null, 'orig-contestImage-1606123215719.jpg', 'res-contestImage-1606123215742.jpg', '2020-11-18', '1', '3'),
    (null, 'orig-contestImage-1606123143906.jpg', 'res-contestImage-1606123144049.jpg', '2020-11-19', '1', '2'),
    (null, 'orig-contestImage-1606122769084.jpg', 'res-contestImage-1606122769148.jpg', '2020-11-23', '1', '7'),
    (null, 'orig-contestImage-1606128124779.jpg', 'res-contestImage-1606128124807.jpg', '2020-11-22', '5', '2'),
    (null, 'orig-contestImage-1606128256748.jpg', 'res-contestImage-1606128256769.jpg', '2020-11-22', '5', '3'),
    (null, 'orig-contestImage-1606128314690.jpg', 'res-contestImage-1606128314710.jpg', '2020-11-22', '5', '7'),
    (null, 'orig-contestImage-1606128387786.jpg', 'res-contestImage-1606128387825.jpg', '2020-11-22', '5', '8'),
    (null, 'orig-contestImage-1606128438107.jpg', 'res-contestImage-1606128438125.jpg', '2020-11-22', '5', '9'),
    (null, 'orig-contestImage-1606128482171.jpg', 'res-contestImage-1606128482183.jpg', '2020-11-22', '5', '10'),
    (null, 'orig-contestImage-1606128528697.jpg', 'res-contestImage-1606128528711.jpg', '2020-11-22', '5', '11'),
    (null, 'orig-contestImage-1606128566037.jpg', 'res-contestImage-1606128566059.jpg', '2020-11-22', '5', '12'),
    (null, 'orig-contestImage-1606128609818.jpg', 'res-contestImage-1606128609889.jpg', '2020-11-22', '5', '13'),
    (null, 'orig-contestImage-1606128665701.jpg', 'res-contestImage-1606128665724.jpg', '2020-11-22', '5', '14'),
    (null, 'orig-contestImage-1606131588592.jpg', 'res-contestImage-1606131588629.jpg', '2020-11-22', '4', '3'),
    (null, 'orig-contestImage-1606130765628.jpg', 'res-contestImage-1606130765648.jpg', '2020-11-22', '4', '7'),
    (null, 'orig-contestImage-1606130805739.jpg', 'res-contestImage-1606130805764.jpg', '2020-11-22', '4', '8'),
    (null, 'orig-contestImage-1606130868236.jpg', 'res-contestImage-1606130868259.jpg', '2020-11-22', '4', '9'),
    (null, 'orig-contestImage-1606130904235.jpg', 'res-contestImage-1606130904258.jpg', '2020-11-22', '4', '10'),
    (null, 'orig-contestImage-1606130939529.jpg', 'res-contestImage-1606130939550.jpg', '2020-11-22', '4', '11'),
    (null, 'orig-contestImage-1606130981859.jpg', 'res-contestImage-1606130981877.jpg', '2020-11-22', '4', '12'),
    (null, 'orig-contestImage-1606131029618.jpg', 'res-contestImage-1606131029664.jpg', '2020-11-22', '4', '13'),
    (null, 'orig-contestImage-1606131085661.jpg', 'res-contestImage-1606131085707.jpg', '2020-11-22', '4', '14'),
    (null, 'orig-contestImage-1606133201506.jpg', 'res-contestImage-1606133201538.jpg', '2020-11-22', '3', '2'),
    (null, 'orig-contestImage-1606133244427.jpg', 'res-contestImage-1606133244446.jpg', '2020-11-22', '3', '3'),
    (null, 'orig-contestImage-1606133290874.jpg', 'res-contestImage-1606133290913.jpg', '2020-11-22', '3', '7'),
    (null, 'orig-contestImage-1606133333586.jpg', 'res-contestImage-1606133333606.jpg', '2020-11-22', '3', '8'),
    (null, 'orig-contestImage-1606133370359.jpg', 'res-contestImage-1606133370383.jpg', '2020-11-22', '3', '9'),
    (null, 'orig-contestImage-1606133412260.jpg', 'res-contestImage-1606133412291.jpg', '2020-11-22', '3', '10'),
    (null, 'orig-contestImage-1606133455589.jpg', 'res-contestImage-1606133455609.jpg', '2020-11-22', '3', '11'),
    (null, 'orig-contestImage-1606133499487.jpg', 'res-contestImage-1606133499501.jpg', '2020-11-22', '3', '12'),
    (null, 'orig-contestImage-1606133537855.jpg', 'res-contestImage-1606133537906.jpg', '2020-11-22', '3', '13'),
    (null, 'orig-contestImage-1606133581853.jpg', 'res-contestImage-1606133581887.jpg', '2020-11-22', '3', '14'),
    (null, 'orig-contestImage-1606140979746.jpg', 'res-contestImage-1606140979775.jpg', '2020-11-22', '2', '9'),
    (null, 'orig-contestImage-1606141047642.jpg', 'res-contestImage-1606141047659.jpg', '2020-11-22', '2', '10'),
    (null, 'orig-contestImage-1606141089965.jpg', 'res-contestImage-1606141089979.jpg', '2020-11-22', '2', '11'),
    (null, 'orig-contestImage-1606141130163.jpg', 'res-contestImage-1606141130222.jpg', '2020-11-22', '2', '12'),
    (null, 'orig-contestImage-1606141171464.jpg', 'res-contestImage-1606141171473.jpg', '2020-11-22', '2', '13'),
    (null, 'orig-contestImage-1606141208103.jpg', 'res-contestImage-1606141208124.jpg', '2020-11-22', '2', '14'),
    (null, 'orig-contestImage-1606148938397.jpg', 'res-contestImage-1606148938414.jpg', '2020-11-22', '6', '2'),
    (null, 'orig-contestImage-1606148995533.jpg', 'res-contestImage-1606148995593.jpg', '2020-11-22', '6', '3'),
    (null, 'orig-contestImage-1606149051861.jpg', 'res-contestImage-1606149051896.jpg', '2020-11-22', '6', '7'),
    (null, 'orig-contestImage-1606149087424.jpg', 'res-contestImage-1606149087439.jpg', '2020-11-22', '6', '8'),
    (null, 'orig-contestImage-1606149135122.jpg', 'res-contestImage-1606149135143.jpg', '2020-11-22', '6', '9'),
    (null, 'orig-contestImage-1606149172350.jpg', 'res-contestImage-1606149172369.jpg', '2020-11-22', '6', '10'),
    (null, 'orig-contestImage-1606149215592.jpg', 'res-contestImage-1606149215607.jpg', '2020-11-22', '6', '11'),
    (null, 'orig-contestImage-1606149255589.jpg', 'res-contestImage-1606149255630.jpg', '2020-11-22', '6', '12'),
    (null, 'orig-contestImage-1606149302814.jpg', 'res-contestImage-1606149302842.jpg', '2020-11-22', '6', '13'),
    (null, 'orig-contestImage-1606149342178.jpg', 'res-contestImage-1606149342209.jpg', '2020-11-22', '6', '14'),
    (null, 'orig-contestImage-1606151585292.jpg', 'res-contestImage-1606151585304.jpg', '2020-11-22', '7', '2'),
    (null, 'orig-contestImage-1606152480065.jpg', 'res-contestImage-1606152480089.jpg', '2020-11-22', '7', '3'),
    (null, 'orig-contestImage-1606152526046.jpg', 'res-contestImage-1606152526069.jpg', '2020-11-22', '7', '7'),
    (null, 'orig-contestImage-1606152562764.jpg', 'res-contestImage-1606152562799.jpg', '2020-11-22', '7', '8'),
    (null, 'orig-contestImage-1606152853364.jpg', 'res-contestImage-1606152853386.jpg', '2020-11-22', '7', '9'),
    (null, 'orig-contestImage-1606153066922.jpg', 'res-contestImage-1606153066948.jpg', '2020-11-22', '7', '10'),
    (null, 'orig-contestImage-1606153367156.jpg', 'res-contestImage-1606153367173.jpg', '2020-11-22', '7', '11'),
    (null, 'orig-contestImage-1606153590868.jpg', 'res-contestImage-1606153590908.jpg', '2020-11-22', '7', '12'),
    (null, 'orig-contestImage-1606153748081.jpg', 'res-contestImage-1606153750320.jpg', '2020-11-22', '7', '13'),
    (null, 'orig-contestImage-1606153925312.jpg', 'res-contestImage-1606153925321.jpg', '2020-11-22', '7', '14'),
    (null, 'orig-contestImage-1606583333218.jpg', 'res-contestImage-1606583333231.jpg', '2020-11-28', '10', '9'),
    (null, 'orig-contestImage-1606584077706.jpg', 'res-contestImage-1606584077740.jpg', '2020-11-28', '10', '10'),
    (null, 'orig-contestImage-1606584355625.jpg', 'res-contestImage-1606584355649.jpg', '2020-11-29', '10', '11'),
    (null, 'orig-contestImage-1606584637745.jpg', 'res-contestImage-1606584637776.jpg', '2020-11-28', '10', '12');

    INSERT INTO photo_details
    VALUES
    (null, 'Stillness speaks', 'What can be more relaxing than a still, clear water? And in the same time there is so much life in it. I can literally stay hours watching the calm lake in Pancharevo, especially on sunset when the water has these perfect firing nuances', '1'),
    (null, 'Equilibrity', 'Everything looks so perfect in that moment when the drop touches the surface of the water. It makes me feel so calm and full with life', '2'),
    (null, 'Wooden island', 'This year we decided to spend our holidays in this heaven of Earth. This was the view I was waking up with every morning. Nature is amazing', '3'),
    (null, 'Lost Lake', 'Spent the whole night in absolute silence at Lost Lake(US) in the Uinta mountains watching the milky way roll across the horizon. This was taken just after the sun had gone down and the sky was still slightly blue', '4'),
    (null, 'When enemies become friends', 'These Latvian cuties use to hate each other, now they are best friends. We should all learn from their wisdom', '5'),
    (null, 'Beauty nap', 'Red panda lying on tree branch', '6'),
    (null, 'Staying tall', 'Brown giraffes on brown grass field during daytime', '7'),
    (null, 'Cattle', 'Brown and black highlander cattles on green grass', '8'),
    (null, 'Sea life', 'Person holding black hair brush', '9'),
    (null, 'Goat', 'Mountain goat standing alone an staring at the beautiful landscape', '10'),
    (null, 'Ostrich', 'Curious ostrich on green grasses', '11'),
    (null, 'I\\'m bored', 'Black young elephant walking beside the trees', '12'),
    (null, 'Cuteness overload', 'White and brown squirrel on brown wood during daytime', '13'),
    (null, 'Grace', 'Graceful brown deer in its natural habitat', '14'),
    (null, 'Beak', 'Brown bird with intriguing hairstyle', '15'),
    (null, 'Drowsy', 'Pug covered with blanket on bedspead', '16'), 
    (null, 'Woof woof backpack', 'Woman walking near rock formations in front of sea during daytime', '17'), 
    (null, 'Shades', 'Woman sitting beside the basset hound', '18'), 
    (null, 'Don\\'t look back', 'Woman sitting near dog on cliff overlooking mountains and forest', '19'), 
    (null, 'Konichiwa', 'Long coated white dog dressed in Japanese style', '20'), 
    (null, 'Urban', 'Short-coated tan and white dog with green zip-up jacket', '21'), 
    (null, 'Gangsta-gangsta', 'White and brown puppy wearing green shirt and golden chain', '22'), 
    (null, 'What?', 'French buldog wearing shirt and hat', '23'), 
    (null, 'Dancing in the rain', 'Two dogs standing in raincoats', '24'), 
    (null, 'Coziness', 'Couple sitting on sofa beside stylish dog', '25'),
    (null, 'Fluffy furniture', 'Brown white and black long coated dog lying on the floor', '26'),
    (null, 'Contentment', 'Man hoding his dog\\'s head in his hand', '27'),
    (null, 'Look me in the eyes', 'Woman sitting on rock with dog', '28'),
    (null, 'On the bridge of friendship', 'Man standing near golden labrador retriever viewing bridge and high-rise buildings', '29'),
    (null, 'Peace', 'Woman hugging black dog', '30'),
    (null, '1-2-3 smile', 'Two brown dogs posing for picture', '31'),
    (null, 'Friend\\'s for life', 'Toddler hugging siberian husky', '32'),
    (null, 'Come here', 'Woman about to kiss a dog', '33'),
    (null, 'Make faces', 'Woman carrying and kissing dog', '34'), 
    (null, 'Wrestling', 'Girl and boy doing Brazilian jiu-jitsu', '35'), 
    (null, 'Score!', 'Women playing volleyball inside court', '36'), 
    (null, 'Clean jerk', 'Man holding barbel ready to lift', '37'), 
    (null, 'Alone on the tennis court', 'Green tennis ball on brown soil', '38'), 
    (null, 'Time to break', 'Woman riding kayak at the middle of the sea', '39'), 
    (null, 'Rugby', 'Athletes playin rugby fighting over the ball', '40'), 
    (null, 'Trekking + nature', 'Two people trekking in the mountain', '41'), 
    (null, 'Under the wave', 'Time lapse photography surfer in wave water', '42'), 
    (null, 'Giro', 'Group of cyclist on asphalt road', '43'), 
    (null, 'Butterfly', 'Man doing butterfly stroke', '44'), 
    (null, 'Idleness', 'Empty brown wooden armchairs', '45'),
    (null, 'Pine forrest', 'Pine trees covered with fog', '46'),
    (null, 'Water vapor', 'Green and white boat on body of water during daytime', '47'),
    (null, 'Crowns', 'Low-angle photography of tree', '48'),
    (null, 'Breakfast', 'White and brown disposable cup on white tissue paper', '49'),
    (null, 'Frozen', 'Close-up photography of frozen dock', '50'),
    (null, 'Afternoon', 'Old men playing chess outside', '51'),
    (null, 'About to move', 'Man in black jacket and black hat holding chess piece', '52'),
    (null, 'Queening', 'Shallow focus photo of men playing chess', '53'),
    (null, 'Seaside chessboard', 'Man in grey jacket playing street chess', '54'),
    (null, 'Openning', 'Playing chess in the park', '55'),
    (null, 'Board', 'Biege and brown chess board set', '56'),
    (null, 'Tournament', 'Empty chessboard table on tournament', '57'),
    (null, 'Thoughtful', 'Two men playing chess for fun', '58'),
    (null, 'One man army', 'Man in blue dress shirt playing chess game by himself', '59'),
    (null, 'Victory', 'Grays-scale photo of two boys playing chess', '60'),
    (null, 'Mountain selfie', 'Man standing in front of the sun holding selfie stick', '61'),
    (null, 'Slope run', 'Man riding greenboard on downhill during snowtime', '62'),
    (null, 'Glide', 'Woman wearing skate shoes inside ice skating rink', '63'),
    (null, 'Brake pedal to the floor', 'Man snowboarding in the mountain', '64'),
    (null, 'Street curling', 'Man sliding stone on a sheet of ice', '65'),
    (null, 'Hockey with best friend', 'Man playing ice hockey with his dog', '66'),
    (null, 'Snowmobiling', 'Snowmobile speeding on snow near tree lines', '67'),
    (null, 'Hardtrail', 'Hardtrail mountain bike in snow covered forest', '68'),
    (null, 'Winter isolation', 'Skier entering a snow forest', '69'),
    (null, 'Breathtaking', 'Ski jump on spectacular cityscape', '70'),
    (null, 'Forgotten Glory', 'Do you remember the time when ships were dominating the world? When they helped the world discover lands they didn\\'t imagine existed...', '71'),
    (null, 'The Fisherman', 'Sometimes a very lonely adventure, it requires so much calm and patience. But who doesn\\'t like spending hours in the deep sea.', '72'),
    (null, 'Beachy paradise', 'I personally prefer spending my holidays contemplating the clear water is places like this one. I took this picture in the Maldives last July.', '73'),
    (null, 'Sunsets to remember', 'I used to spend hours in the evening with my two best friends on this exact spot. This sight gives me so much joy, I don\\'t need anything else to be happy.', '74');
    
    INSERT INTO photo_score
    VALUES
    /* id, score, comment, photosid, usersid */
    (null, '9', 'Gorgeous', '27', '4'),
    (null, '10', 'What a cutie. The moment was brilliantly captured!', '27', '5'),
    (null, '10', 'My favourite photo from the whole contest! Well deserved maximum points.', '27', '6'),
    (null, '8', 'Good angle. Looks like the dog loves your presence.', '27', '1'),
    (null, '7', 'Perfect', '32', '1'),
    (null, '8', 'Good angle. Looks like the dog loves your presence.', '33', '4'),
    (null, '5', 'The quality should be better.', '34', '5'),
    (null, '6', 'Not bad.', '34', '6'),
    (null, '9', 'I love it.', '7', '1'),
    (null, '10', 'I love it.', '8', '1'),
    (null, '7', 'Good job.', '9', '1'),
    (null, '3', 'Not my type of photo.', '10', '1'),
    (null, '6', 'Well done.', '10', '5'),
    (null, '3', 'Well done.', '11', '5'),
    (null, '6', 'Well done.', '12', '5'),
    (null, '10', 'I love it! Completely.', '13', '5'),
    (null, '7', 'Well done.', '14', '5'),
    (null, '5', 'Well done.', '15', '5'),
    (null, '5', 'Well done.', '16', '5'),
    (null, '8', 'You look like a professional.', '17', '5'),
    (null, '8', 'Well done.', '18', '5'),
    (null, '9', 'Gorgeous. You look like a professional.', '19', '4'),
    (null, '8', 'Cutie.', '20', '4'),
    (null, '10', 'Lovely, lovely!!!.', '21', '4'),
    (null, '4', 'You should practice more.', '22', '4'),
    (null, '5', 'It is ok, but there are better competitors.', '23', '4'),
    (null, '6', 'It is ok.', '24', '4'),
    (null, '7', 'It is ok, but there are better competitors.', '25', '4'),
    (null, '10', 'My favorite photo from this contest.', '26', '4'),
    (null, '8', 'I like it..a lot.', '28', '4'),
    (null, '7', 'It is ok, but there are better competitors.', '29', '4'),
    (null, '9', 'It is ok, but there are better competitors.', '30', '4'),
    (null, '1', '! point for the effort.', '31', '4'),
    (null, '5', 'It is ok.', '32', '4'),
    (null, '6', 'Good catch.', '33', '6'),
    (null, '2', 'It is ok, but there are better competitors.', '34', '4'),
    (null, '3', 'I don\\'t understand your art.', '10', '6');
    `;
  return await pool.query(sql);
};

const deleteAllData = async () => {
  const sql = `
  DELETE FROM contest_winners WHERE id > 0; 
  DELETE FROM invitations WHERE id > 0; 
  DELETE FROM photo_score WHERE id > 0; 
  DELETE FROM photo_details WHERE id > 0;
  DELETE FROM covers WHERE id > 0;  
  DELETE FROM photos WHERE id > 0;
  DELETE FROM jury WHERE id > 0;
  DELETE FROM user_points WHERE id > 0;
  DELETE FROM users WHERE id > 0;
  DELETE FROM roles WHERE id > 0;
  DELETE FROM contests WHERE id > 0; 
  DELETE FROM contest_categories WHERE id > 0;
  DELETE FROM contest_types WHERE id > 0;
  DELETE FROM contest_winner_places WHERE id > 0;    
  DELETE FROM photo_score WHERE id > 0;    
    `;

  return await pool.query(sql);
};

export default {
  uploadContestData,
  deleteAllData,
};
