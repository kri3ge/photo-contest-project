import contestData from './contest-data.js';

(async () => {
    await contestData.uploadContestData();

    console.log('Done populating!');

    process.exit(0);
})();
