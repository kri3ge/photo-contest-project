-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema contest
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema contest
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `contest` DEFAULT CHARACTER SET utf8 ;
USE `contest` ;

-- -----------------------------------------------------
-- Table `contest`.`roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `contest`.`roles` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `role` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `contest`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `contest`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` NVARCHAR(45) NULL,
  `first_name` NVARCHAR(45) NOT NULL,
  `last_name` NVARCHAR(45) NOT NULL,
  `password` MEDIUMTEXT NOT NULL,
  `roles_id` INT NOT NULL,
  `picture` MEDIUMTEXT NULL,
  `member_since` DATE NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_users_roles_idx` (`roles_id` ASC) VISIBLE,
  CONSTRAINT `fk_users_roles`
    FOREIGN KEY (`roles_id`)
    REFERENCES `contest`.`roles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `contest`.`contest_categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `contest`.`contest_categories` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `category` NVARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `contest`.`contest_types`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `contest`.`contest_types` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(45) NOT NULL COMMENT 'open or invitational',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `contest`.`contests`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `contest`.`contests` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` NVARCHAR(45) NOT NULL,
  `phase1_limit_days` INT NOT NULL COMMENT 'time in days',
  `phase2_limit_hours` INT NOT NULL,
  `contest_categories_id` INT NOT NULL,
  `contest_types_id` INT NOT NULL,
  `start_date` DATE NOT NULL,
  `is_auto` TINYINT ZEROFILL NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `title_UNIQUE` (`title` ASC) VISIBLE,
  INDEX `fk_contests_contest_categories1_idx` (`contest_categories_id` ASC) VISIBLE,
  INDEX `fk_contests_contest_types1_idx` (`contest_types_id` ASC) VISIBLE,
  CONSTRAINT `fk_contests_contest_categories1`
    FOREIGN KEY (`contest_categories_id`)
    REFERENCES `contest`.`contest_categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_contests_contest_types1`
    FOREIGN KEY (`contest_types_id`)
    REFERENCES `contest`.`contest_types` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `contest`.`jury`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `contest`.`jury` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `users_id` INT NOT NULL,
  `contests_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_jury_users1_idx` (`users_id` ASC) VISIBLE,
  INDEX `fk_jury_contests1_idx` (`contests_id` ASC) VISIBLE,
  CONSTRAINT `fk_jury_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `contest`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_jury_contests1`
    FOREIGN KEY (`contests_id`)
    REFERENCES `contest`.`contests` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `contest`.`user_points`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `contest`.`user_points` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `points` INT ZEROFILL NULL,
  `users_id` INT NOT NULL,
  `contests_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_user_points_users1_idx` (`users_id` ASC) VISIBLE,
  INDEX `fk_user_points_contests1_idx` (`contests_id` ASC) VISIBLE,
  CONSTRAINT `fk_user_points_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `contest`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_points_contests1`
    FOREIGN KEY (`contests_id`)
    REFERENCES `contest`.`contests` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `contest`.`photos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `contest`.`photos` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `photo` MEDIUMTEXT NOT NULL,
  `photo_res` MEDIUMTEXT NOT NULL,
  `upload_date` DATE NOT NULL,
  `contests_id` INT NOT NULL,
  `users_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_photos_contests1_idx` (`contests_id` ASC) VISIBLE,
  INDEX `fk_photos_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_photos_contests1`
    FOREIGN KEY (`contests_id`)
    REFERENCES `contest`.`contests` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_photos_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `contest`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `contest`.`photo_score`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `contest`.`photo_score` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `score` INT NULL,
  `comment` NVARCHAR(1500) NOT NULL,
  `photos_id` INT NOT NULL,
  `users_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_photo_score_photos1_idx` (`photos_id` ASC) VISIBLE,
  INDEX `fk_photo_score_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_photo_score_photos1`
    FOREIGN KEY (`photos_id`)
    REFERENCES `contest`.`photos` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_photo_score_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `contest`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `contest`.`invitations`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `contest`.`invitations` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `contests_id` INT NOT NULL,
  `users_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_invitations_contests1_idx` (`contests_id` ASC) VISIBLE,
  INDEX `fk_invitations_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_invitations_contests1`
    FOREIGN KEY (`contests_id`)
    REFERENCES `contest`.`contests` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_invitations_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `contest`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `contest`.`covers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `contest`.`covers` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `cover_img` MEDIUMTEXT NOT NULL,
  `cover_img_res` MEDIUMTEXT NOT NULL,
  `contests_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_covers_contests1_idx` (`contests_id` ASC) VISIBLE,
  CONSTRAINT `fk_covers_contests1`
    FOREIGN KEY (`contests_id`)
    REFERENCES `contest`.`contests` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `contest`.`photo_details`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `contest`.`photo_details` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` NVARCHAR(45) NOT NULL,
  `story` NVARCHAR(1500) NOT NULL,
  `photos_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_photo_details_photos1_idx` (`photos_id` ASC) VISIBLE,
  CONSTRAINT `fk_photo_details_photos1`
    FOREIGN KEY (`photos_id`)
    REFERENCES `contest`.`photos` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `contest`.`contest_winner_places`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `contest`.`contest_winner_places` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `place` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `contest`.`contest_winners`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `contest`.`contest_winners` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `contests_id` INT NOT NULL,
  `users_id` INT NOT NULL,
  `contest_winner_place_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_contest_winners_contests1_idx` (`contests_id` ASC) VISIBLE,
  INDEX `fk_contest_winners_users1_idx` (`users_id` ASC) VISIBLE,
  INDEX `fk_contest_winners_contest_winner_place1_idx` (`contest_winner_place_id` ASC) VISIBLE,
  CONSTRAINT `fk_contest_winners_contests1`
    FOREIGN KEY (`contests_id`)
    REFERENCES `contest`.`contests` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_contest_winners_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `contest`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_contest_winners_contest_winner_place1`
    FOREIGN KEY (`contest_winner_place_id`)
    REFERENCES `contest`.`contest_winner_places` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
