import pool from './pool.js';
import moment from 'moment';

const getAllContests = async () => {
  const sql = `
    SELECT c.id, c.title, c.phase1_limit_days PhaseOneLimitDays, c.phase2_limit_hours PhaseTwoLimitHours, ca.category, ct.type, c.start_date, c.is_auto, co.cover_img, co.cover_img_res
    FROM contests c
    JOIN contest_categories ca ON c.contest_categories_id = ca.id 
    JOIN contest_types ct ON c.contest_types_id = ct.id 
    JOIN covers co ON c.id = co.contests_id 
    ORDER BY c.id;
    `;

  const result = await pool.query(sql);
  return result;
};

const getContestById = async (id) => {
  const photos = await getPhotosByContestId(id);
  const sql = `
  SELECT c.id, c.title, c.phase1_limit_days PhaseOneLimitDays, c.phase2_limit_hours PhaseTwoLimitHours, ca.category, ct.type, c.start_date, c.is_auto, co.cover_img, co.cover_img_res
  FROM contests c
  JOIN contest_categories ca ON c.contest_categories_id = ca.id 
  JOIN contest_types ct ON c.contest_types_id = ct.id
  JOIN covers co ON c.id = co.contests_id 
  WHERE c.id = ?`;

  const result = await pool.query(sql, [id]);
  return { ...result[0], photos: photos };
};

const getContestByIdOnly = async (id) => {
  const photos = await getPhotosByContestId(id);
  const sql = `
  SELECT c.id, c.title, c.phase1_limit_days PhaseOneLimitDays, c.phase2_limit_hours PhaseTwoLimitHours, ca.category, ct.type, c.start_date, c.is_auto
  FROM contests c
  JOIN contest_categories ca ON c.contest_categories_id = ca.id 
  JOIN contest_types ct ON c.contest_types_id = ct.id
  WHERE c.id = ?`;

  const result = await pool.query(sql, [id]);
  return { ...result[0], photos: photos };
};

const getPhotosByContestId = async (id) => {
  const sql = `
  SELECT p.id, p.users_id user_id, pd.title, pd.story, p.photo, p.photo_res, p.upload_date, AVG(ps.score) score
  FROM photos p
  JOIN photo_details pd ON p.id = pd.photos_id
  LEFT JOIN photo_score ps ON p.id = ps.photos_id
  WHERE p.contests_id = ?
  GROUP BY p.photo;
  `;

  const result = await pool.query(sql, id);
  result.map(photo => photo.upload_date = moment(photo.uploadDate).format('DD/MM/YYYY'));

  return result;
};

const setContestJury = async (contestId, userId) => {
  const sql = `
  INSERT INTO jury
  VALUES 
  (null, ?, ?);
  `;

  await pool.query(sql, [userId, contestId]);
};

const getContestJury = async (id) => {
  const sql = `
  SELECT * 
  FROM jury
  WHERE contests_id = ?;
  `;

  return await pool.query(sql, id);
};

const setContestWinners = async (place, contestId, userId) => {
  const sql = `
  INSERT INTO contest_winners
  VALUES 
  (null, ?, ?, (SELECT cwp.id from contest_winner_places cwp WHERE cwp.place = ?));
  `;

  await pool.query(sql, [contestId, userId, place]);
};

const getContestWinners = async (id) => {
  const sql = `
  SELECT cw.contests_id, cw.users_id, cw.contest_winner_place_id place, cwp.place placeStr, u.first_name, u.last_name, u.picture
  FROM contest_winners cw
  JOIN contest_winner_places cwp ON cw.contest_winner_place_id = cwp.id
  JOIN users u ON cw.users_id = u.id
  WHERE cw.contests_id = ?;
  `;

  return await pool.query(sql, id);
};

const createContest = async (
  title,
  phaseOneLimitDays,
  phaseTwoLimitHours,
  contestCategory,
  contestType,
  startDate,
  is_auto,
) => {

  const sql = `
    INSERT INTO contests
    VALUES
    (null, ?, ?, ?, (SELECT cc.id from contest_categories cc WHERE cc.category = ?), (SELECT ct.id from contest_types ct WHERE ct.type = ?), ?, ?);
    `;

  await pool.query(sql, [
    title,
    phaseOneLimitDays,
    phaseTwoLimitHours,
    contestCategory,
    contestType,
    startDate,
    is_auto,
  ]);

  return await getContestByName(title);
};

const getContestByName = async (name) => {
  const sql = `
  SELECT c.id, c.title, c.phase1_limit_days PhaseOneLimitDays, c.phase2_limit_hours PhaseTwoLimitHours, ca.category, ct.type, c.start_date, c.is_auto
  FROM contests c
  JOIN contest_categories ca ON c.contest_categories_id = ca.id 
  JOIN contest_types ct ON c.contest_types_id = ct.id 
  WHERE c.title = ?
  `;

  const result = await pool.query(sql, [name]);
  return result[0];
};

const setCategoryId = async (contestCategory) => {
  const sql = `
  SELECT id
  FROM contest_categories
  WHERE category = ?;`;
  let foundCategory = await pool.query(sql, [contestCategory]);

  if (foundCategory.length === 0) {
    const sql2 = `
  INSERT INTO contest_categories
  VALUES
  (null, ?);`;
    await pool.query(sql2, [contestCategory]);

    const sql3 = `
  SELECT id
  FROM contest_categories
  WHERE category = ?;`;
    foundCategory = await pool.query(sql3, [contestCategory]);
  }

  return foundCategory[0];
};

const getContestCategories = async () => {
  const sql = `
  SELECT *
  FROM contest_categories
  ORDER BY category;
  `;

  return await pool.query(sql);
};

const createContestCover = async (contestId, cover, coverRes) => {
  const sql = `
  INSERT INTO covers
  VALUES
  (null, ?, ?, ?);
  `;

  const result = await pool.query(sql, [cover, coverRes, contestId]);

  return {
    id: result.insertId,
    cover: cover,
    coverRes: coverRes,
  };
};

const getContestCover = async (id) => {
  const sql = `
  SELECT *
  FROM covers
  WHERE contests_id = ?;
  `;
  const result = await pool.query(sql, id);

  return result[0];
};

const setContestInvitations = async (contestId, userId) => {
  const sql = `
  INSERT INTO invitations
  VALUES
  (null, ?, ?);
  `;

  await pool.query(sql, [contestId, userId]);
};

const getContestInvitations = async (id) => {
  const sql = `
  SELECT *
  FROM invitations
  WHERE contests_id = ?;
  `;

  return await pool.query(sql, id);
};

export default {
  getAllContests,
  getContestById,
  getPhotosByContestId,
  setContestJury,
  getContestJury,
  setContestWinners,
  getContestWinners,
  createContest,
  getContestByName,
  setCategoryId,
  getContestCategories,
  createContestCover,
  getContestCover,
  setContestInvitations,
  getContestInvitations,
  getContestByIdOnly,
};
