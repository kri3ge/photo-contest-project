import express from 'express';
import { PORT } from './config.js';
import cors from 'cors';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import passport from 'passport';
import jwtStrategy from './auth/strategy.js';
import { authMiddleware } from './auth/middlewares.js';

import createAuthController from './controllers/auth-controller.js';
import createUsersController from './controllers/users-controller.js';
import usersService from './services/users-service.js';
import usersData from './data/users-data.js';
import createContestsController from './controllers/contests-controller.js';
import contestsService from './services/contests-service.js';
import contestsData from './data/contests-data.js';
import createPhotosController from './controllers/photos-controller.js';
import photosData from './data/photos-data.js';
import photosService from './services/photos-service.js';

const app = express();
app.use(express.static('public'));

passport.use(jwtStrategy);
app.use(cors(), bodyParser.json(), helmet());
app.use(passport.initialize());

app.use('/api', createAuthController(usersService, usersData));
app.use('/api/users', createUsersController(usersService, usersData));
app.use(authMiddleware);
app.use('/api/contests',
  createContestsController(contestsService, contestsData, usersData),
  createPhotosController(photosService, photosData, usersData),
);

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  res.status(500).send({
    message: 'An unexpected error occurred, our developers are working hard to resolve it.',
  });
});

app.all('*', (req, res) =>
  res.status(404).send({ message: 'Resource not found!' }),
);

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));