import * as actionTypes from '../actions/actionTypes'

const initialState = {
    loading: false,
    contests: [],
    coverContest: {},
    error: ''
}

export const contestsReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_CONTEST_REQUEST:
            return {
                ...state, loading: true
            }
        case actionTypes.FETCH_CONTEST_SUCCESS:
            return {
                ...state,
                loading: false,
                contests: action.payload,
                coverContest: Math.floor(Math.random() * action.payload.length),
                error: ''
            }
        case actionTypes.FETCH_CONTEST_FAILURE:
            return {
                ...state,
                loading: false,
                contests: [],
                error: action.payload
            }
        default:
            return state;
    }
}

export const createContestReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_CREATE_CONTEST_REQUEST:
            return {
                ...state, loading: true
            }
            case actionTypes.FETCH_CREATE_CONTEST_SUCCESS:
            return {
                ...state,
                contests: action.payload,
                loading: false,
                error: ''
            }
            case actionTypes.FETCH_CREATE_CONTEST_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            }
        default:
            return state;
    }
}
    
const initialStateCurrentContest = {
        loading: false,
        currentContest: {},
        error: ''
}
    
export const currentContestReducer = (state = initialStateCurrentContest, action) => {
    switch (action.type) {
        case actionTypes.FETCH_CURRENT_CONTEST_REQUEST:
            return {
                ...state, loading: true
            }
        case actionTypes.FETCH_CURRENT_CONTEST_SUCCESS:
            return {
                ...state,
                loading: false,
                currentContest: action.payload,
                error: ''
            }
        case actionTypes.FETCH_CURRENT_CONTEST_FAILURE:
            return {
                ...state,
                loading: false,
                currentContest: {},
                error: action.payload
            }
        default:
            return state;
    }
}

const initialStateWinners = {
    loading: false,
    winners: [],
    allWinners: [],
    drawError: '',
    getError: ''
}

export const winnersReducer = (state = initialStateWinners, action) => {
    switch (action.type) {
        case actionTypes.FETCH_DRAW_WINNERS_REQUEST:
            return {
                ...state, loading: true
            }
        case actionTypes.FETCH_DRAW_WINNERS_SUCCESS:
            return {
                ...state,
                loading: false,
                winners: action.payload,
                drawError: ''
            }
        case actionTypes.FETCH_GET_WINNERS_SUCCESS:
            return {
                ...state,
                loading: false,
                allWinners: action.payload,
                getError: ''
            }
        case actionTypes.FETCH_DRAW_WINNERS_FAILURE:
            return {
                ...state,
                loading: false,
                winners: [],
                drawError: action.payload
            }
        default:
            return state;
    }
}
