import * as actionTypes from '../actions/actionTypes'

const initialState = {
    loading: false,
    photos: [],
    singlePhoto: [],
    error: ''
}

export const photosReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_PHOTOS_REQUEST:
            return {
                ...state, loading: true
            }
        case actionTypes.FETCH_PHOTOS_SUCCESS:
            return {
                ...state,
                loading: false,
                photos: action.payload,
                error: ''
            }
        case actionTypes.FETCH_PHOTOS_FAILURE:
            return {
                ...state,
                loading: false,
                photos: [],
                error: action.payload
            }
        case actionTypes.FETCH_SINGLE_PHOTO_REQUEST:
            return {
                ...state,
                loading: true,
                singlePhoto: [],
                error: action.payload
            }
        case actionTypes.FETCH_SINGLE_PHOTO_SUCCESS:
            return {
                ...state,
                loading: false,
                singlePhoto: action.payload,
                error: ''
            }
        case actionTypes.FETCH_SINGLE_PHOTO_FAILURE:
            return {
                ...state,
                loading: false,
                singlePhoto: [],
                error: action.payload
            }
        default:
            return state;
    }
}

const votesInitialState = {
    loading: false,
    votes: '',
    error: ''
}

export const votesReducer = (state = votesInitialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_VOTE_REQUEST:
            return {
                ...state, loading: true
            }
        case actionTypes.FETCH_VOTE_SUCCESS:
            return {
                ...state,
                loading: false,
                votes: action.payload,
                error: ''
            }
        case actionTypes.FETCH_VOTE_FAILURE:
            return {
                ...state,
                loading: false,
                votes: [],
                error: action.payload
            }
        default:
            return state;
    }
}

const uploadInitialState = {
    loading: false,
    uploadedPhoto: '',
    error: ''
}

export const uploadPhotoReducer = (state = uploadInitialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_PHOTO_UPLOAD_REQUEST:
            return {
                ...state, loading: true
            }
        case actionTypes.FETCH_PHOTO_UPLOAD_SUCCESS:
            return {
                ...state,
                loading: false,
                uploadedPhoto: action.payload,
                error: ''
            }
        case actionTypes.FETCH_PHOTO_UPLOAD_FAILURE:
            return {
                ...state,
                loading: false,
                uploadedPhoto: [],
                error: action.payload
            }
        default:
            return state;
    }
}
