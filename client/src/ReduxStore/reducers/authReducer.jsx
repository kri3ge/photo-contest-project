import decode from "jwt-decode";
import * as actionTypes from '../actions/actionTypes'

const storageToken = () => localStorage.getItem('token');
const token = storageToken() ? decode(storageToken()) : null;

const sessionInitialState = {
    loading: false,
    isRegistered: false,
    isLoggedIn: (() => {
        const currentTime = Date.now() / 1000;;
        if (!token || token.exp < currentTime) {
            return false;
        } else {
            return true;
        }
    })(),
    isMobile: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ? true : false,
    userId: (token && token.sub) || null,
    username: (token && token.username) || null,
    firstName: (token && token.firstName) || null,
    lastName: (token && token.lastName) || null,
    role: (token && token.role) || null,
    picture: (token && token.picture) || null,
    points: (token && token.points) || null,
    exp: (token && token.exp) || null,
    error: '',
}

export const authReducer = (state = sessionInitialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_LOGIN_REQUEST:
            return {
                ...state, loading: true, error: ''
            }
        case actionTypes.FETCH_LOGIN_SUCCESS:
            return {
                ...state, loading: false, isRegistered: true, isLoggedIn: true, userId: decode(action.payload).sub, username: decode(action.payload).username, firstName: decode(action.payload).firstName, lastName: decode(action.payload).lastName, role: decode(action.payload).role || null, picture: decode(action.payload).picture, points: decode(action.payload).points, exp: decode(action.payload).exp,
            }
        case actionTypes.FETCH_LOGIN_FAILURE:
            return {
                ...state, loading: false, isLoggedIn: false, error: action.payload
            }
        case actionTypes.FETCH_LOGOUT:
            localStorage.clear()
            return {
                ...state, isLoggedIn: false, isRegistered: false, username: null, firstName: null, lastName: null, role: null, picture: null, points: null
            }
        default:
            return state;
    }
}


const registerInitialState = {
    loading: false,
    isRegistered: false,
    error: ''
}

export const registerReducer = (state = registerInitialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_REGISTER_REQUEST:
            return {
                ...state, loading: true, error: '', isRegistered: false,
            }
        case actionTypes.FETCH_REGISTER_SUCCESS:
            return {
                ...state, loading: false, isRegistered: true, error: '',
            }
        case actionTypes.FETCH_REGISTER_FAILURE:
            return {
                ...state, loading: false, isRegistered: false, error: action.payload
            }
        default:
            return state;
    }
}
