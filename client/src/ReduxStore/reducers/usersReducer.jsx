import * as actionTypes from '../actions/actionTypes'

const allUsersInitialState = {
    loading: false,
    users: [],
    error: ''
}

export const allUsersReducer = (state = allUsersInitialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_USERS_REQUEST:
            return {
                ...state, loading: true
            }
        case actionTypes.FETCH_USERS_SUCCESS:
            return {
                ...state,
                loading: false,
                users: action.payload,
                error: ''
            }
        case actionTypes.FETCH_USERS_FAILURE:
            return {
                ...state,
                loading: false,
                users: [],
                error: action.payload
            }
        default:
            return state;
    }
}

const userByIdInitialState = {
    loading: false,
    singleUser: [],
    error: ''
}

export const userByIdReducer = (state = userByIdInitialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_SINGLE_USER_REQUEST:
            return {
                ...state,
                loading: true,
                singleUser: [],
                error: action.payload
            }
        case actionTypes.FETCH_SINGLE_USER_SUCCESS:
            return {
                ...state,
                loading: false,
                singleUser: action.payload,
                error: ''
            }
        case actionTypes.FETCH_SINGLE_USER_FAILURE:
            return {
                ...state,
                loading: false,
                singleUser: [],
                error: action.payload
            }
        default:
            return state;
    }
}


