import basicAxios from "../../requests/axios";
import { userRequest } from '../../requests/requestsURL';
import * as actionTypes from '../actions/actionTypes'

export const fetchUsers = () => {
    return (dispatch) => {
        dispatch({ type: actionTypes.FETCH_USERS_REQUEST })
        basicAxios.get(userRequest.userRequest)
            .then(response => {
                const users = response.data;
                dispatch({ type: actionTypes.FETCH_USERS_SUCCESS, payload: users })
            })
            .catch(error => {
                const errorMessage = error.message;
                dispatch({ type: actionTypes.FETCH_USERS_FAILURE, payload: errorMessage })
            })
    }
}

export const fetchUserById = (userId) => {
    return (dispatch) => {
        dispatch({ type: actionTypes.FETCH_SINGLE_USER_REQUEST })
        basicAxios.get(`${userRequest.userRequest}${userId}`)
            .then(response => {
                const user = response.data;
                dispatch({ type: actionTypes.FETCH_SINGLE_USER_SUCCESS, payload: user })
            })
            .catch(error => {
                const errorMessage = error.message;
                dispatch({ type: actionTypes.FETCH_SINGLE_USER_FAILURE, payload: errorMessage })
            })
    }
}

