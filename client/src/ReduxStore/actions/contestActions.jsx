import basicAxios from "../../requests/axios";
import { contestsRequest } from '../../requests/requestsURL';
import * as actionTypes from '../actions/actionTypes'

export const fetchContests = () => {
    return (dispatch) => {
        dispatch({ type: actionTypes.FETCH_CONTEST_REQUEST })
        basicAxios.get(contestsRequest.getAllContests)
            .then(response => {
                const contests = response.data.contestsList;
                dispatch({ type: actionTypes.FETCH_CONTEST_SUCCESS, payload: contests })
            })
            .catch(error => {
                const errorMessage = error.message;
                dispatch({ type: actionTypes.FETCH_CONTEST_FAILURE, payload: errorMessage })
            })
    }
}

export const fetchContestById = (contestId) => {
    return (dispatch) => {
        dispatch({ type: actionTypes.FETCH_CURRENT_CONTEST_REQUEST })
        basicAxios.get(contestsRequest.getContest + contestId)
            .then(response => {
                dispatch({ type: actionTypes.FETCH_CURRENT_CONTEST_SUCCESS, payload: response.data })
            })
            .catch(error => {
                const errorMessage = error.message;
                dispatch({ type: actionTypes.FETCH_CURRENT_CONTEST_FAILURE, payload: errorMessage })
            })
    }
}

export const createNewContest = (body) => {
    return (dispatch) => {
        dispatch({ type: actionTypes.FETCH_CREATE_CONTEST_REQUEST })
        console.log(body);
        basicAxios.post(contestsRequest.createContest, body)
            .then(response => {
                const file = new FormData()
                file.append('coverImg', body.photo);
                const contestId = response.data.contest.id;
                basicAxios.post(`${contestsRequest.getContest}${contestId}/record`, file, {
                    headers: {
                        'Content-Type': `multipart/form-data; boundary=---WebKitFormBoundary7MA4YWxkTrZu0gW`
                    }
                })
                    .then(response => {
                        const jury = { usersIds: body.jury }
                        basicAxios.post(`${contestsRequest.getContest}${contestId}/jury`, jury)
                            .then(response => {
                                const invitations = { usersIds: body.invitations }
                                basicAxios.post(`${contestsRequest.getContest}${contestId}/invitations`, invitations)
                                    .then(response => {
                                        dispatch({ type: actionTypes.FETCH_CREATE_CONTEST_SUCCESS, payload: { id: contestId, title: body.title } })
                                    })
                                    .catch(error => {
                                        const errorMessage = error.message;
                                        dispatch({ type: actionTypes.FETCH_CREATE_CONTEST_FAILURE, payload: errorMessage })
                                    })
                            })
                            .catch(error => {
                                const errorMessage = error.message;
                                dispatch({ type: actionTypes.FETCH_CREATE_CONTEST_FAILURE, payload: errorMessage })
                            })
                            .catch(error => {
                                const errorMessage = error.message;
                                dispatch({ type: actionTypes.FETCH_CREATE_CONTEST_FAILURE, payload: errorMessage })
                            })
                            .catch(error => {
                                const errorMessage = error.message;
                                dispatch({ type: actionTypes.FETCH_CREATE_CONTEST_FAILURE, payload: errorMessage })
                            })
                    })
            })
    }
}

export const drawWinners = (contestId) => {
    return (dispatch) => {
        dispatch({ type: actionTypes.FETCH_DRAW_WINNERS_REQUEST })
        basicAxios.post(`${contestsRequest.getContest}${contestId}/winners`)
            .then(response => {
                dispatch({ type: actionTypes.FETCH_DRAW_WINNERS_SUCCESS, payload: response.data.winners })
            })
            .catch(error => {
                const errorMessage = error.message;
                dispatch({ type: actionTypes.FETCH_DRAW_WINNERS_FAILURE, payload: errorMessage })
            })
    }
}

export const fetchContestWinners = (contestId) => {
    return (dispatch) => {
        dispatch({ type: actionTypes.FETCH_DRAW_WINNERS_REQUEST })
        basicAxios.get(`${contestsRequest.getContest}${contestId}/winners`)
            .then(response => {
                dispatch({ type: actionTypes.FETCH_GET_WINNERS_SUCCESS, payload: response.data })
            })
            .catch(error => {
                const errorMessage = error.message;
                dispatch({ type: actionTypes.FETCH_DRAW_WINNERS_FAILURE, payload: errorMessage })
            })
    }
}
