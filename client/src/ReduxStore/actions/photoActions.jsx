import basicAxios from "../../requests/axios";
import { contestsRequest } from '../../requests/requestsURL';
import * as actionTypes from '../actions/actionTypes'

export const fetchPhotos = (contestId) => {
    return (dispatch) => {
        dispatch({ type: actionTypes.FETCH_PHOTOS_REQUEST })
        basicAxios.get(`${contestsRequest.getContest}${contestId}/photos`)
            .then(response => {
                dispatch({ type: actionTypes.FETCH_PHOTOS_SUCCESS, payload: response.data })
            })
            .catch(error => {
                dispatch({ type: actionTypes.FETCH_PHOTOS_FAILURE, payload: error.message })
            })
    }
}
export const fetchPhotoById = (contestId, photoId,) => {
    return (dispatch) => {
        dispatch({ type: actionTypes.FETCH_SINGLE_PHOTO_REQUEST })
        basicAxios.get(`${contestsRequest.getContest}${contestId}/photos/${photoId}`)
            .then(response => {
                dispatch({ type: actionTypes.FETCH_SINGLE_PHOTO_SUCCESS, payload: response.data })
            })
            .catch(error => {
                dispatch({ type: actionTypes.FETCH_SINGLE_PHOTO_FAILURE, payload: error.message })
            })
    }
}

export const fetchPhotoVote = (contestId, photoId, body) => {
    return (dispatch) => {
        dispatch({ type: actionTypes.FETCH_VOTE_REQUEST })
        basicAxios.post(`${contestsRequest.getContest}${contestId}/photos/${photoId}/votes`, body)
            .then(response => {
                dispatch({ type: actionTypes.FETCH_VOTE_SUCCESS, payload: response.data })
            })
            .catch(error => {
                dispatch({ type: actionTypes.FETCH_VOTE_FAILURE, payload: error.message })
            })
    }
}

export const uploadPhoto = (contestId, values) => {
    return (dispatch) => {
        const file = new FormData()
        file.append('contestImage', values.photo);
        dispatch({ type: actionTypes.FETCH_PHOTO_UPLOAD_REQUEST })
        basicAxios.post(`${contestsRequest.getContest}${contestId}/photos`, file, {
            headers: {
                'Content-Type': `multipart/form-data; boundary=---WebKitFormBoundary7MA4YWxkTrZu0gW`
            }
        })
        .catch((error) => {
            dispatch({ type: actionTypes.FETCH_PHOTO_UPLOAD_FAILURE, payload: error.message })
         })
        .then((response) => {
            if (response) {
                basicAxios.post(`${contestsRequest.getContest}${contestId}/photos/${response.data.id}`, { title: values.title, story: values.story })
                .catch(error => {
                    dispatch({ type: actionTypes.FETCH_PHOTO_UPLOAD_FAILURE, payload: error.message })
                    })
                .then((response) => {
                    if (response) {
                    dispatch({ type: actionTypes.FETCH_PHOTO_UPLOAD_SUCCESS, payload: response.data })
                    fetchPhotos(contestId);
                    }
                })
            }
        })
    }
}
