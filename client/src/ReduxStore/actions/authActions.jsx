import basicAxios from "../../requests/axios";
import { authRequest, userRequest } from '../../requests/requestsURL';
import * as actionTypes from '../actions/actionTypes'

export const authUser = (body) => {
    return (dispatch) => {
        dispatch({ type: actionTypes.FETCH_LOGIN_REQUEST })
        basicAxios.post(authRequest.userSession, body)
            .then(response => {
                const token = response.data.token;
                localStorage.setItem('token', token)
                dispatch({ type: actionTypes.FETCH_LOGIN_SUCCESS, payload: token })
            })
            .catch(error => {
                const errorMessage = error.message;
                dispatch({ type: actionTypes.FETCH_LOGIN_FAILURE, payload: errorMessage })
            })
    }
}

export const registerUser = (body) => {
    return (dispatch) => {
        dispatch({ type: actionTypes.FETCH_REGISTER_REQUEST })
        basicAxios.post(userRequest.userRequest, body)
            .then(response => {
                basicAxios.post(authRequest.userSession, body)
                .then(response => {
                    const token = response.data.token;
                    localStorage.setItem('token', token)
                    dispatch({type: actionTypes.FETCH_LOGIN_SUCCESS, payload: token })
                    dispatch({ type: actionTypes.FETCH_REGISTER_SUCCESS })
        })
        .catch(error => {
            const errorMessage = error.message;
            dispatch({type: actionTypes.FETCH_LOGIN_FAILURE, payload: errorMessage })
        })
            })
            .catch(error => {
                const errorMessage = error.message;
                dispatch({ type: actionTypes.FETCH_REGISTER_FAILURE, payload: errorMessage })
            })
    }
}

export const loginUserSocialMedia = (body) => {
    return (dispatch) => {
        dispatch({type: actionTypes.FETCH_LOGIN_REQUEST})
        basicAxios.post(authRequest.userSession, body)
        .then(response => {
            const token = response.data.token;
            localStorage.setItem('token', token)
            dispatch({type: actionTypes.FETCH_LOGIN_SUCCESS, payload: token })
        })
        .catch(error => {
            if (error.message.includes('403') ) {
                dispatch({type: actionTypes.FETCH_REGISTER_REQUEST})
                basicAxios.post(userRequest.userRequest, body)
            .then(response => {
            dispatch({type: actionTypes.FETCH_REGISTER_SUCCESS })
            basicAxios.post(authRequest.userSession, body)
        .then(response => {
            const token = response.data.token;
            localStorage.setItem('token', token)
            dispatch({type: actionTypes.FETCH_LOGIN_SUCCESS, payload: token })
        })
        .catch(error => {
            const errorMessage = error.message;
            dispatch({type: actionTypes.FETCH_LOGIN_FAILURE, payload: errorMessage })
        })
        })
        .catch(error => {
            const errorMessage = error.message;
            dispatch({type: actionTypes.FETCH_REGISTER_FAILURE, payload: errorMessage })
        })
        }
            const errorMessage = error.message;
            dispatch({type: actionTypes.FETCH_LOGIN_FAILURE, payload: errorMessage })
        })
    }
}
