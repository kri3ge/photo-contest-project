import { applyMiddleware, combineReducers, configureStore } from '@reduxjs/toolkit';
import { authReducer, registerReducer } from './reducers/authReducer'
import { contestsReducer, createContestReducer, currentContestReducer, winnersReducer } from './reducers/contestReducer'
import { photosReducer, votesReducer, uploadPhotoReducer } from './reducers/photosReducer';
import { allUsersReducer, userByIdReducer } from './reducers/usersReducer';
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension';

const rootReducer = combineReducers({
  auth: authReducer,
  register: registerReducer,
  contests: contestsReducer,
  currentContest: currentContestReducer,
  createContest: createContestReducer,
  photos: photosReducer,
  photoUpload: uploadPhotoReducer,
  votes: votesReducer,
  users: allUsersReducer,
  winners: winnersReducer,
  singleUser: userByIdReducer,
})

const store = configureStore({
  reducer: rootReducer
}, composeWithDevTools(applyMiddleware(thunk)));

export default store;
