import moment from 'moment'

export const getProjectPhase = (contestData) => {
  if (contestData) {
    if (moment(contestData.start_date, "DD/MM/YYYY").add(contestData.PhaseOneLimitDays, 'days').isAfter(moment(), 'days')) {
      return 'Phase One';
    } else if (moment(contestData.start_date, "DD/MM/YYYY").add(contestData.PhaseOneLimitDays, 'days').add('hours', contestData.PhaseTwoLimitHours).isAfter(moment(), 'hours')) {
      return 'Phase Two';
    } else {
      return 'Finished';
    }
  }
}

export const redirectToHomePage = (dispatch, history) => {
    dispatch({type: 'FETCH_LOGOUT'});
    history.push('/home');
}

export const calculateTime = (contestData, time) => {
  switch (time) {
    case 'days' :
      return (moment(contestData.currentContest.start_date, "DD/MM/YYYY").add('days', contestData.currentContest.PhaseOneLimitDays)).diff(moment(), 'days')
    case 'hours' :
      return (moment(contestData.currentContest.start_date, "DD/MM/YYYY").add('days', contestData.currentContest.PhaseOneLimitDays).add('hours', contestData.currentContest.PhaseTwoLimitHours)).diff(moment(), 'hours')
    case 'diffPhaseOne' :
      return moment(contestData.currentContest.start_date, "DD/MM/YYYY").add('days', contestData.currentContest.PhaseOneLimitDays).add('hours', contestData.currentContest.PhaseTwoLimitHours).format('DD/MM/YYYY')
    case 'diffPhaseTwo' : 
      return moment(contestData.currentContest.start_date, "DD/MM/YYYY").add('days', contestData.currentContest.PhaseOneLimitDays).add('hours', contestData.currentContest.PhaseTwoLimitHours).format('DD/MM/YYYY')
    default: 
      return null;
  }
}


