import './CreateContestPage.scss'
import PropTypes from 'prop-types'
import { Form, Formik} from "formik";
import * as Yup from 'yup';
import { useHistory } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { Button, Chip, Dialog, DialogActions, DialogTitle, FormControl, FormControlLabel, Input, InputAdornment, InputLabel, MenuItem, Select, Switch, TextField, Tooltip, useTheme } from '@material-ui/core';
import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';
import LoggedHeader from '../../Layout/Headers/LoggedHeader';
import { connect } from 'react-redux';
import { createNewContest } from '../../ReduxStore/actions/contestActions';
import { fetchUsers } from '../../ReduxStore/actions/userActions';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const getStyles = (id, personName, theme) => {
  return {
    fontWeight:
      personName.indexOf(id) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
    backgroundColor: 
    personName.indexOf(id) === -1
        ? 'lightgrey'
        : '#006d77',
    color: 
    personName.indexOf(id) === -1
        ? 'black'
        : 'white',
  };
}
const CreateContestPage = ({ createNewContest, createContestData, fetchUsers, usersData }) => {
  const [uploadedPhoto, setUploadedPhoto ] = useState(false);
  const [ open, setOpen ] = useState(false);
  const history = useHistory();
  const FILE_SIZE = 2000 * 2000;
  const SUPPORTED_FORMATS = [
      "image/jpg",
      "image/jpeg",
      "image/png"
  ];
  const theme = useTheme();


  useEffect(() => {
    fetchUsers()
  }, [fetchUsers])

  const createContest = (values) => {
  const organizers = usersData.users.filter(user => user.role === 'Organizer').map(user => user.id)
  console.log(organizers);
  values.jury = values.jury.concat(organizers)
  console.log(values);
  createNewContest(values);
  setOpen(true);
};

const handleDragEnter = event => {
    event.preventDefault();
    event.stopPropagation();
  };
  const handleDragLeave = event => {
    event.preventDefault();
    event.stopPropagation();
  };
  const handleDragOver = event => {
    event.preventDefault();
    event.stopPropagation();
  };

  const handleDrop = event => {
    event.preventDefault();
    event.stopPropagation();
    
    const file = [...event.dataTransfer.files][0];
    setUploadedPhoto(file)
  };
  
  const handleClose = () => {
    createContestData.error === '' && history.push('/contests')
    setOpen(false);
  };

  const CreateContestSchema = Yup.object().shape({
        title: Yup.string()
        .required('Title required')
        .min(2, 'Title too short.')
        .max(40, 'Title too long.'),
        contestCategory: Yup.string()
        .required('Category required') 
        .min(5, 'Category too short.')
        .max(400, 'Category too long.'),
        phaseOneLimitDays: Yup.number()
        .typeError('Should be a number')
        .required('Limit Entry Phase required') 
        .min(1, 'Limit Entry Phase too short.')
        .max(30, 'Limit Entry Phase too long.'),
        phaseTwoLimitHours: Yup.number()
        .typeError('Should be a number')
        .required('Limit Voting Phase required') 
        .min(1, 'Limit Voting Phase too short.')
        .max(24, 'Limit Voting Phase too long.'),
        contestType: Yup.string()
        .required('Contest type required'),
        invitations: Yup.string(),
        is_auto: Yup.boolean(),
        photo: Yup.mixed()
        .required('Photo required')
        .test(
            "fileSize",
            "File too large",
            value => value && value.size <= FILE_SIZE
          )
          .test(
            "fileFormat",
            "Unsupported Format",
            value => value && SUPPORTED_FORMATS.includes(value.type)
          )
          .typeError('Should be jpg/png')
    })

    return (
      <div className='create-contest-container'>
        <LoggedHeader />
        <Formik 
          onSubmit={(values) =>createContest (values)}
          initialValues={{
                title: '',
                contestCategory: '',
                contestType: 'Open',
                invitations: [],
                is_auto: false,
                jury: []
            }}
          validationSchema={CreateContestSchema}
        >
          {({ values, errors, touched, handleChange, resetForm, setFieldValue }) => (
            <Form id='create-contest-form'>
              <div className='form-container-create-contest'>
                <div className='cover-photo-upload-div'>
                  {uploadedPhoto ?
                    <label htmlFor='upload-cover-input'>
                      <img src={URL.createObjectURL(uploadedPhoto)} alt='uploaded' className='uploaded-cover' />
                      <Input
                        style={{ display: 'none' }}
                        variant='outlined' className='cover-upload-input-field-with-photo' id='upload-cover-input' name='photo' 
                        type='file'
                        helperText={errors.coverPhoto}
                        onChange={(event) => {
                                                  setFieldValue("coverPhoto", event.currentTarget.files[0])
                                                  setUploadedPhoto(event.currentTarget.files[0])
                                                }}
                      />
                    </label>
                    : 
                    <div
                      id='my-upload'
                      className='empty-cover'
                      onDrop={event => { 
                                handleDrop(event)
                                setFieldValue("photo", [...event.dataTransfer.files][0])
                            }}
                      onDragOver={e => handleDragOver(e)}
                      onDragEnter={e => handleDragEnter(e)}
                      onDragLeave={e => handleDragLeave(e)}
                    >
                      <label htmlFor='upload-cover-input'>
                        <Input
                          style={{ display: 'none' }}
                          variant='outlined' className='cover-upload-input-field' id='upload-cover-input' name='photo' 
                          type='file'
                          helperText={errors.coverPhoto}
                          onChange={(event) => {
                                                  setFieldValue("coverPhoto", event.currentTarget.files[0])
                                                  setUploadedPhoto(event.currentTarget.files[0])
                                                }}
                        />
                        <h2 className='cover-upload-button' variant='contained' component='span'>
                          <AddAPhotoIcon className='add-icon' />
                          Upload or Drop Cover photo
                        </h2>
                        {errors.photo && <h5 className='photo-error'>Photo required</h5>}
                      </label>
                    </div>}
                </div>
                <div className='additional-photo-data'>
                  <h3 className='section-title'>Basic contest info</h3>
                  <div className='basic-info-div'>
                    <div className='info-fields-div'>
                      <TextField
                        variant='outlined' className='create-contest-input-field' name='title' label='Title' autoFocus
                        onChange={handleChange}
                        value={values.title}
                        helperText={errors.title}
                        error={touched.title && errors.title ? true : false}
                      />
                      <TextField
                        variant='outlined' className='create-contest-input-field' name='contestCategory' label='Category' 
                        onChange={handleChange}
                        value={values.contestCategory}
                        type='text'
                        multiline
                        helperText={errors.contestCategory}
                        error={touched.contestCategory && errors.contestCategory ? true : false}
                      />
                    </div>
                  </div>
                  <br />
                  <h3 className='section-title'>Phases deadlines</h3>
                  <div className='basic-info-div'>
                    <br />
                    <div className='info-fields-div'>
                      <TextField
                        variant='outlined' className='create-contest-input-field' name='phaseOneLimitDays' label='Entry limit' 
                        onChange={handleChange}
                        value={values.phaseOneLimitDays}
                        type='text'
                        InputProps={{
                      endAdornment: <InputAdornment style={{color: errors.phaseOneLimitDays ? 'red' : 'black'}} position='end'>days</InputAdornment>}}
                        helperText={errors.phaseOneLimitDays}
                        error={touched.phaseOneLimitDays && errors.phaseOneLimitDays ? true : false}
                      />
                      <TextField
                        variant='outlined' className='create-contest-input-field' name='phaseTwoLimitHours' label='Voting limit' 
                        onChange={handleChange}
                        value={values.phaseTwoLimitHours}
                        type='text'
                        InputProps={{
                    endAdornment: <InputAdornment position='end'>hrs</InputAdornment>}}
                        helperText={errors.phaseTwoLimitHours}
                        error={touched.phaseTwoLimitHours && errors.phaseTwoLimitHours ? true : false}
                      />
                    </div>
                  </div>
                  <br />
                  <h3 className='section-title'>Contest type</h3>
                  <div className='basic-info-div'>
                    <div className='info-fields-div'>
                      <div className='single-form-element-type'>
                        <Select
                          id='contest-type-select'
                          variant='outlined' className='create-contest-input-field' name='contestType' label='Contest Type' 
                          onChange={handleChange}
                          value={values.contestType}
                          type='text'
                          multiline
                          helperText={errors.contestType}
                          defaultValue={values.contestType}
                          error={touched.contestType && errors.contestType ? true : false}
                        >
                          <MenuItem value='Open'>Open</MenuItem>
                          <MenuItem value='Invitational'>Invitational</MenuItem>
                        </Select>
                      </div>
                      <br />
                      <FormControl className='invitations-form-control'>
                        <InputLabel id='mutiple-chip-label-invitations'>Invitations</InputLabel>
                        <Tooltip title='Only for invitational contest type' placement='top-start'>
                          <Select
                            labelId='mutiple-chip-label-invitations'
                            id='mutiple-chip-invitations'
                            className='select-multiple-component'
                            multiple
                            name='invitations'
                            value={values.invitations}
                            onChange={handleChange}
                            disabled={values.contestType === 'Invitational' ? false : true}
                            input={<Input id='select-multiple-chip-invitations' />}
                            renderValue={(selected) => (
                              <div className='invitation-chips'>
                                {selected.map((value) => 
                                (
                                  <Chip key={value} name='invitations' label={usersData.users.filter( user => user.id === value)[0].first_name} className='invitation-single-chip' />
              ))}
                              </div>
          )}
                            MenuProps={MenuProps}
                          >
                            {usersData.users && usersData.users.map((user) => (
                              <MenuItem key={user.id} value={user.id} name='invitations' style={getStyles(user.id, values.invitations, theme)}>
                                {user.first_name + ' ' + user.last_name}
                              </MenuItem>
          ))}
                          </Select>
                        </Tooltip>
                      </FormControl>
                    </div>
                  </div>
                  <h3 className='section-title'>Select jury</h3>
                  <div className='basic-info-div'>
                    <div className='info-fields-div'>
                      <FormControlLabel
                        control={
                          <Switch
                            name='is_auto'
                            checked={values.is_auto}
                            onChange={handleChange}
                          />
                  }
                        label='Autoclose voting'
                      />
                      <FormControl className='jury-form-control'>
                        <InputLabel id='mutiple-chip-label-jury'>Jury</InputLabel>
                        <Tooltip title='All organizers are automatically selected' placement='top-start'>
                          <Select
                            labelId='mutiple-chip-label-jury'
                            id='mutiple-chip-jury'
                            className='select-multiple-component'
                            multiple
                            name='jury'
                            value={values.jury}
                            onChange={handleChange}
                            input={<Input id='select-multiple-chip-jury' />}
                            renderValue={(selected) => (
                              <div className='invitation-chips'>
                                {selected.map((value) => (
                                  <Chip key={value} name='jury' label={usersData.users.filter( user => user.id === value)[0].first_name} className='jury-single-chip' />
              ))}
                              </div>
          )}
                            MenuProps={MenuProps}
                          >
                            {usersData.users && usersData.users.filter(user => user.role === 'Master' ||  user.role === 'Wise and Benevolent Photo Dictator').map((user) => (
                              <MenuItem key={user.username} value={user.id} name='invitations' style={getStyles(user.id, values.jury, theme)}>
                                {user.first_name + ' ' + user.last_name}
                              </MenuItem>
          ))}
                          </Select>
                        </Tooltip>
                      </FormControl>
                      <br />
                    </div>
                  </div>
                </div>
                <br />
              </div>
              <hr />
              <div className='action-buttons'>
                <Button onClick={() => {
                        resetForm(); 
                        setUploadedPhoto(false);
                        }}
                >Reset
                </Button>
                <Button type='submit' className='submit-button'>Submit</Button>
              </div>
              {touched.coverPhoto && errors.coverPhoto === true
                      ? <p style={{color: 'red'}}>Incorrect file type </p>
                    : <div style={{margin: '30px'}} />}
            </Form> )}
        </Formik>
        {createContestData.contests &&
          <Dialog onClose={handleClose} aria-labelledby='simple-dialog-title' open={open}>
            <DialogTitle>
              The contest has been successfully created!
            </DialogTitle>
            <DialogActions>
              <Button onClick={handleClose} className='submit-button'>Ok</Button>
            </DialogActions>
          </Dialog>}
      </div>
    )
}

CreateContestPage.propTypes = {
createNewContest: PropTypes.func.isRequired,
createContestData: PropTypes.object.isRequired,
fetchUsers: PropTypes.func.isRequired,
usersData: PropTypes.object.isRequired,
}

const mapStateToProps = state => {
  return {
    createContestData: state.createContest,
    usersData: state.users,
  }
}
const mapDispatchToProps = dispatch => {
  return {
    createNewContest: (body) => dispatch(createNewContest(body)),
    fetchUsers: () => dispatch(fetchUsers()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateContestPage);
