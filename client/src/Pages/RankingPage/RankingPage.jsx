import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { fetchUsers } from '../../ReduxStore/actions/userActions'
import PropTypes from 'prop-types';
import LoggedHeader from '../../Layout/Headers/LoggedHeader';
import './RankingPage.scss'
import { Link } from 'react-router-dom';
import { Avatar } from '@material-ui/core';

const RankingPage = ({ fetchUsers, usersData}) => {

    useEffect(() => {
        fetchUsers()
    }, [fetchUsers])
    return (
      <div className='ranking-container'>
        <LoggedHeader />
        <div className='all-users-container'>
          <h2 className='users-ranking-title'>Current ranking</h2>
          <div className='users-rows'>
            {usersData.users && usersData.users.filter(user => user.role !== 'Organizer').sort((a, b) => b.user_points - a.user_points).map( user => 
              <div className='single-user-div' key={user.id}>
                <strong className='rank-place'>{usersData.users.filter(user => user.role !== 'Organizer').sort((a, b) => b.user_points - a.user_points).indexOf(user) + 1}.</strong>
                <Link to={`users/${user.id}`} style={{margin: 0}}>
                  <Avatar src={user.picture} alt={user.username} className='user-image' />
                </Link>
                <br />
                <h3 className='fullname-container'>{user.first_name}  {user.last_name}</h3>
                <div className='points-container'>
                  <span>{user.user_points}</span>
                  <span>{user.role}</span>
                </div>
              </div>)}
          </div>
        </div>
      </div>
    )
}

RankingPage.propTypes = {
    fetchUsers: PropTypes.func.isRequired,
    usersData: PropTypes.object.isRequired,
  }
  
  const mapStateToProps = state => {
    return {
      authData: state.auth,
      usersData: state.users,
    }
  }
  
  const mapDispatchToProps = dispatch => {
    return {
      fetchUsers: () => dispatch(fetchUsers()),
    }
  }

export default connect(mapStateToProps, mapDispatchToProps)(RankingPage)
