/* eslint-disable react/prop-types */
import { Tooltip } from "@material-ui/core";
import { useCallback, useEffect, useState } from "react";
import LoggedHeader from "../../Layout/Headers/LoggedHeader";
import './ContestsMainPage.scss';
import FolderSpecialIcon from '@material-ui/icons/FolderSpecial';
import FolderSharedRoundedIcon from '@material-ui/icons/FolderSharedRounded';
import CategoryRow from "./CategoryRow";
import { useHistory } from "react-router-dom";
import { API_URL } from '../../requests/requestsURL';
import { connect } from "react-redux";
import { fetchContests } from "../../ReduxStore/actions/contestActions";
import { useDispatch } from "react-redux";
import { getProjectPhase, redirectToHomePage } from "../../utils/utils";
import BackToTop from "../../Layout/BackToTop/BackToTop";
import PropTypes from 'prop-types';

const ContestsMainPage = ({ fetchContests, contestsData } ) => {
  
  const setCategories = useCallback(() => {
    const categories = []
    contestsData.contests.map(cont => categories.indexOf(cont.category) === -1 && categories.push(cont.category))
    return categories;
  }, [contestsData.contests])

  const [ categoriesData, setCategoriesData ] = useState(setCategories());
  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    const fetchInitialRequests = () => {
    fetchContests();
      if (contestsData.error.includes('401')) {
        redirectToHomePage(dispatch, history)
      }
    }
    fetchInitialRequests();
  }, [fetchContests, 
    contestsData.error, history, dispatch
  ])
  
  useEffect(() => {
    setCategoriesData(setCategories())
  }, [setCategories])
 
  const openContestPage = (event) => {
    const id = event.target.id;
    history.push(`contests/${id}`)
  }
    return (
      <div className='main-contests-container'>
        <LoggedHeader />
        <BackToTop />
        <div className='recent-contests-container'>
          {contestsData && contestsData.contests[0] &&(
            <div className='recent-contests'>
              <br />
              <div className='single-contest' onClick={openContestPage}>
                <div className='contest-main-cover' id='back-to-top-anchor'>
                  <img src={`${API_URL}res-covers/${contestsData.contests[contestsData.coverContest].cover_img_res}`} alt={contestsData.contests[contestsData.coverContest].title} id={contestsData.contests[contestsData.coverContest].id} className='cover-img-main' />
                </div>
                <div className='cover-contest-top-details'>
                  <h3 className='cover-contest-title'>{contestsData.contests[contestsData.coverContest].title} <br /></h3>
                  <div style={{maxWidth: '360px', width:'100%'}} />
                  <Tooltip title='Category' placement='top'><h3 className='category'>{contestsData.contests[contestsData.coverContest].category} <br /></h3></Tooltip>
                  {contestsData.contests[contestsData.coverContest].type === 'Invitational'
                  ? <Tooltip title='Type: Invitational' placement='top'><FolderSpecialIcon className='contest-icon' /></Tooltip>
                  : <Tooltip title='Type: Open' placement='top'><FolderSharedRoundedIcon boxShadow='0px 1px 3px black' className='contest-icon' /></Tooltip>}
                  <Tooltip title={getProjectPhase(contestsData.coverContest)} placement='top'><h5 className='phase-badge'>{getProjectPhase(contestsData.coverContest)}</h5></Tooltip>
                </div>
              </div>
            </div>
          )}
          <div className='categories-container'>
            {categoriesData.map(category => 
              <div className='category-row-div' key={category}>
                <h2 className='category-title'>{category}</h2>
                <CategoryRow category={category} contestsData={contestsData.contests} openContestPage={openContestPage} fetchContests={fetchContests} />
              </div>
          )}
          </div>
        </div>
      </div>
    )
}

ContestsMainPage.propTypes = {
  fetchContests: PropTypes.func.isRequired,
  contestsData: PropTypes.object.isRequired,
  }

const mapStateToProps = state => {
  return {
    contestsData: state.contests
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchContests: () => dispatch(fetchContests())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContestsMainPage);
