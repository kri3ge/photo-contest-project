/* eslint-disable react/prop-types */
import { Tooltip } from "@material-ui/core";
import './CategoryRow.scss';
import FolderSpecialIcon from '@material-ui/icons/FolderSpecial';
import FolderSharedRoundedIcon from '@material-ui/icons/FolderSharedRounded';
import { API_URL } from '../../requests/requestsURL';
import { getProjectPhase } from "../../utils/utils";
import PropTypes from 'prop-types';


const CategoryRow = ({ category, contestsData, openContestPage }) => {

  return (
    <div className='category-row-container'>
      {contestsData.length > 0 && contestsData.filter(contest => contest.category === category).slice(0,3).map(contest =>
        <div className='contest-category-single' key={contest.id} onClick={openContestPage}>
          <div className='contest-top-details' onClick={(e) => e.preventDefault()}>
            <span className='contest-title' id={contest.id}>{contest.title} <br /></span>
            <div style={{ maxWidth: '50px', width: '100%' }} id={contest.id} onClick={(e) => e.preventDefault()} />
            {contest.type === 'Invitational'
              ? <Tooltip title='Contest Type: Invitational' placement='top'><FolderSpecialIcon className='contest-icon' id={contest.id} onClick={(e) => e.preventDefault()} /></Tooltip>
              : <Tooltip title='Contest Type: Open' placement='top'><FolderSharedRoundedIcon id={contest.id} className='contest-icon' onClick={(e) => e.preventDefault()} /></Tooltip>}
            <Tooltip title={getProjectPhase(contest)} placement='top'><h5 className='phase-badge' onClick={(e) => e.preventDefault()}>{getProjectPhase(contest)}</h5></Tooltip>
          </div>
          <img src={contest.cover_img_res && `${API_URL}res-covers/${contest.cover_img_res}`} alt={contest.title} className='row-cover-img' id={contest.id} />
        </div>
      )}
    </div>
  )
}

CategoryRow.propTypes = {
  contestsData: PropTypes.array.isRequired,
  }

export default CategoryRow;

