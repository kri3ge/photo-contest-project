/* eslint-disable react/prop-types */
import LandingHeader from '../../Layout/Headers/LandingHeader';
import './LandingPage.scss';
import DogVideo from '../../Data/dogs-welcome.mp4'
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux'
import { useState } from 'react';

const LandingPage = ({ authData }) => {
  const history = useHistory();
  const [openRegister, setOpenRegister] = useState(false)

  if (authData.isLoggedIn) {
    history.push('/contests')
  }

  return (
    <div className='landing-page-container'>
      <LandingHeader openRegister={openRegister} setOpenRegister={setOpenRegister} />
      <div className='discover-contests-container'>
        <video autoPlay muted loop id='MyVideo' className='landing-video'>
          <source src={DogVideo} type='video/mp4' />
        </video>
        <h2 className='hang-button discover-link' onClick={() => setOpenRegister(true)}>Register now to discover contests</h2>
      </div>
    </div>
  )
}

const mapStateToProps = state => {
  return {
    authData: state.auth,
  }
}

export default connect(mapStateToProps)(LandingPage);
