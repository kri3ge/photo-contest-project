import React from 'react';
import './UserPage.scss';
import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useHistory } from "react-router-dom";
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchUserById } from '../../ReduxStore/actions/userActions';
import { fetchContests } from "../../ReduxStore/actions/contestActions";
import { API_URL } from '../../requests/requestsURL';
import PhotoDetails from '../../Layout/PhotoDetails/PhotoDetails';
import LoggedHeader from '../../Layout/Headers/LoggedHeader';
import { getProjectPhase } from "../../utils/utils";
import FolderSpecialIcon from '@material-ui/icons/FolderSpecial';
import FolderSharedRoundedIcon from '@material-ui/icons/FolderSharedRounded';
import {
  Box,
  Tab,
  Tabs,
  Avatar,
  Badge,
  Grid,
  Tooltip,
} from '@material-ui/core';

const TabPanel = (props) => {
  const { children, value, index, ...other } = props;

  return (
    <div
      role='tabpanel'
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={4}>
          <p>{children}</p>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

const a11yProps = (index) => {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

const UserPage = ({ authData, usersData, fetchUserById, contestsData, fetchContests }) => {
  const { userId } = useParams();
  const history = useHistory();
  const [value, setValue] = useState(0);
  const [openPhoto, setOpenPhoto] = useState(false);
  const [currentPhoto, setCurrentPhoto] = useState([]);

  useEffect(() => {
    fetchUserById(+userId);
  }, [userId, fetchUserById])

  useEffect(() => {
    fetchContests();
  }, [fetchContests])

  const { singleUser: { photos } } = usersData;
  const { contests } = contestsData;
  const userContestsIds = photos?.map(el => el.contests_id);
  const filteredContests = contests.filter(el => userContestsIds?.indexOf(el.id) !== -1);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  }

  const handlePhotoOpen = (event) => {
    setCurrentPhoto(event.target.name.split(','))
    setOpenPhoto(true);
  }

  const openContestPage = (event) => {
    const id = event.target.id;
    history.push(`/contests/${id}`)
  }

  return (
    <div className='user-page-container'>
      <LoggedHeader />
      <div className='profile-cover'>
        <img src={`${API_URL}res-covers/res-profile-cover.jpg`} alt={authData.firstName} id={authData.id} className='profile-img' />
      </div>
      <div className='profile-details'>
        <div className='profile-title'>
          <h2 className='profile-name titles'>{authData.firstName + ' ' + authData.lastName}</h2>
        </div>
      </div>
      <div className='main-contest-info'>
        <div className='tabs-root'>
          <Tabs
            orientation='vertical'
            variant='scrollable'
            value={value}
            onChange={handleChange}
            aria-label='Vertical tabs'
            className='tabs-container'
          >
            <Tab label='Profile' {...a11yProps(0)} />
            <Tab label='Photos' {...a11yProps(1)} />
            <Tab label='Contests' {...a11yProps(2)} />
          </Tabs>
          <TabPanel className='tab-panel' value={value} index={0}>
            <div className='profile-info-container'>
              <Grid container xs={12} spacing={3}>
                <Grid className='profile-pic-container' xs={5}>
                  <Avatar
                    name='image_url'
                    className='avatar'
                    src={authData.picture}
                    alt={authData.firstName}
                    style={{
                      height: "150px",
                      width: "150px",
                      margin: "40px",
                      backgroundColor: "transparent",
                    }}
                  />
                  <Badge
                    badgeContent={authData.role}
                    color='primary'
                    className='user-badge'
                  />
                </Grid>
                <Grid
                  className='profile-grid'
                  direction='column'
                  xs={7}
                  spacing={3}
                >
                  <br /><br /><br /><br />
                  <h2>Points: {usersData.singleUser.user_points ? usersData.singleUser.user_points : 0}</h2>
                  {(authData.role !== 'Oragnizer') && (authData.role !== 'Wise and Benevolent Photo Dictator') && (
                    <h4 className='remaining-points'>
                      {authData.role === 'Junkie' && `Collect ${51 - (authData.points ? authData.points : 0)} points to unlock Enthusiast status`}
                      {authData.role === 'Enthusiast' && `Collect ${151 - (authData.points ? authData.points : 0)} points to unlock Master status`}
                      {authData.role === 'Master' && `Collect ${1001 - (authData.points ? authData.points : 0)} points to unlock Wise and Benevolent Photo Dictator status`}
                    </h4>
                  )}
                </Grid>
              </Grid>
            </div>
          </TabPanel>
          <TabPanel className='tab-panel' value={value} index={1}>
            <div className='my-photos-container'>
              {photos && photos.length > 0 ? photos.map(photo =>
                <div className='user-photo-container' key={photo.id} onClick={handlePhotoOpen}>
                  <img src={`${API_URL}res-photos/${photo.photo_res}`} name={[photo.id, photo.contests_id]} alt={photo.title} className='thumb-photo' />
                  <div className='profile-vote-score-info'>
                    <span className='profile-current-score'><strong>Score: </strong>{photo.score ? photo.score : 0}</span>
                  </div>
                </div>
              )
                :
              <div>
                <span>No photos yet :(</span>
              </div>}
            </div>
          </TabPanel>
          <TabPanel className='tab-panel' value={value} index={2}>
            <div className='my-contests-container'>
              {filteredContests.length > 0 ? filteredContests.map(contest =>
                <div className='contest-category-single' key={contest.id} onClick={openContestPage}>
                  <div className='contest-top-details' onClick={(e) => e.preventDefault()}>
                    <span className='contest-title' id={contest.id}>{contest.title} <br /></span>
                    <div style={{ maxWidth: '50px', width: '100%' }} id={contest.id} onClick={(e) => e.preventDefault()} />
                    {/* <Tooltip title='Category' placement='top'><h3 className='category'>{contest.category} <br /></h3></Tooltip> */}
                    {contest.type === 'Invitational'
                      ? <Tooltip title='Contest Type: Invitational' placement='top'><FolderSpecialIcon className='contest-icon' id={contest.id} onClick={(e) => e.preventDefault()} /></Tooltip>
                      : <Tooltip title='Contest Type: Open' placement='top'><FolderSharedRoundedIcon id={contest.id} className='contest-icon' onClick={(e) => e.preventDefault()} /></Tooltip>}
                    <Tooltip title={getProjectPhase(contest)} placement='top'><h5 className='phase-badge' onClick={(e) => e.preventDefault()}>{getProjectPhase(contest)}</h5></Tooltip>
                  </div>
                  <img src={contest.cover_img_res && `${API_URL}res-covers/${contest.cover_img_res}`} alt={contest.title} className='row-cover-img' id={contest.id} />
                </div>
              )
                :
              <div>
                <span>No contests yet :(</span>
              </div>}
            </div>
          </TabPanel>
        </div>
        <PhotoDetails openPhoto={openPhoto} photoId={currentPhoto[0]} contestId={currentPhoto[1]} setOpenPhoto={setOpenPhoto} />
      </div>
    </div>
  )
}

UserPage.propTypes = {
  authData: PropTypes.object.isRequired,
  usersData: PropTypes.object.isRequired,
  contestsData: PropTypes.object.isRequired,
  fetchUserById: PropTypes.func.isRequired,
  fetchContests: PropTypes.func.isRequired,
}

const mapStateToProps = state => {
  return {
    authData: state.auth,
    usersData: state.singleUser,
    contestsData: state.contests,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchUserById: (userId) => dispatch(fetchUserById(userId)),
    fetchContests: () => dispatch(fetchContests()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserPage);
