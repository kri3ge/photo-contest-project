import { React, useEffect } from 'react'
import './FilterPage.scss';
import { useHistory } from "react-router-dom";
import queryString from 'query-string';
import LoggedHeader from '../../Layout/Headers/LoggedHeader';
import { getProjectPhase } from "../../utils/utils";
import { connect } from 'react-redux';
import { fetchContests } from "../../ReduxStore/actions/contestActions";
import PropTypes from 'prop-types'
import { Tooltip } from "@material-ui/core";
import FolderSpecialIcon from '@material-ui/icons/FolderSpecial';
import FolderSharedRoundedIcon from '@material-ui/icons/FolderSharedRounded';
import { API_URL } from '../../requests/requestsURL';

const FilterPage = ({fetchContests, location, contestsData}) => {
    const history = useHistory();

    useEffect(() => {
        fetchContests();
    }, [fetchContests])

    const filter = queryString.parse(location.search)
    const key = Object.keys(filter);
    const value = Object.values(filter);
    const contestsWithPhase = contestsData.contests?.map(contest => ({...contest, phase: getProjectPhase(contest)}));
    const filteredContests = contestsWithPhase?.filter(contest => contest[key[0]] === value[0]);

    const openContestPage = (event) => {
        const id = event.target.id;
        history.push(`contests/${id}`)
    }

    return (
      <div className='filter-main-container'>
        <LoggedHeader />
        <h2 className='filter-main-title'>Results for {key}: {value}</h2>
        <div className='category-row-container-filter'>
          {filteredContests.length > 0 && filteredContests.map(contest =>
            <div className='contest-category-single-filter' key={contest.id} onClick={openContestPage}>
              <div className='contest-top-details-filter' onClick={(e) => e.preventDefault()}>
                <span className='contest-title-filter' id={contest.id}>{contest.title} <br /></span>
                <div style={{ maxWidth: '50px', width: '100%' }} id={contest.id} onClick={(e) => e.preventDefault()} />
                {contest.type === 'Invitational'
                                ? <Tooltip title='Contest Type: Invitational' placement='top'><FolderSpecialIcon className='contest-icon' id={contest.id} onClick={(e) => e.preventDefault()} /></Tooltip>
                                : <Tooltip title='Contest Type: Open' placement='top'><FolderSharedRoundedIcon id={contest.id} className='contest-icon' onClick={(e) => e.preventDefault()} /></Tooltip>}
                <Tooltip title={getProjectPhase(contest)} placement='top'><h5 className='phase-badge' onClick={(e) => e.preventDefault()}>{getProjectPhase(contest)}</h5></Tooltip>
              </div>
              <img src={contest.cover_img_res && `${API_URL}res-covers/${contest.cover_img_res}`} alt={contest.title} className='row-cover-img-filter' id={contest.id} />
            </div>
                )}
        </div>
      </div>
    )
}

FilterPage.propTypes = {
    contestsData: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    fetchContests: PropTypes.func.isRequired,
}

const mapStateToProps = state => {
    return {
        contestsData: state.contests,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchContests: () => dispatch(fetchContests()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(FilterPage);
