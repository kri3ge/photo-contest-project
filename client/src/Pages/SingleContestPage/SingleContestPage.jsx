/* eslint-disable react/prop-types */
import { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import LoggedHeader from '../../Layout/Headers/LoggedHeader'
import React from 'react'
import './SingleContestPage.scss'
import PropTypes from 'prop-types';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import { Button, Dialog, DialogActions, DialogTitle, Step, StepLabel, Stepper } from '@material-ui/core';
import CameraIcon from '@material-ui/icons/Camera';
import clsx from 'clsx';
import Check from '@material-ui/icons/Check';
import LooksOneRoundedIcon from '@material-ui/icons/LooksOneRounded';
import LooksTwoRoundedIcon from '@material-ui/icons/LooksTwoRounded';
import FlagRoundedIcon from '@material-ui/icons/FlagRounded';
import StepConnector from '@material-ui/core/StepConnector';
import { API_URL } from '../../requests/requestsURL';
import JoinContestForm from '../../Forms/JoinContestForm';
import { connect, useDispatch } from 'react-redux';
import PhotoDetails from '../../Layout/PhotoDetails/PhotoDetails'
import { drawWinners, fetchContestById, fetchContestWinners } from '../../ReduxStore/actions/contestActions';
import { fetchPhotos } from '../../ReduxStore/actions/photoActions';
import { fetchUsers } from '../../ReduxStore/actions/userActions';
import { calculateTime, getProjectPhase, redirectToHomePage } from '../../utils/utils';
import VotingForm from '../../Forms/VotingForm';

const useQontoStepIconStyles = makeStyles({
  root: {
    color: '#eaeaf0',
    display: 'flex',
    height: 22,
    alignItems: 'center',
  },
  active: {
    color: '#784af4',
  },
  circle: {
    width: 8,
    height: 8,
    borderRadius: '50%',
    backgroundColor: 'currentColor',
  },
  completed: {
    color: '#784af4',
    zIndex: 1,
    fontSize: 18,
  },
});

const QontoStepIcon = (props) => {
  const classes = useQontoStepIconStyles();
  const { active, completed } = props;

  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
      })}
    >
      {completed ? <Check className={classes.completed} /> : <div className={classes.circle} />}
    </div>
  );
}

QontoStepIcon.propTypes = {
  active: PropTypes.bool,
  completed: PropTypes.bool,
};

const ColorlibConnector = withStyles({
  alternativeLabel: {
    top: 22,
  },
  active: {
    '& $line': {
      backgroundColor: '#006d77',
    },
    completed: {
      '& $line': {
        backgroundColor: '#83c5be',
      },
    },
  },
  line: {
    height: 3,
    border: 0,
    backgroundColor: '#eaeaf0',
    borderRadius: 1,
  },
})(StepConnector);

const useColorlibStepIconStyles = makeStyles({
  root: {
    backgroundColor: '#ccc',
    zIndex: 1,
    color: '#fff',
    width: 50,
    height: 50,
    display: 'flex',
    borderRadius: '50%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  active: {
    backgroundColor: '#006d77',
    boxShadow: '0 0 4px 0 #006d77',
  },
  completed: {
    backgroundColor: '#83c5be',
  },
});

const ColorlibStepIcon = (props) => {
  const classes = useColorlibStepIconStyles();
  const { active, completed } = props;

  const icons = {
    1: <LooksOneRoundedIcon />,
    2: <LooksTwoRoundedIcon />,
    3: <FlagRoundedIcon />,
  };

  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
        [classes.completed]: completed,
      })}
    >
      {icons[String(props.icon)]}
    </div>
  );
}

ColorlibStepIcon.propTypes = {
  active: PropTypes.bool,
  completed: PropTypes.bool,
  icon: PropTypes.node,
};

const getSteps = () => {
  return ['Participation open', 'Jury votes', 'Results'];
}

const TabPanel = (props) => {
  const { children, value, index, ...other } = props;

  return (
    <div
      role='tabpanel'
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={4}>
          <p>{children}</p>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

const a11yProps = (index) => {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}


const availableSteps = {
  'Phase One': 0,
  'Phase Two': 1,
  'Finished': 2,
}


const SingleContestPage = ({ fetchContestById, contestData, fetchPhotos, drawWinners, fetchUsers, photosData, authData, usersData, winnersData, fetchContestWinners }) => {
  const { contestId } = useParams();
  const [value, setValue] = useState(0);
  const [openDialog, setOpenDialog] = useState(false);
  const [openPhoto, setOpenPhoto] = useState(false);
  const [openVoting, setOpenVoting] = useState({state: false , id: 0});
  const [currentPhoto, setCurrentPhoto] = useState(0);
  const [openConfirmation, setOpenConfirmation] = useState({state: false, message: 'success'})
  const steps = getSteps();
  const history = useHistory();
  const dispatch = useDispatch();
  
  const handleChange = (event, newValue) => {
    setValue(newValue);
  }

  const handleClose = () => {
    setOpenDialog(false);
    setOpenConfirmation({...openConfirmation, state:false});
  };
  
  const handlePhotoOpen = (event) => {
    console.log(event.target);
    setCurrentPhoto(event.target.name)
    setOpenPhoto(true);
  }
  
  useEffect(() => {
    const fetchInitialRequests = async() => {
      await fetchContestById(+contestId);
      if (contestData.error.includes('401')) {
        redirectToHomePage(dispatch, history)
      }
      await fetchPhotos(contestId);
      await fetchUsers();
      await fetchContestWinners(contestId);
    }
    fetchInitialRequests()
    }, [fetchContestById, fetchPhotos, fetchUsers, contestId,
      contestData.error, dispatch, history, fetchContestWinners
    ]);
    
    const drawContestWinners = async () => {
      handleClose()
      await drawWinners(contestData.currentContest.id)
      await fetchContestWinners(contestId);
        setOpenConfirmation({state: true, message: 'success'})
    }

    const openVotingForm = (event) => {
      console.log(event.target);
      console.log(event.currentTarget);
      setOpenVoting({ state: true, id: event.currentTarget.name, title: event.currentTarget.title } )
    }

  return (
    <div className='single-contest-page-container'>
      <LoggedHeader />
      {contestData.currentContest && 
        <div>
          <div className='contest-cover'>
            <img src={`${API_URL}res-covers/${contestData.currentContest.cover_img_res}`} alt={contestData.currentContest.title} id={contestData.currentContest.id} className='cover-img' />
          </div>
          <div className='contest-details'>
            <div className='contest-title'>
              <h2 className='contest-name titles'>{contestData.currentContest.title}</h2>
            </div>
            {getProjectPhase(contestData.currentContest) === 'Phase One' && authData.role !== 'Organizer' &&
              <div className='apply-button'>
                <Button className='join-button pop-btn' variant='outlined' onClick={()=>setOpenDialog(true)}><CameraIcon /><span className='apply-title'>Join Contest!</span></Button>
              </div>}
          </div>
          <div className='main-contest-info'>
            <div className='tabs-root'>
              <Tabs
                orientation='vertical'
                variant='scrollable'
                value={value}
                onChange={handleChange}
                aria-label='Vertical tabs'
                className='tabs-container'
              >
                <Tab label='Info' {...a11yProps(0)} />
                <Tab label='Photos' {...a11yProps(1)} />
                {(authData.role === 'Organizer' || getProjectPhase(contestData.currentContest) === 'Finished')  &&
                  <Tab label='Ranking' {...a11yProps(2)} />}
                {authData.role === 'Organizer' &&
                  <Tab label='Jury' {...a11yProps(3)} />}
              </Tabs>
              <TabPanel className='tab-panel' value={value} index={0}>
                <div className='basic-category-info'>
                  <Stepper alternativeLabel className='stepper-container' activeStep={availableSteps[getProjectPhase(contestData.currentContest)]} connector={<ColorlibConnector />}>
                    {steps.map((label) => (
                      <Step key={label}>
                        <StepLabel className='step-label' StepIconComponent={ColorlibStepIcon}>{label}</StepLabel>
                      </Step>
                ))}
                  </Stepper>
                  <br />
                  {getProjectPhase(contestData.currentContest) === 'Phase One'
              ?
                <div className='phase-info'>
                  <span>{calculateTime(contestData, 'days')} days left to join!</span>
                  <br />
                  Voting starts on {calculateTime(contestData, 'diffPhaseOne')}
                </div>
                :
                getProjectPhase(contestData.currentContest) === 'Phase Two'
                ? 
                  <div className='phase-info'>
                    <span>{calculateTime(contestData, 'hours')} hours left to vote!</span>
                  </div>
                : 
                  <div className='finished-container'>
                    <div className='phase-info'>
                      <span>Contest finished on {calculateTime(contestData, 'diffPhaseTwo')}! Check the final ranking</span>
                    </div>
                  </div>}
                  <div className='additional-info'>
                    <div>
                      <strong className='info-title'>Contest type:  </strong>
                      <span className='info-text'>{contestData.currentContest.type}</span>
                    </div>
                    <br />
                    <div>
                      <strong className='info-title'>Category:  </strong>
                      <span className='info-text'>{contestData.currentContest.category}</span>
                    </div>
                    <div className='draw-winners-btn-container'>
                      {contestData.currentContest && authData.role === 'Organizer' && winnersData.winners.length === 0 && getProjectPhase(contestData.currentContest) === 'Finished' &&
                        <Button className='draw-winners-btn pop-btn' onClick={drawContestWinners}>Draw winners</Button>}
                    </div>
                  </div>
                </div>
              </TabPanel>
              <TabPanel className='tab-panel' value={value} index={1}>
                <div className='all-photos-container'>
                  {(contestData.currentContest.jury && contestData.currentContest.jury.filter(jury => jury.id === authData.userId).length > 0) || authData.role === 'Organizer' || getProjectPhase(contestData.currentContest) !== 'Phase One'
                  ? photosData && photosData.photos.length > 0 ? photosData.photos.sort((a, b) => b.score - a.score).map(photo => 
                    <div key={photo.id} className='contest-photo-container'>
                      <div onClick={handlePhotoOpen}>
                        <img src={`${API_URL}res-photos/${photo.photo_res}`} name={photo.id} alt={photo.title} className='thumb-photo' />
                      </div>
                      <div className='photo-vote-score-info'>
                        {((contestData.currentContest.jury && contestData.currentContest.jury.filter(jury => jury.users_id === authData.userId).length > 0) || authData.role === 'Organizer') && getProjectPhase(contestData.currentContest) === 'Phase Two' &&
                          <>
                            <Button className='main-btn vote-btn pop-btn' name={photo.id} title={photo.title} onClick={openVotingForm}>Vote</Button>
                            <span className='current-score'><strong>Score: {photo.score ? photo.score.toFixed(1) : 0}</strong></span>
                          </>}
                        {getProjectPhase(contestData.currentContest) === 'Finished' &&
                          <span className='current-score'><strong>Score: {photo.score ? photo.score.toFixed(1) : 0}</strong></span>}
                      </div>
                    </div>
              )
            : 
                  <div>
                    <span>No photos yet :(</span>
                  </div>
                  :
                  <div>
                    <span>Photos will be available after entry deadline.</span>
                  </div>}
                </div>
              </TabPanel>
              <TabPanel className='tab-panel' value={value} index={2}>
                <div className='all-photos-ranking-container'>
                  {getProjectPhase(contestData.currentContest) === 'Finished' && winnersData.winners &&
                  winnersData.winners.length > 0 ? winnersData.winners.sort((a, b) => a.place - b.place).map(winner =>
                    <div className='contest-photo-container' key={winner.id}>
                      <div onClick={handlePhotoOpen} name={winner.photo_id} className='single-winner-div'>
                        <h2 className='winner-name'>{winner.first_name} {winner.last_name}</h2>
                        <img src={`${API_URL}res-photos/${winner.photo_res}`} name={winner.photo_id} alt={winner.first_name} className='thumb-photo' />
                        <img src={process.env.PUBLIC_URL + `/${winner.place}.svg`} name={winner.photo_id} alt={winner.placeStr} title={`${winner.placeStr} place`} className='winner-badge' />
                      </div>
                    </div>)
              :
                  <div>
                    <span>Ranking will be available after voting finishes.</span>
                  </div>}
                </div>
              </TabPanel>
              <TabPanel className='tab-panel' value={value} index={3}>
                <div className='main-jury-container'>
                  {contestData.currentContest.jury && usersData.users.length > 0 && contestData.currentContest.jury.map(jury => (
                    <div key={jury.users_id} className='contest-jury-container'>
                      {usersData.users.filter(user => user.id === jury.users_id)[0] && usersData.users.filter(user => user.id === jury.users_id)[0].first_name + ' ' + usersData.users.filter(user => user.id === jury.users_id)[0].last_name}
                    </div>)
                )}
                </div>
              </TabPanel>
            </div>
          </div>
          <JoinContestForm openDialog={openDialog} setOpenDialog={setOpenDialog} contestData={contestData.currentContest} />
          <PhotoDetails openPhoto={openPhoto} photoId={currentPhoto} contestId={contestData.currentContest.id} setOpenPhoto={setOpenPhoto} />
          <VotingForm openVoting={openVoting} setOpenVoting={setOpenVoting} />
          <Dialog onClose={handleClose} aria-labelledby='simple-dialog-title' open={openConfirmation.state}>
            <DialogTitle>
              {winnersData.winners.length > 0 ? 'Winners have been set!' : 'There are not enough votes to draw winners! \n Consider extending the voting phase.'}
            </DialogTitle>
            <DialogActions>
              <Button onClick={handleClose} className='submit-button'>Ok</Button>
            </DialogActions>
          </Dialog>
        </div>}
    </div>
  )
}

SingleContestPage.propTypes = {
  fetchContestById: PropTypes.func.isRequired,
  contestData: PropTypes.object.isRequired,
  fetchPhotos: PropTypes.func.isRequired,
  fetchUsers: PropTypes.func.isRequired,
  drawWinners: PropTypes.func.isRequired,
  fetchContestWinners: PropTypes.func.isRequired,
  photosData: PropTypes.object.isRequired,
  authData: PropTypes.object.isRequired,
}

const mapStateToProps = state => {
  return {
    contestData: state.currentContest,
    photosData: state.photos,
    authData: state.auth,
    usersData: state.users,
    winnersData: state.winners
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchContestById: (contestId) => dispatch(fetchContestById(contestId)),
    fetchPhotos: (contestId) => dispatch(fetchPhotos(contestId)),
    fetchUsers: () => dispatch(fetchUsers()),
    drawWinners: (contestId) => dispatch(drawWinners(contestId)),
    fetchContestWinners: (contestId) => dispatch(fetchContestWinners(contestId)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SingleContestPage);
