export const authRequest  = {
    userSession: `/session`,
};

export const userRequest  = {
    userRequest: `/users/`,
};

export const contestsRequest  = {
    getContest: `/contests/`,
    getAllContests: `/contests?page=1&limit=20`,
    getAllCategories: `/contests/categories`,
    createContest: `/contests/record`,
};

export const API_URL = 'http://localhost:3001/';
