import axios from 'axios';

const basicAxios = axios.create({
    baseURL:'http://localhost:3001/api',
    headers: {
        Authorization: {
          toString () {
            return `Bearer ${localStorage.getItem('token')}`
          }
        }
      }
    });

export default basicAxios;
