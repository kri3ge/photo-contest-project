import React, { useState } from 'react';
import './NotFound.scss'
import ImagePreview from './ImagePreview';
import Camera, { IMAGE_TYPES } from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';
import LoggedHeader from '../Headers/LoggedHeader';
import { IconButton, Button } from '@material-ui/core';
import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';

const NotFound = () => {
  const [cameraState, setCameraState] = useState(false);
  const [dataUri, setDataUri] = useState('');
  const isFullscreen = false;

  const openCamera = (event) => {
    event.preventDefault();
    setCameraState(true);
  };

  const handleTakePhotoAnimationDone = (dataUri) => {
    setDataUri(dataUri);
  }

  return (
    <div className='not-found-container'>
      <LoggedHeader />
      <h2 className='not-found-title'>Seems like you're lost...</h2>
      <h2>Don't miss the opportunity to immortalize that disoriented face of yours!</h2>
      <hr />
      <div>
        {!cameraState
          ? (
            <div className='camera-button'>
              <IconButton onClick={openCamera}>
                <AddAPhotoIcon fontSize='large' className='selfie-icon' />
              </IconButton>
            </div>
          ) : dataUri
            ? <ImagePreview
                dataUri={dataUri}
                isFullscreen={isFullscreen}
                className='selfie-container-photo'
              />
            : <Camera
                onTakePhotoAnimationDone={handleTakePhotoAnimationDone}
                imageType={IMAGE_TYPES.JPG}
                isFullscreen={isFullscreen}
                className='selfie-container-photo'
              />}
      </div>
      <div>
        {dataUri &&
          <div className='download-button-container'>
            <a href={dataUri} target='_blank' rel='noopener noreferrer' download>
              <Button className='download-btn'>
                DOWNLOAD
              </Button>
            </a>
          </div>}
      </div>
    </div>
  )
}

export default NotFound;
