import { IconButton } from '@material-ui/core';
import './Footer.scss';
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import TwitterIcon from "@material-ui/icons/Twitter";
import EmailIcon from '@material-ui/icons/Email';
import { connect } from 'react-redux';
import PropTypes from 'prop-types'
import ReactIcon from '../../Data/react-icon.svg'
import ReduxIcon from '../../Data/redux-icon.svg'
import MaterialIcon from '../../Data/material-ui-icon.svg'
import MariaDBIcon from '../../Data/mariadb-icon.svg'
import MySQLIcon from '../../Data/mysql-icon.svg'
import NodeIcon from '../../Data/nodejs-icon.svg'
import SASSIcon from '../../Data/sass-icon.svg'

const Footer = ({authData}) => {
  return ( authData.isLoggedIn &&
    <div className='footer-container'>
      <div className='main-footer'>
        <div className='contact-details'>
          <IconButton>
            <EmailIcon className='social-media-icon' />
          </IconButton>
          <span className='email-info'>geekshots.contests@gmail.com </span>
        </div>
        <div className='technologies-container'>
          <img src={ReactIcon} alt='React' className='svg-technology' />
          <img src={ReduxIcon} alt='Redux' className='svg-technology' />
          <img src={MaterialIcon} alt='Material UI' className='svg-technology' />
          <img src={SASSIcon} alt='SASS' className='svg-technology' />
          <img src={NodeIcon} alt='Node.js' className='svg-technology' />
          <img src={MySQLIcon} alt='MySQL' className='svg-technology' />
          <img src={MariaDBIcon} alt='MariaDB' className='mariadb-icon' />
        </div>
        <div className='social-media'>
          <IconButton>
            <FacebookIcon className='social-media-icon' />
          </IconButton>
          <IconButton>
            <InstagramIcon className='social-media-icon' />
          </IconButton>
          <IconButton>
            <TwitterIcon className='social-media-icon' />
          </IconButton>
        </div>
      </div>
    </div>
  )
}

Footer.propTypes = {
  authData: PropTypes.object.isRequired,
}

const mapStateToProps = state => {
  return {
    authData: state.auth,
  }
}

export default connect(mapStateToProps)(Footer)
