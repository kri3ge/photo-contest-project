/* eslint-disable react/prop-types */
import { React, useState, useEffect } from 'react';
import './LoggedHeader.scss';
import Logo from '../../Data/gs-logo.png';
import { Link, useHistory } from 'react-router-dom';
import { connect, useDispatch } from 'react-redux';
import { fetchContests } from "../../ReduxStore/actions/contestActions";
import ExitToAppRoundedIcon from "@material-ui/icons/ExitToAppRounded";
import CameraIcon from '@material-ui/icons/Camera';
import EmojiEventsIcon from '@material-ui/icons/EmojiEvents';
import PropTypes from 'prop-types'
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import {
  Button,
  Avatar,
  IconButton,
  Tooltip,
  withStyles,
  Menu,
  MenuItem,
} from '@material-ui/core';

const StyledMenu = withStyles({
  paper: {
    border: '1px solid #d3d4d5',
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'left',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'left',
    }}
    {...props}
  />
));

const LoggedHeader = ({ authData, contestsData, fetchContests }) => {
  const [anchorElCategory, setAnchorElCategory] = useState(null);
  const [anchorElType, setanchorElType] = useState(null);
  const [anchorElPhase, setanchorElPhase] = useState(null);

  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    fetchContests();
  }, [fetchContests])

  const categories = []
  contestsData.contests?.map(cont => categories.indexOf(cont.category) === -1 && categories.push(cont.category))

  const handleClickCategory = (event) => {
    setAnchorElCategory(event.currentTarget);
  };

  const handleClickType = (event) => {
    setanchorElType(event.currentTarget);
  };

  const handleClickPhase = (event) => {
    setanchorElPhase(event.currentTarget);
  };

  const handleCloseCategory = () => {
    setAnchorElCategory(null);
  };

  const handleCloseType = () => {
    setanchorElType(null);
  };

  const handleClosePhase = () => {
    setanchorElPhase(null);
  };

  const handleLogout = (event) => {
    event.preventDefault();
    dispatch({ type: 'FETCH_LOGOUT' });
    history.push('/home');
  }

  const openUserPage = (event) => {
    event.preventDefault();
    history.push("/users/" + authData.userId);
  };

  const openFilterPage = (event) => {
    event.preventDefault();
    history.push(`/filter?${event.currentTarget.title}=${event.currentTarget.name}`);
    handleCloseCategory();
    handleCloseType();
    handleClosePhase();
  };

  return (
    <div className='main-header-container header'>
      <div className='main-header-logo-container'>
        <Link to='/home'><img src={Logo} alt='GeekShot logo' className='main-header-logo pop-btn' /></Link>
        <span className='titles main-header-logo-text'>GeekShots</span>
      </div>
      <div className='search-container'>
        <div className='contests-button'>
          <Link to='/contests' className='menu-link'>
            <CameraIcon className='header-icon' />
            <Button className='hang-button hanging-btn'> Contests  </Button>
          </Link>
          <span className='horizontal-line'>|</span> 
        </div>
        <div className='filter-buttons'>
          <Button
            className='filter-button'
            aria-controls='customized-menu'
            aria-haspopup='true'
            variant='text'
            color='inherit'
            endIcon={<KeyboardArrowDownIcon />}
            onClick={handleClickCategory}
          >
            Category
          </Button>
          <StyledMenu
            id='customized-menu'
            anchorEl={anchorElCategory}
            keepMounted
            open={Boolean(anchorElCategory)}
            onClose={handleCloseCategory}
          >
            {(categories && categories.length > 0) && categories.map(category =>
              <MenuItem className='menu-item' key={category}>
                <Button className='menu-item-button' title='category' name={category} onClick={openFilterPage}>
                  {category}
                </Button>
              </MenuItem>
            )}
          </StyledMenu>
          <Button
            className='filter-button'
            aria-controls='customized-menu'
            aria-haspopup='true'
            variant='text'
            color='inherit'
            endIcon={<KeyboardArrowDownIcon />}
            onClick={handleClickType}
          >
            Type
          </Button>
          <StyledMenu
            id='customized-menu'
            anchorEl={anchorElType}
            keepMounted
            open={Boolean(anchorElType)}
            onClose={handleCloseType}
          >
            <MenuItem className='menu-item'>
              <Button className='menu-item-button' title='type' name='Open' onClick={openFilterPage}>
                Open
              </Button>
            </MenuItem>
            <MenuItem className='menu-item'>
              <Button className='menu-item-button' title='type' name='Invitational' onClick={openFilterPage}>
                Invitational
              </Button>
            </MenuItem>
          </StyledMenu>
          <Button
            className='filter-button'
            aria-controls='customized-menu'
            aria-haspopup='true'
            variant='text'
            color='inherit'
            endIcon={<KeyboardArrowDownIcon />}
            onClick={handleClickPhase}
          >
            Phase
          </Button>
          <StyledMenu
            id='customized-menu'
            anchorEl={anchorElPhase}
            keepMounted
            open={Boolean(anchorElPhase)}
            onClose={handleClosePhase}
          >
            <MenuItem className='menu-item'>
              <Button className='menu-item-button' title='phase' name='Phase One' onClick={openFilterPage}>
                Phase one
              </Button>
            </MenuItem>
            <MenuItem className='menu-item'>
              <Button className='menu-item-button' title='phase' name='Phase Two' onClick={openFilterPage}>
                Phase two
              </Button>
            </MenuItem>
            <MenuItem className='menu-item'>
              <Button className='menu-item-button' title='phase' name='Finished' onClick={openFilterPage}>
                Finished
              </Button>
            </MenuItem>
          </StyledMenu>
        </div>
      </div>
      <div className='main-header-buttons'>
        {authData.role === 'Organizer' ?
          <div className='admin-actions-container'>
            <Link to='/new-contest' className='menu-link'>
              <Button
                className='main-btn create-contest-btn' variant='outlined'
              >
                Create Contest
              </Button>
            </Link>
            <Link to='/ranking' className='menu-link'>
              <Tooltip title='Ranking' placement='bottom-start'>
                <IconButton
                  className='pop-btn ranking-btn' variant='outlined'
                >
                  <EmojiEventsIcon fontSize='large' />
                </IconButton>
              </Tooltip>
            </Link>
          </div>
          : <div className='empty-div-admin' />}
        <IconButton variant='outlined' color='inherit' onClick={openUserPage}>
          <Tooltip title={authData.username} placement='bottom-start'>
            <Avatar className='avatar-btn' src={authData.picture} style={{ height: '32px', width: '32px', backgroundColor: 'white' }} />
          </Tooltip>
        </IconButton>
        <IconButton className='hang-button pop-btn' onClick={handleLogout}>
          <ExitToAppRoundedIcon fontSize='large' />
        </IconButton>
      </div>
    </div>
  )
}

LoggedHeader.propTypes = {
  authData: PropTypes.object.isRequired,
  contestsData: PropTypes.object.isRequired,
  fetchContests: PropTypes.func.isRequired,
}

const mapStateToProps = state => {
  return {
    authData: state.auth,
    contestsData: state.contests,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchContests: () => dispatch(fetchContests())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoggedHeader);
