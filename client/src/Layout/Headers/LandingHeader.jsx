import './LandingHeader.scss';
import PropTypes from 'prop-types'
import Logo from '../../Data/gs-logo.png';
import { Link } from 'react-router-dom';
import LoginForm from '../../Forms/LoginForm';
import RegisterForm from '../../Forms/RegisterForm';
import { useState } from 'react';

const LandingHeader = ({openRegister}) => {
  const [openDialog, setOpenDialog] = useState({ login: false, register: openRegister })

  return (
    <div className='header-container header'>
      <div className='header-logo-container'>
        <Link to='/home'><img src={Logo} alt='GeekShot logo' className='header-logo pop-btn' /></Link>
        <span className='titles header-logo-text'>GeekShots</span>
      </div>
      <div className='header-middle'>
        <br />
      </div>
      <div className='header-buttons'>
        <LoginForm openDialog={openDialog} setOpenDialog={setOpenDialog} />
        <RegisterForm openDialog={openDialog} setOpenDialog={setOpenDialog} />
      </div>
    </div>
  )
}

LandingHeader.propTypes = {
  openRegister: PropTypes.bool.isRequired,
  }

export default LandingHeader;

