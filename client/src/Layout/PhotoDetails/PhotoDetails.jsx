import { API_URL } from '../../requests/requestsURL';
import React, { useEffect } from 'react'
import { Dialog, DialogContent, DialogTitle, IconButton, Tooltip } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { connect } from 'react-redux';
import './PhotoDetails.scss'
import { fetchPhotoById } from '../../ReduxStore/actions/photoActions';
import Magnifier from "react-magnifier";
import { FacebookShareButton } from 'react-share';
import FacebookIcon from '@material-ui/icons/Facebook';
import ShareIcon from '@material-ui/icons/Share';
import PropTypes from 'prop-types';



const PhotoDetails = ({ openPhoto, setOpenPhoto, contestId, photoId, photoData, fetchPhotoById }) => {

  const handleClose = () => {
   setOpenPhoto(false)
  };

  useEffect(() => {
    const fetchAllPhotos = async() => {
      await fetchPhotoById(contestId, photoId);
    }
    fetchAllPhotos();


  }, [contestId, photoId, fetchPhotoById])

  return photoData ?
    <Dialog
      open={openPhoto}
      className='photo-details-dialog'
      onClose={handleClose}
      fullScreen
      aria-labelledby='responsive-dialog-title'
    >
      <div className='photo-dialog' key={photoData.id}>
        <DialogTitle id='responsive-dialog-title'>
          <IconButton onClick={handleClose} className='close-dialog-button'>
            <CloseIcon onClick={handleClose} />
          </IconButton>
          <h2 className='photo-title'>{photoData.title}
          </h2>
        </DialogTitle>
        <DialogContent className='input-main-container-photo'>
          <div className='single-photo-container'>
            <Magnifier src={`${API_URL}orig-photos/${photoData.photo}`} alt={photoData.title} className='orig-photo' mgShape='circle' />
          </div>
          <div className='additional-photo-info'>
            <Tooltip title='Share on Facebook'>
              <FacebookShareButton
                url={`https://geekshots-photo-contest.web.app/contests/${contestId}`}
                quote='GeekShots - check out this contest'
                className='facebook-share-button'
              >
                <ShareIcon fontSize='small' />
                <FacebookIcon fontSize='large' color='primary' />
              </FacebookShareButton>
            </Tooltip>
            <div className='photo-story'>
              <p>
                <strong>{photoData.story}</strong>
              </p>
            </div>
            <div className='photo-score'>
              <p>
                <strong className='small-title'>Upload date: </strong>{photoData.upload_date}
              </p>
              <p>
                <strong className='small-title'>Score: </strong>{photoData.scores.length > 0 ? (photoData.scores.reduce((acc, score) => (score.score + acc), 0) / photoData.scores.length).toFixed(2) : 'No votes yet'}
              </p>
            </div>
            <div className='photo-story' />
          </div>
        </DialogContent>
      </div>
    </Dialog>
    : null
}

PhotoDetails.propTypes = {
  openPhoto: PropTypes.bool.isRequired,
  setOpenPhoto: PropTypes.func.isRequired,
  contestId: PropTypes.number.isRequired,
  photoId: PropTypes.number.isRequired,
  photoData: PropTypes.object.isRequired,
  fetchPhotoById: PropTypes.func.isRequired,
}

const mapStateToProps = state => {
  return {
    contestsData: state.contests,
    photoData: state.photos.singlePhoto[0],
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchPhotoById: (contestId, photoId) => dispatch(fetchPhotoById(contestId, photoId)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PhotoDetails);

