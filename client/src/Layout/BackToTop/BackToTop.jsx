import PropTypes from 'prop-types';
import './BackToTop.scss';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Fab from '@material-ui/core/Fab';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Zoom from '@material-ui/core/Zoom';

const ScrollTop = (props) => {
    const { children, window } = props;
  const trigger = useScrollTrigger({
    target: window ? window() : undefined,
    disableHysteresis: true,
    threshold: 100,
  });

  const handleClick = (event) => {
    const anchor = (event.target.ownerDocument || document).querySelector('#back-to-top-anchor');
    console.log(anchor);

    if (anchor) {
      anchor.scrollIntoView({ behavior: 'smooth', block: 'center' });
      console.log('inside anchor');
    }
  };

  return (
    <Zoom in={trigger}>
      <div onClick={handleClick} role='presentation' className='back-to-top-root' style={{zIndex: "100"}}>
        {children}
      </div>
    </Zoom>
  );
}

ScrollTop.propTypes = {
  children: PropTypes.element.isRequired,
  window: PropTypes.func,
};

const BackToTop = (props) => {
  return (
    <fragment>
      <ScrollTop {...props}>
        <Fab className='fab-component' size='small' aria-label='scroll back to top' title='Back to the top'>
          <KeyboardArrowUpIcon />
        </Fab>
      </ScrollTop>
    </fragment>
  );
}


export default BackToTop;
