import './App.scss';
import LandingPage from './Pages/LandingPage/LandingPage';
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from "react-router-dom";
import ContestsMainPage from './Pages/AllContestsPage/ContestsMainPage';
import { Provider } from 'react-redux';
import store from './ReduxStore/store'
import SingleContestPage from './Pages/SingleContestPage/SingleContestPage';
import UserPage from './Pages/UserPage/UserPage';
import FilterPage from './Pages/FilterPage/FilterPage';
import Footer from './Layout/Footer/Footer'
import CreateContestPage from './Pages/CreateContestPage/CreateContestPage';
import NotFound from '../src/Layout/NotFound/NotFound';
import PrivateRoute from './RouterComponents/PrivateRoute';
import OrganizerPrivateRoute from './RouterComponents/OrganizerPrivateRoute';
import RankingPage from './Pages/RankingPage/RankingPage';


const App = () => {
  return (
    <div className='App'>
      <Router>
        <Provider store={store}>
          <Switch>
            <Redirect path='/' exact to='/home' />
            <Route path='/home' component={LandingPage} />
            <OrganizerPrivateRoute path='/new-contest' component={CreateContestPage} />
            <OrganizerPrivateRoute path='/ranking' exact component={RankingPage} />
            <PrivateRoute path='/contests' exact component={ContestsMainPage} />
            <PrivateRoute path='/contests/:contestId' component={SingleContestPage} />
            <PrivateRoute path='/filter' component={FilterPage} />
            <PrivateRoute path='/users/:userId' component={UserPage} />
            <PrivateRoute path='*' component={NotFound} />
          </Switch>
          <Footer />
        </Provider>
      </Router>
    </div>
  );
}

export default App;
