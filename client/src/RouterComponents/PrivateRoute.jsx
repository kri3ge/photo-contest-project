import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';

const PrivateRoute = ({ component: Component, authData, location, ...rest }) => (
  <Route
    {...rest} render={(props) => (
      authData.isLoggedIn === false || (Date.now() >= authData.exp * 1000)
        ? <Redirect to={{
          pathname: '/home',
          state: { from: location }
        }}
          />
        : <Component {...props} />
    )}
  />
)

PrivateRoute.propTypes = {
  component: PropTypes.func.isRequired,
  authData: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
}

const mapStateToProps = state => {
  return {
    authData: state.auth,
  }
}

export default connect(mapStateToProps)(PrivateRoute);




