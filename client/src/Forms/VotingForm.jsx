import React from 'react'
import PropTypes from 'prop-types'
import { Box, Button, Checkbox, Dialog, DialogContent, DialogTitle, FormControlLabel, TextField } from '@material-ui/core';
import { Form, Formik} from "formik";
import * as Yup from 'yup';
import Rating from "@material-ui/lab/Rating";
import './VotingForm.scss'
import { fetchPhotos, fetchPhotoVote } from '../ReduxStore/actions/photoActions';
import { connect } from 'react-redux';

const labels = {
  1: 'Useless',
  2: 'Amateur',
  3: 'Good but still amateur',
  4: 'Not bad',
  5: 'Ok',
  6: 'Good',
  7: 'Very good',
  8: 'Great',
  9: 'Excellent',
  10: 'Piece of Art',
};

const VotingForm = ({ openVoting, setOpenVoting, fetchPhotoVote, currentContest, fetchPhotos }) => {
  const [hover, setHover] = React.useState(-1);
  const handleClose = () => {
    setOpenVoting({ state: false, id: 0, title: '' })
  };

  const handleVote = async (values) => {
    if (values.categoryNotFit === true) {
      values.score = 0;
    }
    await fetchPhotoVote(currentContest.id, openVoting.id, { score: values.score, comment: values.comment })
    await fetchPhotos(currentContest.id);
    handleClose()
  }

  const VotingSchema = Yup.object().shape({
    score: Yup.number()
      .required('Title required'),
    comment: Yup.string()
      .required('Comment required')
      .min(5, 'Comment too short.')
      .max(400, 'Comment too long.'),
    categoryNotFit: Yup.boolean(),
  })

  return (
    <div className='voting-form-main-container'>
      <Dialog open={openVoting.state} onClose={handleClose} width='sm' aria-labelledby='form-dialog-title'>
        <DialogTitle id='form-dialog-title'>Vote for photo {openVoting.title}</DialogTitle>
        <DialogContent>
          <Formik
            onSubmit={(values) => { handleVote(values) }}
            initialValues={{
              score: 0,
              comment: '',
              categoryNotFit: false,
            }}
            validationSchema={VotingSchema}
          >
            {({ values, errors, touched, handleChange, resetForm, setFieldValue }) => (
              <Form id='upload-form'>
                <div className='form-container-join'>
                  <div className='additional-photo-data'>
                    <div className='rating-container'>
                      <Rating
                        name='score'
                        className='rating-form'
                        defaultValue={5} max={10}
                        value={values.categoryNotFit === true ? 0 : values.score}
                        onChange={handleChange}
                        onChangeActive={(event, newHover) => {
                          setHover(newHover);
                        }}
                      />
                      <Box ml={2} className='rate-box'>{labels[hover !== -1 ? hover : values.categoryNotFit === true ? 0 : values.score] || ''}</Box>
                    </div>
                    <TextField
                      variant='outlined' className='vote-input-field' name='comment' label='Comment'
                      onChange={handleChange}
                      value={values.comment}
                      type='text'
                      multiline
                      helperText={errors.comment}
                      error={touched.comment && errors.comment ? true : false}
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          name='categoryNotFit'
                          checked={values.categoryNotFit}
                          onChange={handleChange}
                        />
                      }
                      label={`Doesn't fit contest category`}
                    />
                  </div>
                  <br />
                </div>
                <div className='action-buttons'>
                  <Button onClick={() => {
                    resetForm();
                  }}
                  >Reset
                  </Button>
                  <Button type='submit' className='submit-button'>Submit</Button>
                </div>
              </Form>)}
          </Formik>
        </DialogContent>
      </Dialog>
    </div>
  )
}

VotingForm.propTypes = {
  openVoting: PropTypes.object.isRequired,
  setOpenVoting: PropTypes.func.isRequired,
  fetchPhotoVote: PropTypes.func.isRequired,
  fetchPhotos: PropTypes.func.isRequired,
  currentContest: PropTypes.object.isRequired,
}

const mapStateToProps = state => {
  return {
    votesData: state.votes,
    currentContest: state.currentContest.currentContest,
  }
}
const mapDispatchToProps = dispatch => {
  return {
    fetchPhotoVote: (contestId, photoId, body) => dispatch(fetchPhotoVote(contestId, photoId, body)),
    fetchPhotos: (contestId) => dispatch(fetchPhotos(contestId)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(VotingForm);

