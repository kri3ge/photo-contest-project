import { Button, Dialog, DialogActions, DialogContent, DialogTitle, IconButton, Input, TextField } from '@material-ui/core';
import { useState } from 'react';
import PropTypes from 'prop-types'
import './JoinContestForm.scss';
import { Form, Formik } from "formik";
import * as Yup from 'yup';
import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';
import { connect } from 'react-redux';
import { fetchPhotos, uploadPhoto } from '../ReduxStore/actions/photoActions';
import CloseIcon from '@material-ui/icons/Close';

const JoinContestForm = ({ uploadNewPhoto, photoUploadData, openDialog, setOpenDialog, contestData, fetchPhotos }) => {
  const [openConfirmation, setOpenConfirmation] = useState({state: false, message: 'success'})
  const [uploadedPhoto, setUploadedPhoto] = useState(false);
  const FILE_SIZE = 1600 * 1024;
  const SUPPORTED_FORMATS = [
    "image/jpg",
    "image/jpeg",
    "image/png"
  ];

  const handleClose = () => {
    setOpenDialog(false);
    setOpenConfirmation({...openConfirmation, state:false});
  };

  const handleDragEnter = event => {
    event.preventDefault();
    event.stopPropagation();
  };
  const handleDragLeave = event => {
    event.preventDefault();
    event.stopPropagation();
  };
  const handleDragOver = event => {
    event.preventDefault();
    event.stopPropagation();
  };

  const handleDrop = event => {
    event.preventDefault();
    event.stopPropagation();
    setUploadedPhoto([...event.dataTransfer.files][0])
  };

  const uploadPhoto = async (values) => {
   await uploadNewPhoto(contestData.id, values);
    if (!photoUploadData.loading) {
      if(photoUploadData.error.includes('403')) {
        handleClose()
        setOpenConfirmation({state: true, message: 'error'})
      } else {
        handleClose()
        setOpenConfirmation({state: true, message: 'success'})
        setUploadedPhoto(false)
        await fetchPhotos(contestData.id)
      }
    }
  }


  const UploadPhotoSchema = Yup.object().shape({
    title: Yup.string()
      .required('Title required')
      .min(2, 'Title too short.')
      .max(40, 'Title too long.'),
    story: Yup.string()
      .required('Story required')
      .min(5, 'Story too short.')
      .max(400, 'Story too long.'),
    photo: Yup.mixed()
      .required('Photo required')
      .test(
        "fileSize",
        "File too large",
        value => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        value => value && SUPPORTED_FORMATS.includes(value.type)
      )
  })

  return (
    <div className='join-contest-container'>
      <Dialog
        open={openDialog}
        onClose={handleClose}
        maxWidth='xl'
        aria-labelledby='responsive-dialog-title'
      >
        <div className='join-dialog'>
          <IconButton onClick={handleClose} className='close-dialog-btn'>
            <CloseIcon onClick={handleClose} />
          </IconButton>
          <DialogTitle id='responsive-dialog-title'>
            <h3 className='join-title'>Join the contest
              <br />
              <strong className='contest-dialog-name'>{contestData.title}</strong>
            </h3>
          </DialogTitle>
          <DialogContent className='input-main-container-join'>
            <Formik
              onSubmit={(values) => { uploadPhoto(values) }}
              initialValues={{
                title: '',
                story: '',
              }}
              validationSchema={UploadPhotoSchema}
            >
              {({ values, errors, touched, handleChange, resetForm, setFieldValue }) => (
                <Form id='upload-form'>
                  <div className='form-container-join'>
                    <div className='photo-upload-div'>
                      {uploadedPhoto ?
                        <img src={URL.createObjectURL(uploadedPhoto)} alt='uploaded' className='uploaded-image' />
                        :
                        <div
                          id='my-upload'
                          className='empty-photo'
                          onDrop={event => {
                            handleDrop(event)
                            setFieldValue("photo", [...event.dataTransfer.files][0])
                          }}
                          onDragOver={e => handleDragOver(e)}
                          onDragEnter={e => handleDragEnter(e)}
                          onDragLeave={e => handleDragLeave(e)}
                        >
                          <label htmlFor='upload-photo-input'>
                            <Input
                              style={{ display: 'none' }}
                              variant='outlined' className='upload-input-field' id='upload-photo-input' name='photo'
                              type='file'
                              helperText={errors.photo}
                              onChange={(event) => {
                                setFieldValue("photo", event.currentTarget.files[0])
                                setUploadedPhoto(event.currentTarget.files[0])
                              }}
                            />
                            <h2 className='upload-button' variant='contained' component='span'>
                              <AddAPhotoIcon className='add-icon' />
                              Upload or Drop photo
                            </h2>
                          </label>
                          {errors.photo && <h5 style={{ color: 'red', fontWeight: 'normal' }}>Photo required</h5>}
                        </div>}
                    </div>
                    <div className='additional-photo-data'>
                      <TextField
                        variant='outlined' className='join-input-field' name='title' label='Title' autoFocus
                        onChange={handleChange}
                        value={values.title}
                        helperText={errors.title}
                        error={touched.title && errors.title ? true : false}
                      />
                      <TextField
                        variant='outlined' className='join-input-field' name='story' label='Story'
                        onChange={handleChange}
                        value={values.story}
                        type='text'
                        multiline
                        helperText={errors.story}
                        error={touched.story && errors.story ? true : false}
                      />
                    </div>
                    <br />
                  </div>
                  <div className='action-buttons'>
                    <Button onClick={() => {
                      resetForm();
                      setUploadedPhoto(false);
                    }}
                    >Reset
                    </Button>
                    <Button type='submit' className='submit-button'>Submit</Button>
                  </div>
                  {touched.photo && errors.photo === true
                    ? <p style={{ color: 'red' }}>Incorrect file type </p>
                    : <div style={{ margin: '30px' }} />}
                </Form>)}
            </Formik>
          </DialogContent>
        </div>
      </Dialog>
      <Dialog onClose={handleClose} aria-labelledby='simple-dialog-title' open={openConfirmation.state}>
        <DialogTitle>
          {openConfirmation.message === 'success' ? 'The photo has been submitted successfully!' : 'You have already submitted a photo for this contest!'}
        </DialogTitle>
        <DialogActions>
          <Button onClick={handleClose} className='submit-button'>Ok</Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}

JoinContestForm.propTypes = {
  openDialog: PropTypes.bool.isRequired,
  setOpenDialog: PropTypes.func.isRequired,
  uploadNewPhoto: PropTypes.func.isRequired,
  fetchPhotos: PropTypes.func.isRequired,
  contestData: PropTypes.object.isRequired,
  photoUploadData: PropTypes.object.isRequired,
}

const mapStateToProps = state => {
  return {
    photoUploadData: state.photoUpload
  }
}

const mapDispatchToProps = dispatch => {
  return {
    uploadNewPhoto: (contestId, values) => dispatch(uploadPhoto(contestId, values)),
    fetchPhotos: (contestId) => dispatch(fetchPhotos(contestId)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(JoinContestForm);

