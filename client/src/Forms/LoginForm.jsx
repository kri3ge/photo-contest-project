/* eslint-disable react/prop-types */
import { Button, Dialog, DialogContent, DialogTitle, TextField, withStyles } from "@material-ui/core";
import { Form, Formik } from "formik";
import { useHistory } from "react-router-dom";
import * as Yup from 'yup';
import GoogleLogin from 'react-google-login';
import FacebookLogin from 'react-facebook-login';
import './LoginForm.scss'
import { connect } from 'react-redux'
import decode from "jwt-decode";
import { useEffect, useState } from "react";
import { authUser, loginUserSocialMedia} from "../ReduxStore/actions/authActions";

const getUserDataFromMedia = userDetails => {
  if (userDetails.googleId) {
    const token =  decode(userDetails.tokenId);
    return {username: token.email, firstName: token.given_name, lastName: token.family_name, password: token.sub, picture: token.picture};
  } else if (userDetails.graphDomain === 'facebook') {
    const fullName = userDetails.name.split(' ');
    return {username: userDetails.email, firstName: fullName[0], lastName: fullName[1], password: userDetails.id, picture: userDetails.picture}
  } else {
    return null;
  }
}

const redirectToMainPage = (authData, history) => {
  if (authData.isLoggedIn) {
    history.push('/contests')
  }
}

const MainButton = withStyles(() => ({
  root: {
    color: 'white',
    textTransform: 'none',
    backgroundColor: 'black',
    '&:hover': {
      color: 'black',
      backgroundColor: 'white',
    },
  }
}))(Button);
  
const LoginForm = ({openDialog, setOpenDialog, authUser, registerUser, authData, registrationData, loginSocialMedia}) => {
  const [ validatorValue, setValidatorValue ] = useState(false);
  const history = useHistory();

  useEffect(() => {
    authData.isLoggedIn && history.push("/contests");
  }, [authData.isLoggedIn, history])

  const handleOpen = () => {
    setOpenDialog({ login: true, register: false });
  };

  const handleClose = () => {
    setOpenDialog({ ...openDialog, login: false });
    setValidatorValue(false);
  };

  const openRegister = (event) => {
    setOpenDialog({ login: false, register: true });
    setValidatorValue(false);
  }

  const handleMediaLogin = userDetails => {
    const accountData = getUserDataFromMedia(userDetails)
    loginSocialMedia(accountData)
    if (registrationData.error.includes('409')) {
        alert('You are already registered with other Social Media account. Please login with it. You cannot use different accounts with the same email.')
        }
        else {
          redirectToMainPage(authData, history)
        }
  }

  const responseGoogle = (response) => {
    handleMediaLogin(response);
  }

  const signInUser = async (values) => {
    authUser(values);
    if (authData.error) {
      if (authData.error.includes('403')) {
        setValidatorValue(true);
      }
    } else {
      redirectToMainPage(authData, history);
    }
  }

  const SignInSchema = Yup.object().shape({
    username: Yup.string()
      .email('Username should be your email address')
      .required('Email required'),
    password: Yup.string()
      .required('Password required')
      .min(5, 'Password too short.')
      .matches(/^[a-zA-Z\d\-_.,&!@#%$]+$/, 'Password can only contain Latin letters and symbols.')
  })

  const responseFacebook = (response) => {
    handleMediaLogin(response);
  }

  return (
    <div className='sign-main-container'>
      <MainButton className='main-btn-sign main-btn' variant='outlined' color='inherit' onClick={handleOpen}>Login</MainButton>
      <Dialog
        open={openDialog.login}
        onClose={handleClose}
        maxWidth='xl'
        aria-labelledby='responsive-dialog-title'
      >
        <div className='sign-dialog'>
          <DialogTitle id='responsive-dialog-title'>
            <h3 className='sign-title'>Login</h3>
          </DialogTitle>
          <DialogContent className='input-main-container-sign'>
            <Formik
              onSubmit={(values) => { signInUser(values) }}
              initialValues={{
                username: '',
                password: ''
              }}
              validationSchema={SignInSchema}
            >
              {({ values, errors, touched, handleChange, resetForm }) => (
                <Form>
                  <div className='form-container'>
                    <TextField
                      variant='outlined' className='form-text-field' name='username' label='Username' autoFocus
                      onChange={handleChange}
                      value={values.username}
                      helperText={errors.username}
                      error={touched.username && errors.username ? true : false}
                    />
                    <TextField
                      variant='outlined' className='form-text-field' name='password' label='Password'
                      onChange={handleChange}
                      value={values.password}
                      type='password'
                      helperText={errors.password}
                      error={touched.password && errors.password ? true : false}
                    />
                    <Button onClick={() => { resetForm(); setValidatorValue(false) }}>Reset</Button>
                    <Button type='submit' className='register-button'>Submit</Button>
                    {validatorValue
                      ? <p style={{ color: 'red' }}>Incorrect username/password! </p>
                      : <div style={{ margin: '30px' }} />}
                  </div>
                </Form>)}
            </Formik>
            <div className='google-facebook-login'>
              <GoogleLogin
                className='google-login'
                clientId='281916861534-ibcocdes1j7d9ljtc66tr7kca0qgr5j2.apps.googleusercontent.com'
                buttonText='Login with Google'
                onSuccess={responseGoogle}
                onFailure={(res) => console.log(res)}
                cookiePolicy='single_host_origin'
              />
              <FacebookLogin
                className='pop-btn'
                appId='2900497533516051'
                fields='name,email,picture'
                callback={responseFacebook}
                icon='fa-facebook'
              />
            </div>
            <div className='not-member-container'>
              <span>Not a member?  <Button className='register-button' onClick={openRegister}>Register</Button></span>
            </div>
          </DialogContent>
        </div>
      </Dialog>
    </div>
  )
}

const mapStateToProps = state => {
  return {
    authData: state.auth,
    registrationData: state.register,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    authUser: (body) => dispatch(authUser(body)),
    loginSocialMedia: (body) => dispatch(loginUserSocialMedia(body))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
