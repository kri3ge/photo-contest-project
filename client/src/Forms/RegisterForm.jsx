/* eslint-disable react/prop-types */
import { Button, Dialog, DialogContent, DialogTitle, TextField } from "@material-ui/core";
import { Form, Formik } from "formik";
import { useHistory } from "react-router-dom";
import * as Yup from 'yup';
import { connect } from "react-redux";
import './RegisterForm.scss';
import { useState } from "react";
import { authUser, registerUser } from "../ReduxStore/actions/authActions";

const redirectToMainPage = (authData, history) => {
  if (authData.isLoggedIn) {
    history.push('/contests')
  }
}

const RegisterForm = ({openDialog, setOpenDialog, authData, authUser, registerUser, registrationData}) => {
    const [ validatorValue, setValidatorValue ] = useState(false);
    const history = useHistory();  
  
    const handleOpen = () => {
        setOpenDialog({login: false, register: true})
    };
    
    const handleClose = () => {
        setOpenDialog({...openDialog, register:false})
        setValidatorValue(false);
    };

  const openLogin = (event) => {
    setOpenDialog({ login: true, register: false })
    setValidatorValue(false);
  }

    const registerNewUser = accountData => {
        registerUser(accountData);
        if (registrationData.error.includes('409')) {
          setValidatorValue(true);
        }
        if (registrationData.isRegistered) {
          authUser(accountData);
          redirectToMainPage(authData, history);
        }
    }

  const RegisterSchema = Yup.object().shape({
    username: Yup.string()
      .email('Username should be your email address')
      .required('username required')
      .matches(/^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/, 'Invalid email'),
    firstName: Yup.string()
      .min(2, 'First name too short.')
      .max(30, 'First name too long.')
      .required('First name required'),
    lastName: Yup.string()
      .min(2, 'Last name too short.')
      .max(30, 'Last name too long.')
      .required('Last name required'),
    password: Yup.string()
      .required('Password required')
      .min(5, 'Password too short.')
      .matches(/^[a-zA-Z\d\-_.,&!@#%$]+$/, 'Password can only contain Latin letters and symbols.')
  })

  return (
    <div className='register-main-container'>
      <Button className='hang-button hanging-btn' color='inherit' onClick={handleOpen}>Register</Button>
      <Dialog
        open={openDialog.register}
        onClose={handleClose}
        maxWidth='xl'
        aria-labelledby='responsive-dialog-title'
      >
        <div className='register-dialog'>
          <DialogTitle id='responsive-dialog-title'>
            <h3 className='sign-title'>Register</h3>
          </DialogTitle>
          <DialogContent className='input-main-container-register'>
            <Formik
              onSubmit={(values) => registerNewUser(values)}
              initialValues={{
                username: '',
                firstName: '',
                lastName: '',
                password: ''
              }}
              validationSchema={RegisterSchema}
            >
              {({ values, errors, touched, handleChange, resetForm }) => (
                <Form>
                  <div className='form-container'>
                    <TextField
                      variant='outlined' className='form-input-field' name='username' label='Username' autoFocus
                      onChange={handleChange}
                      value={values.username}
                      helperText={errors.username}
                      error={touched.username && errors.username ? true : false}
                    />
                    <TextField
                      variant='outlined' className='form-input-field' name='firstName' label='First name'
                      onChange={handleChange}
                      value={values.firstName}
                      helperText={errors.firstName}
                      error={touched.firstName && errors.firstName ? true : false}
                    />
                    <TextField
                      variant='outlined' className='form-input-field' name='lastName' label='Last name'
                      onChange={handleChange}
                      value={values.lastName}
                      helperText={errors.lastName}
                      error={touched.lastName && errors.lastName ? true : false}
                    />
                    <TextField
                      variant='outlined' className='form-input-field' name='password' label='Password'
                      onChange={handleChange}
                      value={values.password}
                      type='password'
                      helperText={errors.password}
                      error={touched.password && errors.password ? true : false}
                    />
                    <Button onClick={() => { resetForm(); setValidatorValue(false) }}>Reset</Button>
                    <Button type='submit' className='login-button'>Submit</Button>
                    {validatorValue
                      ? <p style={{ color: 'red' }}>Username already registered! </p>
                      : <div style={{ margin: '30px' }} />}
                  </div>
                </Form>)}
            </Formik>
            <div className='already-member-container'>
              <span>Already a member?  <Button className='login-button' onClick={openLogin}>Login</Button></span>
            </div>
          </DialogContent>
        </div>
      </Dialog>
    </div>
  )
}

const mapStateToProps = state => {
  return {
    authData: state.auth,
    registrationData: state.register,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    authUser: (body) => dispatch(authUser(body)),
    registerUser: (body) => dispatch(registerUser(body))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterForm);
